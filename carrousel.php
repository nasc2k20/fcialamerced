<link rel="stylesheet" href="styles/css/carousel.css">

<div class="panel panel-success panel-mb-6">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-folder-o icon-title"></i> 
            <button class="close" arial-label="close" id="btnCerrarCarrousel">
                <span aria-hidden="true">[Cerrar]</span>
            </button>
        </h3>
    </div>
    <div class="panel-body">
        <section class="content">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="row">
                        <!-- The carousel -->
                        <div id="transition-timer-carousel" class="carousel slide transition-timer-carousel" data-ride="carousel">
                            <!-- Indicators -->
                            <ol class="carousel-indicators">
                                <li data-target="#transition-timer-carousel" data-slide-to="0" class="active"></li>
                                <li data-target="#transition-timer-carousel" data-slide-to="1"></li>
                                <li data-target="#transition-timer-carousel" data-slide-to="2"></li>
                                <li data-target="#transition-timer-carousel" data-slide-to="3"></li>
                                <li data-target="#transition-timer-carousel" data-slide-to="4"></li>
                                <li data-target="#transition-timer-carousel" data-slide-to="5"></li>
                                <li data-target="#transition-timer-carousel" data-slide-to="6"></li>
                                <li data-target="#transition-timer-carousel" data-slide-to="7"></li>
                            </ol>

                            <!-- Wrapper for slides -->
                            <div class="carousel-inner">
                                <div class="item active">
                                    <img src="images/electrolit1.jpg" width="1200" height="250" />
                                </div>
                                <div class="item">
                                    <img src="images/oraldex.jpg" width="1200" height="250" />
                                </div>
                                <div class="item">
                                    <img src="images/oralite.jpg" width="1200" height="250" />
                                </div>
                                <div class="item">
                                    <img src="images/logo.png" width="1200" height="250" />
                                </div>
                                <div class="item">
                                    <img src="images/palagrip.jpg" width="1200" height="250" />
                                </div>
                                <div class="item">
                                    <img src="images/sudagrip.jpg" width="1200" height="250" />
                                </div>
                                <div class="item">
                                    <img src="images/virogrip.jpg" width="1200" height="250" />
                                </div>
                                <div class="item">
                                    <img src="images/virogrip2.jpg" width="1200" height="250" />
                                </div>
                            </div>

                            <!-- Controls -->
                            <a class="left carousel-control" href="#transition-timer-carousel" data-slide="prev">
                                <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>
                                <!--<span class="glyphicon glyphicon-chevron-left"></span>-->
                            </a>
                            <a class="right carousel-control" href="#transition-timer-carousel" data-slide="next">
                                <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                                <!--<span class="glyphicon glyphicon-chevron-right"></span>-->
                            </a>

                            <!-- Timer "progress bar" -->
                            <hr class="transition-timer-carousel-progress-bar animate" />
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script src="styles/js/carousel.js"></script>