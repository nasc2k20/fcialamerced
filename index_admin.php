<?php
require_once 'login/validar.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <!--<meta http-equiv="refresh" content="0;url=index_admin.php">-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sistema de Inventario y Facturacion - Farmacia La Merced</title>

    <link href="styles/css/bootstrap.min.css" rel="stylesheet">
    <link href="styles/css/metisMenu.min.css" rel="stylesheet">
    <link href="styles/css/sb-admin-2.css" rel="stylesheet">
    <link href="styles/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="styles/css/jquery-confirm.min.css">
    <link rel="stylesheet" href="styles/css/jquery-ui.min.css">
    <link rel="stylesheet" href="styles/css/carousel.css">
    <link rel="stylesheet" href="styles/css/bootstrap-datepicker.min.css">

    <!-- <script src="styles/js/jquery.min.js"></script> -->
    <script src="styles/js/jquery-3.3.1.js"></script>
    <script src="styles/js/bootstrap.min.js"></script>
    <script src="styles/js/metisMenu.min.js"></script>
    <script src="styles/js/sb-admin-2.js"></script>
    <script src="styles/js/jquery-confirm.min.js"></script>
    <script src="styles/querys_modulos/MenuPrincipal.js"></script>
    <script src="styles/js/typeahead.min.js"></script>
    <script src="styles/js/jquery-ui.min.js"></script>
    <script src="styles/js/carousel.js"></script>
    <script src="styles/js/bootstrap-datepicker.min.js"></script>
</head>


<body>

    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index_admin.php"> Sistema de Inventario y Facturaci&oacute;n - <strong>Farmacia La Merced</strong></a>
            </div>

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#" class="PerfilUsuarioItem"><i class="fa fa-user fa-fw"></i> Perfil de Usuario</a>
                        </li>
                        <li><a href="#"><i class="fa fa-gear fa-fw"></i> Configuraci&oacute;n</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="login/cerrar_sesion.php"><i class="fa fa-sign-out fa-fw"></i> Cerrar Sesi&oacute;n</a>
                        </li>
                    </ul>
                </li>
            </ul>


            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <div class="row" align="center">
                                    <!-- class="col-md-5 col-lg-6" -->
                                    <div align="center">
                                        <img src="images/avatar_2x.png" alt="" class="img-circle img-responsive" height="100" width="100">
                                    </div>
                                </div>
                                <div style="height: 10px;"></div>
                                <div class="row" align="center" style="font-size: 10pt; text-align: center;">
                                    <?php echo strtoupper($_SESSION['usuario']); ?>
                                    <br>
                                    <span class="user-status">
                                        <span style="color: green;"><i class="fa fa-circle"></i></span>
                                        En Linea
                                    </span>
                                    <br>
                                    <?php echo strtoupper($_SESSION['nombre_usu']); ?>
                                </div>
                            </div>
                        </li>
                        <li>
                            <a href="index_admin.php"><i class="fa fa-dashboard fa-fw"></i> Inicio</a>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Inventario<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#" class="MostrarProdItem">Ver Productos</a>
                                </li>
                                <li>
                                    <a href="#" class="MostrarLabItem">Laboratorios</a>
                                </li>

                                <li>
                                    <a href="#" class="MostrarProdTomaInv">Toma de Inventario</a>
                                </li>

                                <li>
                                    <a href="#" class="MostrarKardexItem">Kardex de Producto</a>
                                </li>
                                <!---->
                            </ul>
                        </li>

                        <li>
                            <a href="tables.html"><i class="fa fa-table fa-fw"></i> Ventas<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#" class="VerFacturasItem">Facturaci&oacute;n</a>
                                </li>
                                <!--
                                <li>
                                    <a href="reportes/rep_ventas_fecha_det.php" target="cargando">Detalle de Facturas</a>
                                </li>
                                
                                <li>
                                    <a href="morris.html">Anular Factura</a>
                                </li>
                                -->
                            </ul>
                        </li>

                        <li>
                            <a href="forms.html"><i class="fa fa-edit fa-fw"></i> Compras<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#" class="MostrarComprasItem">Nueva Compra</a>
                                </li>
                                <li>
                                    <a href="#" class="MostrarProvItem">Proveedores</a>
                                </li>
                            </ul>
                        </li>
                        <!--
                        <li>
                            <a href="forms.html"><i class="fa fa-user-circle fa-fw"></i> Clientes<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="flot.html">Nuevo Cliente</a>
                                </li>
                                <li>
                                    <a href="morris.html">Estado de Cuentas</a>
                                </li>
                            </ul>
                        </li>
                       -->
                        <li>
                            <a href="#"><i class="fa fa-file-zip-o fa-fw"></i> Reportes<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#" class="VerFacturasFechasItem">Ventas por Fecha</a>
                                </li>
                                <li>
                                    <a href="consultas/ver_depto.php" target="cargando">Utilidad por Fecha</a>
                                </li>
                                <li>
                                    <a href="consultas/ver_depto.php" target="cargando">Clientes Deudores</a>
                                </li>
                                <li>
                                    <a href="#" class="MostrarInvDnm">Inventario DNM</a>
                                </li>
                                <li>
                                    <a href="#" class="MostrarTomaInvGenItem">Toma de Inventario General</a>
                                </li>
                                <li>
                                    <a href="#" class="MostrarTomaInvLabItem">Toma de Inventario por Laboratorio</a>
                                </li>
                            </ul>
                        </li>


                        <li>
                            <a href="#"><i class="fa fa-files-o fa-fw"></i> Administraci&oacute;n<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#" class="MostrarUsusItem">Usuarios</a>
                                </li>
                                <li>
                                    <a href="#" class="MostrarBackupItem">Copia de Seguridad</a>
                                </li>
                                <!---->
                                <li>
                                    <a href="#" class="MostrarDeseItem">Desechos</a>
                                </li>

                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="page-wrapper">
            <!--<iframe class="col-lg-12 col-md-12 col-sm-12" name="cargando" height="600" frameborder="0" scrolling="auto"></iframe>-->
            <div id="CarrouselDiv"></div>
            <div id="MostrarLab"></div>
            <div id="MostrarProd"></div>
            <div id="MostrarProdToma"></div>
            <div id="MostrarKardex"></div>
            <div id="MostrarProv"></div>
            <div id="MostrarUsuario"></div>
            <div id="MostrarUsuarioPerfil"></div>
            <div id="MostrarCompras"></div>
            <div id="MostrarFacturas"></div>
            <div id="MostrarDesechos"></div>
            <div id="ReporteTomaInvGen"></div>
            <div id="ReporteTomaInvLab"></div>
            <div id="ReporteVentasFecha"></div>
            <div id="MostrarBackup"></div>
            <div id="MostrarInvDnmDv"></div>
            
        </div>

    </div>
    <!--
    <div id="page-footer" align="center" style="background-color: lightgray;">
        <div class="container">
            <h5><strong>Copyright &copy;
                    <?php  //echo "2018"; //echo date('Y'); ?> - <a href="www.scsynet.com" target="_blank">SysnetCorp</a>.</strong></h5>
            <hr>
            <div class="text-center center-block">
                <a href="https://www.facebook.com/scsysnetweb" target="_blank"><i id="social-fb" class="fa fa-facebook-official fa-3x social"></i></a>
                <a href="https://twitter.com/scsysnetweb" target="_blank"><i id="social-tw" class="fa fa-twitter-square fa-3x social"></i></a>
                <a href="https://plus.google.com/+scsysnetweb" target="_blank"><i id="social-gp" class="fa fa-google-plus-square fa-3x social"></i></a>
                <a href="mailto:scsysnetweb.scsynet.com" target="_blank"><i id="social-em" class="fa fa-envelope-square fa-3x social"></i></a>
                <a href="#" target="_blank"><i id="social-wa" class="fa fa-whatsapp fa-3x social" aria-hidden="true"></i></a>
            </div>
            <hr>
        </div>
    </div>
-->
<script>
$('html, body').css({
    overflow: 'auto',
    height: 'auto'
});
</script>

</body>


</html>
