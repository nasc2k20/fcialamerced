<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>.:: Sistema de Inventario - Farmacia La Merced ::.</title>
    <link href="styles/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="styles/css/login_css.css" rel="stylesheet"/>
    
    <script language="javascript">
        function valida_usuario()
        { 
        //valido el nombre 
        if (document.form1.TextBox1.value.length=='' || document.form1.TextBox2.value.length=='')
        {
            if(document.form1.TextBox1.value=='')
            {
                alert("Tiene Digitar su Usuario") 
                document.form1.TextBox1.focus() 
                return false; 
            }
            else
            {
                alert("Tiene que Digitar su Contraseña");
                document.form1.TextBox2.focus();
                return false;
            }
        }
        }

        function valida_contra()
        {
        if(document.form1.TextBox2.value.length=0)
        {
            alert("Tiene que Digitar su Contraseña");
            document.form1.TextBox2.focus();
            return 0;
        }
        }
    </script>
    
    
</head>

<body>
    <div class="container">
        <div class="card card-container">
            <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
            <img id="profile-img" class="profile-img-card" src="images/avatar_2x.png" />
            <p id="profile-name" class="profile-name-card"></p>
            <form id="form1" name="form1" method="post" action="login/index2.php" onsubmit="return valida_usuario();" class="form-signin">
                <span id="reauth-email" class="reauth-email"></span>
                <input type="text" id="TextBox1" name="TextBox1" class="form-control" placeholder="Digite Usuario" required autofocus>
                <input type="password" id="TextBox2" name="TextBox2" class="form-control" placeholder="Digite Contrase&ntilde;a" required>
                <div id="remember" class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Recuerdame
                    </label>
                </div>
                <button name="Button1" id="Button1" class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Iniciar Sesi&oacute;n</button>
            </form><!-- /form -->
            <a href="#" class="forgot-password">
                Olvidaste la Contrase&ntilde;a?
            </a>
        </div><!-- /card-container -->
    </div><!-- /container -->
</body>
</html>