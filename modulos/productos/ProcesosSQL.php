<?php
require '../../conectar.php';

date_default_timezone_set("America/El_Salvador");

if($_POST['SqlQuery']=='insertar')
{
    try
	{
		$codigo		= $_POST["CodigoProd"];
		$cod_barra	= ($_POST["CodBarraProd"]=='' ? ' ' : $_POST["CodBarraProd"]);
		$nombre_prod= strtoupper($_POST["NombreProd"]);
        $p_costo = number_format(($_POST["PrecioCosto"]),4,'.','');
		$p_dnm	    = number_format(($_POST["PrecioDnm"]=='' ? '0.00' : $_POST["PrecioDnm"]),2,'.','');
		$p_public	=number_format($_POST["PrecioPub"],4,'.','');
		$p_venta = number_format(($_POST["PrecioVenta"]),4,'.','');
		$presenta	= $_POST["PresentaProd"];
		$indicacion	= ($_POST["IndicaProd"]=='' ? ' ' : $_POST["IndicaProd"]);
		$vitrina	= ($_POST["VitrinaProd"]=='' ? '0' : $_POST["VitrinaProd"]);
		$cod_lab	= $_POST["CodeLab"];
		$impuesto	= $_POST["ImpuestoProd"];
		$marca		= "NONE";
        $texto      = "agregar";
		$existencia	= $_POST["SaldoProd"];
		$formato_fi	= new DateTime($_POST["VenceProd"]);
		$f_vence	= $formato_fi->format("Y-m-d");
		$estado_prod= $_POST["EstadoProd"];
		
		
		$inser_prod = "INSERT INTO productos (codigo_barra_prod,nombre_producto,existencia_prod,precio_costo,
		precio_venta,presentacion,vitrina,marca,date_vence,indicaciones,impuesto,cod_lab,estado_prod,precio_dnm,precio_publico)
		VALUES(:codigo_barra_prod,:nombre_producto,:existencia_prod,:precio_costo,
		:precio_venta,:presentacion,:vitrina,:marca,:date_vence,:indicaciones,:impuesto,:cod_lab,:estado_prod,:precio_dnm,:precio_publico)";
		$inser_prod = $DBcon->prepare($inser_prod);
		$inser_prod->bindparam(":codigo_barra_prod", $cod_barra,PDO::PARAM_STR);
		$inser_prod->bindparam(":nombre_producto", $nombre_prod,PDO::PARAM_STR);
		$inser_prod->bindparam(":presentacion", $presenta,PDO::PARAM_STR);
		$inser_prod->bindparam(":marca",$marca,PDO::PARAM_STR);
		$inser_prod->bindparam(":indicaciones", $indicacion,PDO::PARAM_STR);
		$inser_prod->bindparam(":impuesto", $impuesto,PDO::PARAM_STR);
		$inser_prod->bindparam(":estado_prod", $estado_prod,PDO::PARAM_STR);
		$inser_prod->bindparam(":precio_costo", $p_costo,PDO::PARAM_STR);
		$inser_prod->bindparam(":precio_venta", $p_venta,PDO::PARAM_STR);
		$inser_prod->bindparam(":precio_dnm", $p_dnm,PDO::PARAM_STR);
		$inser_prod->bindparam(":precio_publico", $p_public,PDO::PARAM_STR);
		$inser_prod->bindparam(":date_vence", $f_vence,PDO::PARAM_STR);
		$inser_prod->bindparam(":existencia_prod", $existencia,PDO::PARAM_STR);
		$inser_prod->bindparam(":vitrina", $vitrina,PDO::PARAM_STR);
		$inser_prod->bindparam(":cod_lab", $cod_lab,PDO::PARAM_STR);
		
        if($inser_prod->execute())
        {
            header("location: index.php?&texto=$texto");
        }
		else
        {
            echo "\nError al Agregar los Datos:\n";
            print_r($inser_prod->errorInfo());
            echo "<br>El Codigo de Error es: ". $inser_prod->errorCode();
        }
	}
	catch(PDOException $e)
	{
		echo "ERROR AL AGREGAR DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL AGREGAR DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL AGREGAR DATOS 3 ".$s->getMessage();
        exit;
    }
}
elseif($_POST['SqlQuery']=='actualizar')
{
    try
	{
        $codigo		= $_POST["CodigoProd"];
		$cod_barra	= ($_POST["CodBarraProd"]=='' ? ' ' : $_POST["CodBarraProd"]);
		$nombre_prod= strtoupper($_POST["NombreProd"]);
        $p_costo = number_format(($_POST["PrecioCosto"]),4,'.','');
		$p_dnm	    = number_format(($_POST["PrecioDnm"]=='' ? '0.00' : $_POST["PrecioDnm"]),2,'.','');
		$p_public	=number_format($_POST["PrecioPub"],4,'.','');
		$p_venta = number_format(($_POST["PrecioVenta"]),4,'.','');
		$presenta	= $_POST["PresentaProd"];
		$indicacion	= ($_POST["IndicaProd"]=='' ? ' ' : $_POST["IndicaProd"]);
		$vitrina	= ($_POST["VitrinaProd"]=='' ? '0' : $_POST["VitrinaProd"]);
		$cod_lab	= $_POST["CodeLab"];
		$impuesto	= $_POST["ImpuestoProd"];
		$marca		= "NONE";
        $texto      = "editar";
		$existencia	= $_POST["SaldoProd"];
		$formato_fi	= new DateTime($_POST["VenceProd"]);
		$f_vence	= $formato_fi->format("Y-m-d");
		$estado_prod= $_POST["EstadoProd"];
		
		
		$update_prod = "UPDATE productos 
						SET 
						codigo_barra_prod=:codigo_barra_prod,
						nombre_producto=:nombre_producto,
						existencia_prod=:existencia_prod,
						precio_costo=:precio_costo,
						precio_dnm=:precio_dnm,
						precio_publico=:precio_publico,
						precio_venta=:precio_venta,
						presentacion=:presentacion,
						vitrina=:vitrina,
						marca=:marca,
						date_vence=:date_vence,
						indicaciones=:indicaciones,
						impuesto=:impuesto,
						cod_lab=:cod_lab,
						estado_prod=:estado_prod
						WHERE cod_producto=:codprod ";
		$update_prod = $DBcon->prepare($update_prod);
		$update_prod->bindparam(":codigo_barra_prod", $cod_barra,PDO::PARAM_STR);
		$update_prod->bindparam(":nombre_producto", $nombre_prod,PDO::PARAM_STR);
		$update_prod->bindparam(":presentacion", $presenta,PDO::PARAM_STR);
		$update_prod->bindparam(":marca",$marca,PDO::PARAM_STR);
		$update_prod->bindparam(":indicaciones", $indicacion,PDO::PARAM_STR);
		$update_prod->bindparam(":impuesto", $impuesto,PDO::PARAM_STR);
		$update_prod->bindparam(":estado_prod", $estado_prod,PDO::PARAM_STR);
		$update_prod->bindparam(":precio_costo", $p_costo,PDO::PARAM_STR);
		$update_prod->bindparam(":precio_dnm", $p_dnm,PDO::PARAM_STR);
		$update_prod->bindparam(":precio_publico", $p_public,PDO::PARAM_STR);
		$update_prod->bindparam(":precio_venta", $p_venta,PDO::PARAM_STR);
		$update_prod->bindparam(":date_vence", $f_vence,PDO::PARAM_STR);
		$update_prod->bindparam(":existencia_prod", $existencia,PDO::PARAM_STR);
		$update_prod->bindparam(":codprod", $codigo,PDO::PARAM_STR);
		$update_prod->bindparam(":vitrina", $vitrina,PDO::PARAM_STR);
		$update_prod->bindparam(":cod_lab", $cod_lab,PDO::PARAM_STR);
		
        if($update_prod->execute())
        {
            header("location: index.php?&texto=$texto");
        }
		else
        {
            echo "\nError al Actualizar los Datos:\n";
            print_r($update_prod->errorInfo());
            echo "<br>El Codigo de Error es: ". $update_prod->errorCode();
        }
	}
	catch(PDOException $e)
	{
		echo "ERROR AL ACTUALIZAR LOS DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL ACTUALIZAR LOS DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL ACTUALIZAR LOS DATOS 3 ".$s->getMessage();
        exit;
    }
}
elseif($_POST['SqlQuery']=='eliminar')
{
    
    try
	{
        $txt_codigo     = $_POST['EliminarID'];
		$texto			= "eliminar";
        
        
		$delete_prod = "DELETE FROM productos WHERE cod_producto=:cod_producto";
		$delete_kard = "DELETE FROM kardex WHERE cod_producto=:cod_producto";
		$delete_prod = $DBcon->prepare($delete_prod);
        $delete_kard = $DBcon->prepare($delete_kard);
        $delete_prod->bindParam(":cod_producto", $txt_codigo);
        $delete_kard->bindParam(":cod_producto", $txt_codigo);
        
        
        if($delete_prod->execute())
        {
            $delete_kard->execute();
            header("location: index.php?&texto=$texto");
        }
		else
        {
            echo "\nError al Eliminar los Datos:\n";
            print_r($delete_prod->errorInfo());
            echo "<br>El Codigo de Error es: ". $delete_prod->errorCode();
        }
	}
	catch(PDOException $e)
	{
		echo "ERROR AL ELIMINAR LOS DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL ELIMINAR LOS DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL ELIMINAR LOS DATOS 3 ".$s->getMessage();
        exit;
    }
}
else
{
    
}
?>