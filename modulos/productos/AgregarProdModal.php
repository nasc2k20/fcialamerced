<!-- Modal -->
<?php
include '../../conectar.php';

$sel_c = "SELECT MAX(cod_producto)AS cod_prod FROM productos";
$eje_c = $DBcon->prepare($sel_c);
$eje_c->execute();
$ver_c = $eje_c->fetch(PDO::FETCH_ASSOC);
$CodeProd = $ver_c['cod_prod'];

date_default_timezone_set("America/El_Salvador");

?>
<style>
    /********************************************************************/
/*** PANEL PRIMARY ***/
.with-nav-tabs.panel-primary .nav-tabs > li > a,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
    color: #fff;
}
.with-nav-tabs.panel-primary .nav-tabs > .open > a,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > .open > a:focus,
.with-nav-tabs.panel-primary .nav-tabs > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li > a:focus {
	color: #fff;
	background-color: #3071a9;
	border-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.active > a:focus {
	color: #428bca;
	background-color: #fff;
	border-color: #428bca;
	border-bottom-color: transparent;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu {
    background-color: #428bca;
    border-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a {
    color: #fff;   
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > li > a:focus {
    background-color: #3071a9;
}
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:hover,
.with-nav-tabs.panel-primary .nav-tabs > li.dropdown .dropdown-menu > .active > a:focus {
    background-color: #4a9fe9;
}
/********************************************************************/
</style>

<script>
    function formato_fecha_nac() {
	cadena = document.getElementById('fecha_vence').value;
	if(cadena.length == 2) { 
		document.getElementById('fecha_vence').value += "-";
	} else if (cadena.length == 5) {
		document.getElementById('fecha_vence').value += "-";
	} 
}

</script>

<div class="modal fade" id="AgregarProdModal" tabindex="-1" role="dialog" aria-labelledby="AgregarProdModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content modal-lg">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="AgregarProdModalLabel">Nuevo Producto</h4>
            </div>
            <div class="modal-body">

                <div class="panel with-nav-tabs panel-primary">
                    <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1primary" data-toggle="tab"><i class="fa fa-folder-open" aria-hidden="true"></i> Generalidades</a></li>
                            <li><a href="#tab2primary" data-toggle="tab"><i class="fa fa-usd" aria-hidden="true"></i> Precios</a></li>
                            <li><a href="#tab3primary" data-toggle="tab"><i class="fa fa-location-arrow" aria-hidden="true"></i> Ubicaci&oacute;n</a></li>
                            <li><a href="#tab4primary" data-toggle="tab"><i class="fa fa-calendar" aria-hidden="true"></i> Saldos y Vencimientos</a></li>
                        </ul>
                    </div>
                    <div class="panel-body">
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab1primary">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="codigo">Codigo: </label>
                                        <input type="text" class="form-control" id="codigo" value="<?php echo $CodeProd+1; ?>" readonly>
                                    </div>
                                    <div class="col-sm-7">
                                        <label for="codigo_barra">Codigo de Barra: </label>
                                        <input type="text" class="form-control" id="codigo_barra" placeholder="Codigo de Barra">
                                    </div>
                                </div>

                                <div style="height:3rem;"></div>

                                <div class="row">
                                    <div class="col-sm-10">
                                        <label for="nombre_producto">Nombre del Producto: </label>
                                        <input type="text" class="form-control" id="nombre_producto" placeholder="Nombre del Producto" style="text-transform:uppercase">
                                    </div>
                                </div>

                                <div style="height:3rem;"></div>

                                <div class="row">
                                    <div class="col-md-5">
                                        <label for="indicaciones">Indicaciones: </label>
                                        <textarea id="indicaciones" cols="30" rows="5" class="form-control" placeholder="Indicaciones" style="resize:none;"></textarea>

                                    </div>
                                    <div class="col-sm-5">
                                        <label for="laboratorio">Laboratorio: </label>
                                        <?php
                                    require("../../conectar.php");
                                    $sel_lab = "SELECT * 
                                                FROM laboratorios 
                                                ORDER BY nombre_lab ASC";
                                    $eje_lab = $DBcon->prepare($sel_lab);
                                    $eje_lab->execute();
                                    ?>
                                        <select id="laboratorio" class="form-control">
                                            <?php
                                    while($ver_lab = $eje_lab->fetch(PDO::FETCH_ASSOC))
                                    {
                                    ?>
                                            <option value="<?php echo $ver_lab["cod_lab"]; ?>">
                                                <?php echo $ver_lab["nombre_lab"]; ?>
                                            </option>
                                            <?php
                                    }
                                    ?>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="tab-pane fade" id="tab2primary">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <label for="precio_dnm">Precio DNM: </label>
                                        <input type="number" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" id="precio_dnm" placeholder="0.00">
                                    </div>

                                    <div class="col-sm-3">
                                        <label for="precio_publico">Precio Publico: </label>
                                        <input type="number" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" id="precio_publico" placeholder="0.00">
                                    </div>

                                    <div class="col-sm-4">
                                        <label for="precio_costo">Costo: </label>
                                        <input type="number" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" id="precio_costo" placeholder="0.00">
                                    </div>
                                </div>

                                <div style="height:3rem;"></div>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="precio_venta">Precio de Venta: </label>
                                        <input type="number" min="0" step="0.01" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" id="precio_venta" placeholder="0.00">
                                    </div>
                                </div>
                            </div>


                            <div class="tab-pane fade" id="tab3primary">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label for="presentacion">Tipo Presentacion: </label>
                                        <select id="presentacion" class="form-control">
                                            <option value="Caja">Caja</option>
                                            <option value="Unidad">Unidad</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-3">
                                        <label for="vitrina">Vitrina: </label>
                                        <input type="text" class="form-control" id="vitrina" placeholder="Vitrina" maxlength="2">
                                    </div>
                                    <div class="col-sm-3">
                                        <label for="impuesto">Impuesto: </label>
                                        <select id="impuesto" class="form-control">
                                            <option value="iva">SI</option>
                                            <option value="sin">NO</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="tab-pane fade" id="tab4primary">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <label for="existencia">Existencia:</label>
                                        <input type="number" min="0" step="1" data-number-to-fixed="2" data-number-stepfactor="100" class="form-control" id="existencia" placeholder="1">
                                    </div>

                                    <div class="col-sm-5">
                                        <label for="fecha_vence">Fecha Vence: </label>
                                        <input type="text" class="form-control" id="fecha_vence" placeholder="00-00-0000" maxlength="10" onKeyPress="formato_fecha_nac();" required>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-10">
                                        <label for="estado_prod">Estado:</label>
                                        <select id="estado_prod" class="form-control">
                                            <option value="s" selected>Activo</option>
                                            <option value="n">Inactivo</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="AgregarProd();"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>
