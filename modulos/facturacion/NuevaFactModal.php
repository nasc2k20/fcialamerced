<div class="modal fade" tabindex="-1" role="dialog" id="NuevaFacturaProdModal">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nuevo Proveedor</h4>
            </div>
            <div class="modal-body">

                <div id="HeaderNuevaFactura"></div>

                <div style="height:10px;"></div>
                <!--
                <div id="CargarAgregarProductos"></div>
                -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
