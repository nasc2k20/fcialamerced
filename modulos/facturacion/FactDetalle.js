$(function () {
    $('#ResultadoAutocompleteFact').hide();

    $('#BuscarProdFactDet').typeahead({

        source: function (query, result) {
            $.ajax({
                url: "modulos/facturacion/ProductosAutcomplete.php",
                data: 'query=' + query,
                dataType: "json",
                type: "POST",
                success: function (data) {
                    result($.map(data, function (item) {
                        return item;
                    }));
                }
            });
        },
        itemSelected: function (data, value, text) {
            console.log(data + ' ' + value + ' ' + text);
        }
    });


    $('#BuscarProdFactDet').keypress(function (e) {

        var Code = (e.keyCode ? e.keyCode : e.which);
        if (Code == 13) {
            CargarCodigo($(this).val());
        } else {
            $('#ResultadoAutocompleteFact').hide();
        }
    })

    $('#BtnAgregarFactProd').click(function () {
        var Datos = $('#BuscarProdFactDet').val();
        CargarCodigo(Datos);

    });

    
    $('#CantidadVenta').keypress(function (e) {
        var Code = (e.keyCode ? e.keyCode : e.which);
        if (Code == 13) {
            if ($(this).val() == '') {
                alert('No has digitado la Cantidad a Vender');
            } else if($(this).val()==0){
                alert('La Cantidad a Vender NO puede ser Cero');
            } else {
                $('#PrecioVenta').focus();
            }
        }
    });

    $('#PrecioVenta').keypress(function (e) {
        var Code = (e.keyCode ? e.keyCode : e.which);
        if (Code == 13) {
            if ($(this).val() == '') {
                alert('Debes de Digitar un Precio');
            } else if ($(this).val()==0) {
                alert('El Precio NO puede ser Cero');
            } else {
                $('#AgregarVentaDet').focus();
            }
        }
    });





})


function GuardarVentaTotal(CodVenta)
{
    var FechaVenta = $('#FactFecha').val();
    var CodVendedor = $('#FactVendedor').val();
    var FactDocumento = $('#FactDocumento').val();
    var TipoPago = $('#FactTipoPago').val();
    var NombreCliente = $('#FactNombre').val();
    var SqlQuery = 'insertar';
    
    var Parametros = {
        "CodVenta": CodVenta,
        "FechaVenta": FechaVenta,
        "FactDocumento": FactDocumento,
        "CodVendedor": CodVendedor,
        "TipoPago": TipoPago,
        "NombreCliente": NombreCliente,
        "SqlQuery": SqlQuery
    };
    
    if(FechaVenta==''){
        alert('Debe de Seleccionar fecha de Venta');
        $('#FactFecha').focus();
    } else if(TipoPago==''){
        alert('Debe de Seleccionar el Tipo de Pago');
        $('#FactTipoPago').focus();
    } else if(CodVendedor==''){
        alert('Debe de Seleccionar el Vendedor');
        $('#FactVendedor').focus();
    } else if(CodVenta==''){
        alert('Debe de Seleccionar el Codigo de Venta');
    } else{
        $.ajax({
            url : "modulos/facturacion/ProcesosSQL.php", 
            method : "POST",
            data : Parametros,
            beforeSend : function(){
                $("#MostrarDatosFacturas").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
            },
            success : function (data){
                $('#NuevaFacturaProdModal').modal('hide');

                if ($('.modal-backdrop').is(':visible')) {
                    $('.modal-backdrop').remove();
                };

                $("#MostrarDatosFacturas").html(data);
            }
        })
    }
    
    
    
    
}


function CargarCodigo(CodProducto) {
    var CodeData = '';
    var NombreData = '';
    var PrecioData = '';
    var VenceData = '';
    var ExisteData = '';
    var arrreglo = CodProducto.split(':');
    CodeData = arrreglo[1];
    NombreData = arrreglo[0];
    PrecioData = arrreglo[2];
    VenceData = arrreglo[3];
    ExisteData = arrreglo[4];
    
    $('#CodProdFactDet').val(CodeData);
    $('#NombreProdFactDet').val(NombreData);
    $('#PrecioVenta').val(PrecioData);
    $('#VenceVenta').val(VenceData);
    $('#ExistenciaVenta').val(ExisteData);

    if ($('#CodProdFactDet').val() == '') {
        alert('No Hay datos que mostrar');
        $('#ResultadoAutocompleteFact').hide();
    } else {
        //alert('Producto Elegido es: '+RealData);
        $('#ResultadoAutocompleteFact').show();
        $('#CantidadVenta').focus();
        $('#BuscarProdFactDet').val(NombreData);
        
    }
}





function AgregarVentaDetProd() {
    var CodeProd = $('#CodProdFactDet').val();
    var NombreProd = $('#NombreProdFactDet').val();
    var CantProd = $('#CantidadVenta').val();
    var PrecioProd = $('#PrecioVenta').val();
    var VenceProd = $('#VenceVenta').val();
    var CodFactura = $('#CodFactura').val();
    var Existencia = $('#ExistenciaVenta').val();
    var SqlQuery = 'verificar';
    
    
    if(Existencia>0)
        {
            var Parametros = {
                "CodeProd": CodeProd,
                "NombreProd": NombreProd,
                "CantProd": CantProd,
                "PrecioProd": PrecioProd,
                "VenceProd": VenceProd,
                "CodFactura": CodFactura,
                "SqlQuery": SqlQuery
            };


            if (CodeProd == '') {
                alert('El Codigo del Producto esta Vacio');
            } else if (NombreProd == '') {
                alert('El Nombre del Producto esta Vacio');
            } else if (CantProd == '') {
                alert('No has digitado la Cantidad a Vender');
                $('#CantidadVenta').focus();
            } else if (PrecioProd == '') {
                alert('Debe de Digitar el Precio de Venta');
                $('#PrecioVenta').focus();
            } else if (VenceProd == '') {
                alert('Debe de Digitar La Fecha de Vencimiento');
            } else if (CodFactura == '') {
                alert('No Has Seleccionado el Codigo de Venta');
            } else {
                $.ajax({
                    url: "modulos/facturacion/ProcesosSQL.php",
                    method: "POST",
                    data: Parametros,
                    beforeSend: function () {
                        $("#DetalleNuevaFactura").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
                    },
                    success: function (data) {
                        $("#DetalleNuevaFactura").html(data);
                        $("#ResultadoAutocompleteFact").hide();
                        $("#BuscarProdFactDet").focus();
                    }
                });
            }
        }
    else
        {
            alert('El Producto No tiene Existencia Registrada');
            $("#ResultadoAutocompleteFact").hide();
            $("#BuscarProdFactDet").focus();
        }
}
