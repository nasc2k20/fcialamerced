<?php
require_once '../../conectar.php';
$no = 1;

date_default_timezone_set("America/El_Salvador");

$FechaActual = date('Y-m-d');
$AcumTot = 0;

if (isset($_POST['query']))
{
    //echo "aqui";
    
    $fVentaForm = new datetime($_POST['query']);
    $sql_busqueda = $fVentaForm->format('Y-m-d');
    
    $sel_fact = "SELECT * FROM ventas 
                WHERE fecha_modif = '$sql_busqueda' AND venta_anular='NO' 
                ORDER BY date_venta DESC";
}
else
{
    $sel_fact = "SELECT * FROM ventas 
                WHERE venta_anular='NO' AND fecha_venta='$FechaActual'
                ORDER BY date_venta DESC";
}


$eje_fact = $DBcon->prepare($sel_fact);
$eje_fact->execute();
?>
       
       <div class="modal fade bs-example-modal-lg" id="DetalleFactura" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Detalle de Factura</h4>
                    </div>
                    <div class="modal-body">
                        <div id="loader" style="position: absolute;	text-align: center;	top: 55px;	width: 100%;display:none;"></div><!-- Carga gif animado -->
                        <div id="FactDetLista"></div><!-- Datos ajax Final -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle-o" aria-hidden="true"></i> Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
       
       
       
        <table id="dataTables1" class="table table-bordered table-striped table-hover">
        <thead>
          <tr>
            <th class="center">No.</th>
            <th class="center">Fecha</th>
            <th class="center">Recibo</th>
            <th>Nombre de Cliente</th>
            <th class="center">Total</th>
            <th class="center" colspan="2" width="10%">Acciones</th>
          </tr>
        </thead>
        <tbody>
        <?php
        while ($ver_fact = $eje_fact->fetch(PDO::FETCH_ASSOC)) {
       ?>
          <tr>
                  <td width='5%' class='center'><?php echo $no; ?></td>
                  <td width='15%'><?php $fecha_c_v = new datetime($ver_fact["fecha_venta"]); echo $fecha_c_v->format('d-m-Y') ?></td>
                  <td class='center' width='10%'><?php echo ""; ?></td>
                  <td width='40%'><?php echo $ver_fact["nombre_cliente"]; ?></td>
                  <td class='center' width='15%' align='right'>$ <?php echo number_format($ver_fact["total_venta"],3); ?></td>
                  <td class='center' width='5%'>
                      <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#DetalleFactura" onclick="VerFacturaVtaDet('<?php echo $ver_fact['cod_venta']; ?>');"><i class="fa fa-search" aria-hidden="true"></i> </button>
                  </td>
                  <td class='center' width='5%' data-toggle="tooltip" data-placement="top" title="Anular">
                      <button class="btn btn-warning btn-sm" onclick="AnularFactura('<?php echo $ver_fact['cod_venta']; ?>')"><i class="fa fa-download" aria-hidden="true"></i> </button>
                  </td>
                </tr>
        <?php
          $no++;
            $AcumTot += $ver_fact['total_venta'];
        }
        ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="7"></td>
            </tr>
            <tr>
                <td colspan="7"><h4>Valor de Venta: $<strong><?php echo number_format($AcumTot,4); ?></strong></h4></td>
            </tr>
        </tfoot>
      </table>
