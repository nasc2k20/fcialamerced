<?php
require_once '../../conectar.php';
$no = 1;

$counter_subto = 0;
$counter_desci = 0;
$sub_total = 0;
$iva_compra = 0;
//$cod_nfactu = $_GET["nfactu"];

$cod_nfactu = $_POST["CodFact"];
    
$sel_nfact_temp = "SELECT * FROM ventas_detalle  
                  WHERE cod_venta=$cod_nfactu 
                  ORDER BY cod_det_vta ASC";
$eje_nfact_temp = $DBcon->prepare($sel_nfact_temp);
$eje_nfact_temp->execute();
?>
       <br>
       <span class="pull-right">
       <button class="btn btn-primary btn-sm"><i class="fas fa-print"></i></button>
       </span>
        <table class="table table-borderless table-striped table-hover" style="font-size: 10pt;">
        <thead>
          <tr>
            <th class="center">Cant2</th>
            <th class="center">Nombre de Producto</th>
            <th class="center">Precio Vta</th>
            <th class="center">Vence</th>
            <th class="center">Total</th>
            <th class="center"></th>
          </tr>
        </thead>
        <tbody>
        <?php
        while ($ver_nfact_temp = $eje_nfact_temp->fetch(PDO::FETCH_ASSOC)) {
       ?>
          <tr style="height: 5px;">
                  <td class='center' width='8%'><?php echo $ver_nfact_temp["cant_vta"]; ?></td>
                  <td class='center' width='42%'><?php echo $ver_nfact_temp["nombre_prod"]; ?></td>
                  <td class='center' width='15%'>$ <?php echo $ver_nfact_temp["precio_vta"]; ?></td>
                  <td class='center' width='15%'><?php $fvencefor = new datetime($ver_nfact_temp["vence_prod"]); echo $fvencefor->format('d-m-Y'); ?></td>
                  <td class='center' width='15%'>$ <?php echo $ver_nfact_temp["total_vta"]; ?></td>
                  <td class='center' width='5%' data-toggle="tooltip" data-placement="top" title="Eliminar">
                    <button class="btn btn-danger btn-sm" onclick="EliminarProdDetVenta('<?php echo $ver_nfact_temp['cod_det_vta']; ?>','<?php echo $ver_nfact_temp["cod_prod"]; ?>','<?php echo $ver_nfact_temp['nombre_prod']; ?>','<?php echo $cod_nfactu; ?>')"><i class="fa fa-trash" aria-hidden="true" title="Eliminar"></i></button>
                  </td>
            </tr>
        <?php
            $counter_subto += $ver_nfact_temp["total_vta"];
          $no++;
        }
        ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="6" width='75%'></td>
            </tr>
            <tr>
                <td colspan="4" width='75%'></td>
                <td  width='10%'>Sumas: </td>
                <td  width='10%'>$<?php echo number_format($counter_subto,2); ?></td>
            </tr>
            <tr>
                <td colspan="4" width='75%'></td>
                <td  width='10%'>Exentas: </td>
                <td  width='10%'>$<?php echo "0.00"; ?></td>
            </tr>
               <tr>
                <td colspan="4" width='75%'></td>
                <td  width='10%'>Total: </td>
                <td  width='10%'>$<?php echo number_format($counter_subto,2); ?></td>
            </tr>
        </tfoot>
      </table>
