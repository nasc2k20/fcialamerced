$(function () {
    NuevaFacturaHeader();
    
});

function NuevaFacturaHeader() {
    $.ajax({
        url: "modulos/facturacion/NuevaFactHeader.php",
        method: "POST",
        data: {
            query: ""
        },
        beforeSend: function () {
            $("#HeaderNuevaFactura").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
        },
        success: function (data) {
            $('#HeaderNuevaFactura').html(data);
            var CodFact = $('#CodFactura').val();
        
            CargarFactProdDet(CodFact);
            
        }
    });
}


function CargarFactProdDet(CodFact) {
    $.ajax({
        url: "modulos/facturacion/ver_factura_detalle.php",
        method: "POST",
        data: {
            CodFact: CodFact
        },
        beforeSend: function () {
            $("#DetalleNuevaFactura").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
        },
        success: function (data) {
            $('#DetalleNuevaFactura').html(data);
        }
    });
}


function EliminarProdDetVenta(CodDetVenta, CodigoProd, NombreProd, CodFactura) {
    var SqlQuery = 'eliminar';
    var Parametros = {
        "SqlQuery": SqlQuery,
        "EliminarID": CodigoProd,
        "CodDetVenta": CodDetVenta,
        "CodFactura": CodFactura
    };

    $.confirm({
        icon: 'fa fa-smile-o',
        title: 'Eliminaci&oacute;n',
        theme: 'modern',
        closeIcon: true,
        columnClass: 'col-md-4 col-md-offset-4',
        content: 'Desea Eliminar El producto: <br> <strong><h3>' + NombreProd + ' de la Factura #' + CodFactura + '</h3></strong>',
        animation: 'scale',
        icon: 'fa fa-warning',
        type: 'orange',
        draggable: true,
        buttons: {
            ok: {
                text: 'Confirmar',
                btnClass: 'btn-blue',
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: "modulos/facturacion/ProcesosSQL.php",
                        data: Parametros,
                        beforeSend: function (objeto) {
                            $("#DetalleNuevaFactura").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
                        },
                        success: function (datos) {
                            $("#DetalleNuevaFactura").html(datos);
                        }
                    });
                },
            },
            cancel: {
                text: 'Cancelar',
                btnClass: 'btn-red',
                action: function () {}
            }
        }
    });
}




function AnularFactura(CodFactura) {
    var SqlQuery = 'anular';
    var Parametros = {
        "SqlQuery": SqlQuery,
        "EliminarID": CodFactura
    };

    $.confirm({
        icon: 'fa fa-smile-o',
        title: 'Anulaci&oacute;n',
        theme: 'modern',
        closeIcon: true,
        columnClass: 'col-md-4 col-md-offset-4',
        content: 'Desea Anular la Factura Numero: <br> <strong><h3>' + CodFactura + ' </h3></strong>',
        animation: 'scale',
        icon: 'fa fa-check-circle',
        type: 'blue',
        draggable: true,
        buttons: {
            ok: {
                text: 'Confirmar',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: "modulos/facturacion/ProcesosSQL.php",
                        data: Parametros,
                        beforeSend: function (objeto) {
                            $("#MostrarDatosFacturas").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
                        },
                        success: function (datos) {
                            $("#MostrarDatosFacturas").html(datos);
                        }
                    });
                },
            },
            cancel: {
                text: 'Cancelar',
                btnClass: 'btn-gray',
                action: function () {}
            }
        }
    });
}