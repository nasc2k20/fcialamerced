<?php
include '../../login/validar.php';
require '../../conectar.php';


date_default_timezone_set("America/El_Salvador");

$usuario	= $_SESSION["usuario"];
//$usuario = "admin";


if($_POST['SqlQuery']=='verificar')
{
    try
	{   
        $cod_nfactu = $_POST["CodFactura"];
        $cod_prod	= $_POST["CodeProd"];
        $CantVta = $_POST['CantProd'];
        $PrecioVta = $_POST['PrecioProd'];
        $VenceVta = $_POST['VenceProd'];
        $NombreProdVta = strtoupper($_POST['NombreProd']);
        $precio_cto = 0;
        $tot_prod = 0;
	
	
        $sel_prods = "SELECT * FROM productos WHERE cod_producto='$cod_prod'";
        $eje_prods = $DBcon->prepare($sel_prods);
        $eje_prods->execute();
        $ver_prods = $eje_prods->fetch(PDO::FETCH_ASSOC);
        $precio_cto = $ver_prods["precio_costo"];
        $PrecioPub = ($ver_prods["precio_publico"]=='') ? '0.00' : $ver_prods["precio_publico"];
        $tot_prod	= number_format($PrecioVta*$CantVta,4,'.','');
        
        
        $sel_prod_ft = "SELECT * FROM ventas_detalle 
                        WHERE cod_prod='$cod_prod' AND cod_venta='$cod_nfactu'";
        $eje_prod_ft = $DBcon->prepare($sel_prod_ft);
        $eje_prod_ft->execute();
        $con_prod_ft = $eje_prod_ft->rowCount();
        $ver_prod_ft = $eje_prod_ft->fetch(PDO::FETCH_ASSOC);
        $cant_vt = $ver_prod_ft["cant_vta"];
        $prec_vt = $ver_prod_ft["precio_vta"];
        $cant_tot_vf = 0;
        $prec_tot_vf = 0;

        if($con_prod_ft>0)
        {
            $cant_tot_vf = $cant_vt+$CantVta;
            $prec_tot_vf = number_format($cant_tot_vf*$prec_vt,4,'.','');
            $insert_fact = "UPDATE ventas_detalle 
                           SET 
                           cant_vta=:cant_vta,
                           total_vta=:total_vta  
                           WHERE cod_prod='$cod_prod' AND cod_venta='$cod_nfactu'";
            $insert_fact = $DBcon->prepare($insert_fact);
            $insert_fact->bindparam(":cant_vta", $cant_tot_vf ,PDO::PARAM_STR);
            $insert_fact->bindparam(":total_vta", $prec_tot_vf,PDO::PARAM_STR);
        }
        else
        {
            $insert_fact = "INSERT INTO ventas_detalle 
                           (cod_prod,vence_prod,
                           cant_vta,nombre_prod,precio_pub_vta,precio_vta,
                           total_vta,cod_venta,costo_vta) 
                           VALUES (:cod_prod,:vence_prod,:cant_vta,:nombre_prod,
                           :precio_pub_vta,:precio_vta,:total_vta,:cod_venta,:costo_vta)";
            $insert_fact = $DBcon->prepare($insert_fact);
            $insert_fact->bindparam(":nombre_prod", $NombreProdVta,PDO::PARAM_STR);
            $insert_fact->bindparam(":vence_prod", $VenceVta,PDO::PARAM_STR);
            $insert_fact->bindparam(":precio_pub_vta", $PrecioPub,PDO::PARAM_STR);
            $insert_fact->bindparam(":costo_vta", $precio_cto,PDO::PARAM_STR);
            $insert_fact->bindparam(":precio_vta", $PrecioVta,PDO::PARAM_STR);
            $insert_fact->bindparam(":total_vta", $tot_prod,PDO::PARAM_STR);
            $insert_fact->bindparam(":cant_vta", $CantVta,PDO::PARAM_STR);
            $insert_fact->bindparam(":cod_venta", $cod_nfactu,PDO::PARAM_STR);
            $insert_fact->bindparam(":cod_prod", $cod_prod,PDO::PARAM_STR);
        }
		
        if($insert_fact->execute())
        {
            header("location: index.php");
        }
        else
        {
            //echo '<script> alert("Error al Eliminar los Datos: '.$DBcon->errorInfo().' El Codigo de Error es: '.$DBcon->errorCode().' ")</script>';
            echo "\nError al Verificar los Datos:\n";
            print_r($insert_fact->errorInfo());
            echo "<br>El Codigo de Error es: ". $insert_fact->errorCode();
        }
	}
	catch(PDOException $e)
	{
		echo "ERROR AL VERIFICAR LOS DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL VERIFICAR LOS DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL VERIFICAR LOS DATOS 3 ".$s->getMessage();
        exit;
    }
}
elseif($_POST['SqlQuery']=='insertar')
{   
    $ValorInsert = 0;
    $cod_nfactu	= $_POST["CodVenta"];
    
    $sel_tot = "SELECT * FROM ventas_detalle WHERE cod_venta=:coddetvta";
    $eje_tot = $DBcon->prepare($sel_tot);
    $eje_tot->execute(array(":coddetvta"=> $cod_nfactu));
    $con_tot = $eje_tot->rowCount();

        
    if($con_tot>0)
    {
        try
        {
            $NewFechaFact = date('Y-m-d');
            $fi_format = new datetime($_POST["FechaVenta"]);
            $fecha_vta	= $fi_format->format('Y-m-d');
            $fecha_det	= date('Y-m-d H:i:s');
            $cod_nfactu	= $_POST["CodVenta"];
            $nombre_cli	= ($_POST["NombreCliente"]=='') ? ' ' : strtoupper($_POST["NombreCliente"]);
            $vendedor	= $_POST["CodVendedor"];
            $documento	= ($_POST["FactDocumento"]=='') ? '0' : $_POST["FactDocumento"];
            $tipo_pago	= $_POST["TipoPago"];
            $vta_anula	= "NO";


            $sel_tot = "SELECT * FROM ventas_detalle WHERE cod_venta=:coddetvta";
            $eje_tot = $DBcon->prepare($sel_tot);
            $eje_tot->execute(array(":coddetvta"=> $cod_nfactu));
            $total_fact =0;

            while($ver_tot = $eje_tot->fetch(PDO::FETCH_ASSOC))
            {
                $total_fact += number_format($ver_tot["total_vta"],4,'.','');
            }
            $venta_neta = number_format($total_fact/1.13,4,'.','');
            $venta_iva	= number_format(($venta_neta*13)/100,4,'.','');




            $insert_fvta = "INSERT INTO ventas (fecha_venta,date_venta,nombre_cliente,
                            tipo_pago,sumas_vta,iva_vta,total_venta,
                            cod_empleado,venta_anular,usuario,fecha_modif) 
                            VALUES (:fecha_venta,:date_venta,:nombre_cliente,
                            :tipo_pago,:sumas_vta,:iva_vta,:total_venta,
                            :cod_empleado,:venta_anular,:usuario,:fecha_modif)";
            $insert_fvta = $DBcon->prepare($insert_fvta);
            $insert_fvta->bindparam(":venta_anular", $vta_anula);
            $insert_fvta->bindparam(":usuario", $usuario);
            $insert_fvta->bindparam(":cod_empleado", $vendedor);
            $insert_fvta->bindparam(":fecha_venta", $NewFechaFact);
            $insert_fvta->bindparam(":fecha_modif", $fecha_vta);
            $insert_fvta->bindparam(":date_venta", $fecha_det);
            $insert_fvta->bindparam(":nombre_cliente", $nombre_cli);
            $insert_fvta->bindparam(":tipo_pago", $tipo_pago);
            $insert_fvta->bindparam(":sumas_vta", $venta_neta);
            $insert_fvta->bindparam(":iva_vta", $venta_iva);
            $insert_fvta->bindparam(":total_venta", $total_fact);

            if($insert_fvta->execute())
            {
                $texto = "venta";

                $sel_exis = "SELECT * FROM productos a 
                             INNER JOIN ventas_detalle b ON a.cod_producto=b.cod_prod
                             WHERE b.cod_venta=:coddetvta";
                $eje_exis = $DBcon->prepare($sel_exis);
                $eje_exis->execute(array(":coddetvta"=> $cod_nfactu));

                while($ver_exis = $eje_exis->fetch(PDO::FETCH_ASSOC))
                {
                    $cod_produ = $ver_exis["cod_producto"];
                    $existe_prod = $ver_exis["existencia_prod"];
                    $costo_prod = $ver_exis["precio_costo"];
                    $precio_prod = $ver_exis["precio_venta"];
                    $cant_venta	= $ver_exis["cant_vta"];
                    $total_exis = $existe_prod-$ver_exis["cant_vta"];

                    $referencia = "VENTA DE PRODUCTO FACT# ".$cod_nfactu;
                    $entrada = 0;

                    $insert_kar = "INSERT INTO kardex (fecha_var,fecha_tot,cod_producto,
                                num_factura,costo_prod_kardex,precio_vta_kardex,
                                referencia_kardex,entrada_kardex,salida_kardex,total,
                                usuario)VALUES (:fecha_var,:fecha_tot,:cod_producto,
                                :num_factura,:costo_prod_kardex,:precio_vta_kardex,
                                :referencia_kardex,:entrada_kardex,:salida_kardex,:total,
                                :usuario)";
                    $insert_kar = $DBcon->prepare($insert_kar);
                    $insert_kar->bindparam(":fecha_var", $NewFechaFact);
                    $insert_kar->bindparam(":referencia_kardex", $referencia);
                    $insert_kar->bindparam(":usuario", $usuario);
                    $insert_kar->bindparam(":costo_prod_kardex", $costo_prod);
                    $insert_kar->bindparam(":precio_vta_kardex", $precio_prod);
                    $insert_kar->bindparam(":fecha_tot", $fecha_det);
                    $insert_kar->bindparam(":cod_producto", $cod_produ);
                    $insert_kar->bindparam(":num_factura", $cod_nfactu);
                    $insert_kar->bindparam(":entrada_kardex", $entrada);
                    $insert_kar->bindparam(":salida_kardex", $cant_venta);
                    $insert_kar->bindparam(":total", $total_exis);



                    $update_exis = "UPDATE productos 
                                  SET 
                                  existencia_prod=:txt_existencia 
                                  WHERE cod_producto=:codproducto";
                    $update_exis = $DBcon->prepare($update_exis);
                    $update_exis->bindparam(":codproducto", $cod_produ,PDO::PARAM_STR);
                    $update_exis->bindparam(":txt_existencia", $total_exis,PDO::PARAM_STR);

                    if($update_exis->execute())
                    {
                        $insert_kar->execute();
                        $ValorInsert = 1;
                    }
                    else
                    {
                        echo "\nError al Editar los Datos:\n";
                        print_r($update_exis->errorInfo());
                        echo "<br>El Codigo de Error es: ". $update_exis->errorCode();
                        echo "\nError al Editar los Datos:\n";
                        print_r($insert_kar->errorInfo());
                        echo "<br>El Codigo de Error es: ". $insert_kar->errorCode();
                    }

                }
            }
            else
            {
                echo "\nError al Editar los Datos:\n";
                print_r($insert_fvta->errorInfo());
                echo "<br>El Codigo de Error es: ". $insert_fvta->errorCode();
            }




            if($ValorInsert>0)
            {
                header('location: index.php');
            }
        }
        catch(PDOException $e)
        {
            echo "ERROR AL INSERTAR LOS DATOS ".$e->getMessage(); 
            exit;		
        }
        catch (Throwable $t)
        {
            echo "ERROR AL INSERTAR LOS DATOS 2 ".$t->getMessage();
            exit;
        }
        catch (Exception $s)
        {
            echo "ERROR AL INSERTAR LOS DATOS 3 ".$s->getMessage();
            exit;
        }
    }
    else
    {
        ?>
        <script>
            alert("No se puede Guardar una Factura sin Productos.");
        </script>
        <?php
    }
    
    
    
    
    
    
}
elseif($_POST['SqlQuery']=='anular')
{
    try
	{
        $ValorInsert = 0;
        $PrecioCosto = 0;
        $PrecioVenta = 0;
        $CodFactura = $_POST["EliminarID"];
        $venta_anular = "SI";
        $texto = "anulada";
        $txt_fecha = date('Y-m-d');
        $fecha_det = date('Y-m-d H:i:s');
        $referencia = "ANULACION DE VENTA FACT# ".$CodFactura;
        $salida = 0;
        
        $upda_venta = "UPDATE ventas 
                        SET 
                        venta_anular=:venta_anular  
                        WHERE cod_venta=:cod_venta";
        $upda_venta = $DBcon->prepare($upda_venta);
        $upda_venta->bindparam(":venta_anular", $venta_anular,PDO::PARAM_STR);
        $upda_venta->bindparam(":cod_venta", $CodFactura,PDO::PARAM_STR);
                
        
        if($upda_venta->execute())
        {
            $sel_datvta = "SELECT * FROM ventas_detalle WHERE cod_venta=$CodFactura";
            $eje_datvta = $DBcon->prepare($sel_datvta);
            $eje_datvta->execute();

            while($ver_datvta = $eje_datvta->fetch(PDO::FETCH_ASSOC))
            {
                $sel_datprod = "SELECT * FROM productos 
                                WHERE cod_producto='$ver_datvta[cod_prod]'";
                $eje_datprod = $DBcon->prepare($sel_datprod);
                $eje_datprod->execute();
                $ver_datprod = $eje_datprod->fetch(PDO::FETCH_ASSOC);
                $PrecioCosto = $ver_datprod['precio_costo'];
                $existe_prod = $ver_datprod["existencia_prod"];
                
                $tot_venta = $ver_datvta["cant_vta"];
                $PrecioVenta = $ver_datvta['precio_vta'];

                $total_existe = $existe_prod+$tot_venta;

                $upda_existe = "UPDATE productos 
                                SET 
                                existencia_prod=:tot_existe  
                                WHERE cod_producto=:cod_prod";
                $upda_existe = $DBcon->prepare($upda_existe);
                $upda_existe->bindparam(":cod_prod", $ver_datprod["cod_producto"],PDO::PARAM_STR);
                $upda_existe->bindparam(":tot_existe", $total_existe,PDO::PARAM_STR);
                
                
                

                $insert_kar = "INSERT INTO kardex (fecha_var,fecha_tot,cod_producto,
                                num_factura,costo_prod_kardex,precio_vta_kardex,
                                referencia_kardex,entrada_kardex,salida_kardex,total,
                                usuario)VALUES (:fecha_var,:fecha_tot,:cod_producto,
                                :num_factura,:costo_prod_kardex,:precio_vta_kardex,
                                :referencia_kardex,:entrada_kardex,:salida_kardex,:total,
                                :usuario)";
                $insert_kar = $DBcon->prepare($insert_kar);
                $insert_kar->bindparam(":fecha_var", $txt_fecha);
                $insert_kar->bindparam(":referencia_kardex", $referencia);
                $insert_kar->bindparam(":usuario", $usuario);
                $insert_kar->bindparam(":costo_prod_kardex", $PrecioCosto);
                $insert_kar->bindparam(":precio_vta_kardex", $PrecioVenta);
                $insert_kar->bindparam(":fecha_tot", $fecha_det);
                $insert_kar->bindparam(":cod_producto", $ver_datprod["cod_producto"]);
                $insert_kar->bindparam(":num_factura", $CodFactura);
                $insert_kar->bindparam(":entrada_kardex", $tot_venta);
                $insert_kar->bindparam(":salida_kardex", $salida);
                $insert_kar->bindparam(":total", $total_existe);
                

                if($insert_kar->execute())
                {
                    $upda_existe->execute();
                    $ValorInsert = 1;
                }
                else
                {
                    echo "\nError al Editar los Datos:\n";
                    print_r($upda_existe->errorInfo());
                    echo "<br>El Codigo de Error es: ". $upda_existe->errorCode();
                }
            }
        }
        else
        {
            echo "\nError al Editar los Datos:\n";
            print_r($upda_venta->errorInfo());
            echo "<br>El Codigo de Error es: ". $upda_venta->errorCode();
        }
        
        if($ValorInsert>0)
        {
            header("location: index.php");
        }
        


	}
	catch(PDOException $e)
	{
		echo "ERROR AL ANULAR LOS DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL ANULAR LOS DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL ANULAR LOS DATOS 3 ".$s->getMessage();
        exit;
    }
}
elseif($_POST['SqlQuery']=='eliminar')
{
    
    try
	{
        $txt_codigo     = $_POST['EliminarID'];
        $txt_venta     = $_POST['CodFactura'];
        $txt_detalle    = $_POST['CodDetVenta'];
		$texto			= "eliminar";
        
        
		$delete_prod = "DELETE FROM ventas_detalle WHERE cod_det_vta=:cod_det_vta AND cod_venta=:cod_venta AND cod_prod=:cod_prod";
		$delete_prod = $DBcon->prepare($delete_prod);
        $delete_prod->bindParam(":cod_prod", $txt_codigo);
        $delete_prod->bindParam(":cod_venta", $txt_venta);
        $delete_prod->bindParam(":cod_det_vta", $txt_detalle);
        
        if($delete_prod->execute())
        {
            header("location: index.php?texto=$texto");
        }
		else
        {
            //echo '<script> alert("Error al Eliminar los Datos: '.$DBcon->errorInfo().' El Codigo de Error es: '.$DBcon->errorCode().' ")</script>';
            echo "\nError al Eliminar los Datos:\n";
            print_r($delete_prod->errorInfo());
            echo "<br>El Codigo de Error es: ". $delete_prod->errorCode();
        }
        /**/
        
	}
	catch(PDOException $e)
	{
		echo "ERROR AL ELIMINAR DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL ELIMINAR DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL ELIMINAR DATOS 3 ".$s->getMessage();
        exit;
    }
}
else
{
    
}
?>