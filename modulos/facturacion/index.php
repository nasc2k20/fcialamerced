<?php
include 'NuevaFactModal.php';
?>
   <div class="panel panel-success panel-mb-6" id="FacturaVerPanel">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-folder-o icon-title"></i> Listado de Facturas
            <button class="close" arial-label="close" id="btnCerrarFactura">
                <span aria-hidden="true">&times;</span>
            </button>
        </h3>
    </div>
    <div class="panel-body">
        <section class="content-header">
           
           <div class="row">
                <div class="col-lg-6">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Busqueda de Factura por Fecha" id="BusquedaTxt" name="BusquedaTxt" style="text-transform: uppercase;">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
                        </span>
                    </div><!-- /input-group -->
                </div><!-- /.col-lg-6 -->
                <div class="col-lg-6">
                    <span class="pull-right" data-toggle="tooltip" title="Nuevo">
                        <button type="button" class="btn btn-success btn-social" data-toggle="modal" data-target="#NuevaFacturaProdModal" id="BtnNuevaFact"><i class="fa fa-plus-square" aria-hidden="true"></i> Nueva</button>
                    </span>
                </div>
            </div><!-- /.row -->
        </section>

        <?php  
        $texto = "";
        if(isset($_GET["texto"]))
        {
            $texto = $_GET["texto"];
        } 
        
        if($texto=="agregar"){ ?>
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Factura Agregada con Exito</strong></div>
        <?php }elseif($texto=="editar"){ ?>

        <div class="alert alert-info" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Factura Modificada con Exito</strong></div>
        <?php }elseif($texto=="eliminar"){ ?>

        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Factura Eliminada con Exito</strong></div>
        <?php } ?>

        <div style="height:10px;"></div>
        <hr>
        <section class="content">
            <div class="box box-primary">
                <div class="box-body">
                    
                     <div id="MostrarDatosFacturas"></div> <!--Carga los datos ajax -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content-->

    </div>
</div>


<!---->
<script src="styles/querys_modulos/FacturaQuerys.js"></script>
<script src="modulos/facturacion/QuerysFactura.js"></script>