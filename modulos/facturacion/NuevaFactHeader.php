<?php
include '../../conectar.php';

$sel_c = "SELECT MAX(cod_venta)AS cod_venta FROM ventas";
$eje_c = $DBcon->prepare($sel_c);
$eje_c->execute();
$ver_c = $eje_c->fetch(PDO::FETCH_ASSOC);
$CodFactura = $ver_c['cod_venta']+1;

date_default_timezone_set("America/El_Salvador");

?>
<script>
function formato_fecha() {
	cadena = document.getElementById('FactFecha').value;
	if(cadena.length == 2) { 
		document.getElementById('FactFecha').value += "-";
	} else if (cadena.length == 5) {
		document.getElementById('FactFecha').value += "-";
	} 
}
</script>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2">
            <label for="CodFactura">Corr:</label>
            <input type="text" class="form-control" id="CodFactura" value="<?php echo $CodFactura; ?>" disabled>
        </div>
        <div class="col-sm-3">
            <label for="FactFecha">Fecha: </label>
            <div class="input-group">
                <input type="text" class="form-control" id="FactFecha" value="<?php echo date('d-m-Y'); ?>" onkeypress="formato_fecha();" maxlength="10" placeholder="00-00-0000">
                <div class="input-group-addon">
                    <a href="#"><i class="fa fa-calendar"></i></a>
                </div>
            </div>
        </div>
        <div class="col-sm-7">
           <label for="FactNombre">Cliente:</label>
            <input type="text" class="form-control" id="FactNombre" placeholder="Nombre del Cliente" style="text-transform: uppercase;">
        </div>
    </div>

    <div style="height: 10px;"></div>

    <div class="row">
        <div class="col-sm-2">
            <label for="FactTipoPago">Tipo de Pago: </label>
            <select class="form-control" id="FactTipoPago">
                <option value="credito">Cr&eacute;dito</option>
                <option value="efectivo" selected>Contado</option>
            </select>
        </div>
        <div class="col-sm-5">
            <label for="FactVendedor">Vendedor: </label>
            <select class="form-control" id="FactVendedor">
                <?php
                include('../../conectar.php');
                $sel_empl = "SELECT * FROM empleado  
                            ORDER BY nombre_emp ASC";
                $eje_empl = $DBcon->prepare($sel_empl);
                $eje_empl->execute();
                ?>
                <?php
                while($ver_empl = $eje_empl->fetch(PDO::FETCH_ASSOC)){
                ?>
                <option value="<?php echo $ver_empl['cod_empleado']; ?>">
                    <?php echo $ver_empl['nombre_emp']." ".$ver_empl['apellido_emp']; ?>
                </option>
                <?php
                }
                ?>
            </select>
        </div>
        <div class="col-sm-2">
            <label for="FactDocumento">Documento: </label>
            <input type="text" class="form-control" id="FactDocumento" value="0">
        </div>
        <div class="col-sm-3">
            <div style="height: 25px;"></div>
            <button type="button" class="btn btn-success" id="GuardarVenta" onclick="GuardarVentaTotal(<?php echo $CodFactura; ?>);"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
            <button type="button" class="btn btn-danger" id="CerrarVenta"><i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar</button>
        </div>
    </div>

    <div style="height: 20px;"></div>

    <div class="row">
        <div class="col-sm-12">
            <input type="hidden" id="CodProdFactDet" class="form-control">
            <input type="hidden" id="NombreProdFactDet" class="form-control">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Busqueda Por Nombre" id="BuscarProdFactDet" style="text-transform: uppercase;">

                <span class="input-group-btn">
                    <button class="btn btn-info" type="button" id="BtnAgregarFactProd"><i class="fa fa-plus-square" aria-hidden="true"></i> Agregar</button>
                </span>
            </div>

        </div>
    </div>
    
    <div style="height: 10px;"></div>

        <div class="row" id="ResultadoAutocompleteFact">
            <div class="col-sm-3">
                <label for="CantidadVenta">Cant.</label>
                <input type="number" step="1" min="1" class="form-control" id="CantidadVenta">
            </div>
            <div class="col-sm-3">
                <label for="PrecioVenta">Precio.</label>
                <input type="number" step="0.0001" min="1" class="form-control" id="PrecioVenta">
            </div>
            <div class="col-sm-2">
                <label for="VenceVenta">Vence.</label>
                <input type="text" class="form-control" id="VenceVenta" readonly>
            </div>
            <div class="col-sm-2">
                <label for="ExistenciaVenta">Existencia.</label>
                <input type="text" class="form-control" id="ExistenciaVenta" readonly>
            </div>
            <div class="col-sm-2">
                <div style="height: 25px;"></div>
                <button class="btn btn-primary btn-sm" type="submit" id="AgregarVentaDet" onclick="AgregarVentaDetProd();"><i class="fa fa-plus-square" aria-hidden="true"></i> </button>
            </div>
        </div>


    <div id="DetalleNuevaFactura"></div><!-- Carga los datos ajax -->
</div>





<script src="modulos/facturacion/FactDetalle.js"></script>
