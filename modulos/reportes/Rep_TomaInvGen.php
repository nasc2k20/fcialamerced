<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../styles/css/bootstrap.min.css">
    <script src="../../styles/js/jquery-3.3.1.min.js"></script>
    <script src="../../styles/js/jspdf.min.js"></script>
    <script src="../../styles/js/jspdf.plugin.autotable.min.js"></script>
</head>

<?php
$Datos = array();

require_once '../../conectar.php';
$fecha = date("d-m-Y");

$contar = 0;
$acum_costo = 0;
$acum_c_tot = 0;
$acum_saldo = 0;
$acum_p_tot = 0;
$acum_precio= 0;

$sel_nfact = "SELECT * FROM laboratorios a
			   INNER JOIN productos b ON a.cod_lab=b.cod_lab
			   ORDER BY b.nombre_producto ASC";
$eje_nfact = $DBcon->prepare($sel_nfact);
$eje_nfact->execute();
$con_nfact = $eje_nfact->rowCount();

if($con_nfact > 0)
{
    while($ver_nfact = $eje_nfact->fetch(PDO::FETCH_ASSOC))
    {
        $Datos[] = $ver_nfact;
    }
}
?>
<body>

    <table class="table table-stripped table-bordered" id="RepTomaInvGen">
        <thead>
            <tr>
                <th>CORR</th>
                <th>NOMBRE DEL PRODUCTO</th>
                <th>LABORATORIO</th>
                <th>COSTO</th>
                <th>SALDO</th>
                <th>FISICO</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $total_productos=1;
            foreach ($Datos as $ver_nfact)
            {
        ?>
            <tr style="font-size: 8pt;">
                <td width="5%"><?php echo $total_productos; ?></td>
                <td width="40%"><?php echo strtoupper($ver_nfact["nombre_producto"]); ?></td>
                <td width="25%"><?php echo $ver_nfact["nombre_lab"]; ?></td>
                <td width="10%"><?php echo number_format($ver_nfact["precio_costo"],4); ?></td>
                <td width="10%"><?php echo number_format($ver_nfact["existencia_prod"],0); ?></td>
                <td width="10%"></td>
            </tr>
            <?php
                $acum_costo = number_format($ver_nfact["precio_costo"]*$ver_nfact["existencia_prod"],4);
                $acum_precio = number_format($ver_nfact["precio_venta"]*$ver_nfact["existencia_prod"],4);
                $acum_c_tot += floatval($acum_costo);
                $acum_p_tot +=floatval($acum_precio);
                $acum_saldo += intval($ver_nfact["existencia_prod"]);
                $contar+=1;
                $total_productos +=1;
            }
        ?>
        </tbody>
    </table>


    <table class="table table-stripped table-borderless" id="Consolidados">
        <thead>
            <tr>
                <th width="35%">CONCEPTO</th>
                <th width="65%">VALOR</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td width="35%">Total de Productos</td>
                <td width="65%"><?php echo $contar; ?></td>
            </tr>
            <tr>
                <td width="35%">Total Unidades en Existencia</td>
                <td width="65%"><?php echo number_format($acum_saldo,0); ?></td>
            </tr>
            <tr>
                <td width="35%">Total Inventario (Costo)</td>
                <td width="65%">$ <?php echo number_format($acum_c_tot,4); ?></td>
            </tr>
            <tr>
                <td width="35%">Total Inventario (Precio Venta)</td>
                <td width="65%">$ <?php echo number_format($acum_p_tot,4); ?></td>
            </tr>
            <tr>
                <td width="35%">Diferencia (Precio Venta - Costo)</td>
                <td width="65%">$ <?php echo number_format($acum_p_tot-$acum_c_tot,4); ?></td>
            </tr>
        </tbody>
    </table>


    <script>
        $(function() {

            CargarReporte4();

            $('#RepTomaInvGen').empty();

        });

        function CargarReporte4() {
            //var pdf = new jsPDF();
            var pdf = new jsPDF('p','pt','letter');
            var Consolidado = pdf.autoTableHtmlToJson(document.getElementById('Consolidados'));
            
            pdf.setProperties({
                title: 'ReporteInventarioGeneral',
                subject: 'Reporte de Inventario General',
                author: 'SysnetSoftware',
                keywords: 'Generado por JsPdf',
                creator: 'Sysnet Corp'
            });
            pdf.setFontSize(12);
            pdf.text(25, 80, "REPORTE DE INVENTARIO GENERAL");
            pdf.autoTable({
                styles: {
                    cellPadding: 3,
                    rowHeight: 25,
                    fontSize: 10,
                    columnWidth: 'wrap'
                },
                theme: 'grid',
                margin: {
                    top: 90,
                    left: 25,
                },
                head: [
                    ['NUM', 'NOMBRE DEL PRODUCTO', 'LABORATORIO', 'COSTO', 'SALDO', 'FISICO']
                ],
                body: [
                    <?php $ContarNum=1; foreach($Datos as $d){ ?>["<?php echo $ContarNum; ?>", "<?php echo strtoupper($d['nombre_producto']) ?>", "<?php echo $d['nombre_lab']; ?>", "<?php echo '$ '.number_format($d['precio_costo'],2); ?>", "<?php echo $d['existencia_prod']; ?>", ""],
                    <?php $ContarNum +=1; }?>
                ]
                
            });
            
            pdf.autoTable(Consolidado.columns, Consolidado.data);
            
            var iframe = document.createElement('iframe');
            iframe.setAttribute('style', 'position:absolute; top:0;bottom:0;right:0;left:0; height:100%; width:100%');
            document.body.appendChild(iframe);
            iframe.src = pdf.output('datauristring');

            //pdf.save('mipdf.pdf');
        }

    </script>

</body>

</html>
