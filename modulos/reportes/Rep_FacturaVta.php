<?php
require_once '../../conectar.php';
$no = 1;

$counter_subto = 0;
$counter_desci = 0;
$sub_total = 0;
$iva_compra = 0;

if (isset($_POST['CodFactura']))
{
    $cod_nfactu = $_POST["CodFactura"];
    
    $sel_nfact_temp = "SELECT * FROM ventas_detalle  
				  WHERE cod_venta=$cod_nfactu 
				  ORDER BY cod_det_vta ASC";
    
}
else
{
    $sel_nfact_temp = "";
}


$eje_nfact_temp = $DBcon->prepare($sel_nfact_temp);
$eje_nfact_temp->execute();


?>
<span class="pull-right">
    <button class="btn btn-primary btn-sm" type="button" id="BtnImpFact"><i class="fa fa-print" aria-hidden="true"></i></button>
</span>

<br>

<table class="table table-borderless" style="border:none;">
    <thead>
        <tr>
            <th>Cant</th>
            <th>Nombre de Producto</th>
            <th>Precio Vta.</th>
            <th>Total</th>
        </tr>
    </thead>
    <tbody>

        <?php
        while ($ver_nfact_temp = $eje_nfact_temp->fetch(PDO::FETCH_ASSOC)) {
       ?>
        <tr>
            <td width='10%'><?php echo $ver_nfact_temp["cant_vta"]; ?></td>
            <td width='35%'><?php echo $ver_nfact_temp["nombre_prod"]; ?></td>
            <td width='15%'>$ <?php echo $ver_nfact_temp["precio_vta"]; ?></td>
            <td width='15%'>$ <?php echo $ver_nfact_temp["total_vta"]; ?></td>
        </tr>
        <?php
            $counter_subto += $ver_nfact_temp["total_vta"];
          $no++;
        }
        ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="4" width='75%'></td>
        </tr>
        <tr>
            <td colspan="2" width='75%'></td>
            <td width='10%'>Sumas: </td>
            <td width='10%'>$<?php echo number_format($counter_subto,2); ?></td>
        </tr>
        <tr>
            <td colspan="2" width='75%'></td>
            <td width='10%'>Exentas: </td>
            <td width='10%'>$<?php echo "0.00"; ?></td>
        </tr>
        <tr>
            <td colspan="2" width='75%'></td>
            <td width='10%'>Total: </td>
            <td width='10%'>$<?php echo number_format($counter_subto,2); ?></td>
        </tr>
    </tfoot>
</table>

<script>
$(function(){
  $('#BtnImpFact').click(function(){
    alert('Modulo en Construccion...');
    //alert('Imprimiendo...');
  });
});

</script>