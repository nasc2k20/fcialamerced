<?php
require_once '../../conectar.php';
$no = 1;

$counter_subto = 0;
$counter_desci = 0;
$sub_total = 0;
$iva_compra = 0;
//$cod_nfactu = $_GET["nfactu"];



if (isset($_POST['CodCompra']) or isset($_POST["CodProv"]))
{
    $cod_nfactu = $_POST["CodCompra"];
    $cod_prov = $_POST["CodProv"];
    
    $sel_nfact_temp = "SELECT * FROM compras_detalle  
				  WHERE cod_compra=$cod_nfactu 
				  ORDER BY cod_det_compra ASC";
    
}
else
{
    $sel_nfact_temp = "";
}


$eje_nfact_temp = $DBcon->prepare($sel_nfact_temp);
$eje_nfact_temp->execute();


$sel_prov = "SELECT * FROM compras a 
            INNER JOIN proveedor b ON b.cod_prov=a.cod_prov 
            WHERE a.cod_compra=$cod_nfactu AND b.cod_prov = $cod_prov";
$eje_prov = $DBcon->prepare($sel_prov);
$eje_prov->execute();
$ver_prov = $eje_prov->fetch(PDO::FETCH_ASSOC);

?>
       <br>
        <table class="table table-bordered table-striped table-hover">
        <thead>
         <tr>
            <td colspan="4"><i class="fa fa-user-circle" aria-hidden="true"></i> <strong><?php echo $ver_prov['nombre_prov']; ?></strong></td>
            <td colspan="2">Fact. N°: <?php echo $ver_prov['num_factura']; ?></td>
        </tr>
          <tr>
            <th class="center">Cant</th>
            <th class="center">Bonif.</th>
            <th class="center">Nombre de Producto</th>
            <th class="center">Precio Fact.</th>
            <th class="center">Desc.</th>
            <th class="center">Total</th>
          </tr>
        </thead>
        <tbody>
        
        <?php
        while ($ver_nfact_temp = $eje_nfact_temp->fetch(PDO::FETCH_ASSOC)) {
       ?>
          <tr>
                  <td class='center' width='10%'><?php echo $ver_nfact_temp["cantidad_comp"]; ?></td>
                  <td class='center' width='10%'><?php echo $ver_nfact_temp["bonificacion_comp"]; ?></td>
                  <td class='left' width='35%'><?php echo $ver_nfact_temp["nombre_prod_comp"]; ?></td>
                  <td class='center' width='15%'>$ <?php echo $ver_nfact_temp["precio_facturacion"]; ?></td>
                  <td class='center' width='15%'><?php echo $ver_nfact_temp["descuento_ind"]; ?></td>
                  <td class='center' width='15%'>$ <?php echo $ver_nfact_temp["total_prod_compra"]; ?></td>
            </tr>
        <?php
            $counter_subto += $ver_nfact_temp["total_prod_compra"];
            $counter_desci += number_format(($ver_nfact_temp["total_prod_compra"]*$ver_nfact_temp["descuento_ind"])/100,2);
          $no++;
        }
        ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4" width='75%'></td>
                <td  width='10%'>Sumas: </td>
                <td  width='10%'>$<?php echo number_format($counter_subto,2); ?></td>
            </tr>
            <tr>
                <td colspan="4" width='75%'></td>
                <td  width='10%'>Desc: </td>
                <td  width='10%'>$<?php echo number_format($counter_desci,2); ?></td>
            </tr>
               <tr>
                <td colspan="4" width='75%'></td>
                <td  width='10%'>Sub-Total: </td>
                <td  width='10%'>$<?php $sub_total = $counter_subto-$counter_desci; echo number_format($sub_total,2); ?></td>
            </tr>
               <tr>
                <td colspan="4" width='75%'></td>
                <td  width='10%'>Iva: </td>
                <td  width='10%'>$<?php $iva_compra = number_format(($sub_total*13)/100,2); echo number_format($iva_compra,2); ?></td>
            </tr>
            <tr>
                <td colspan="4" width='75%'></td>
                <td  width='10%'>Total Compra: </td>
                <td  width='10%'>$<?php echo number_format($sub_total+$iva_compra,2); ?></td>
            </tr>
        </tfoot>
      </table>
