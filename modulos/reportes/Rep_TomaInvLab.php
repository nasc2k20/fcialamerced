<?php
require_once '../../conectar.php';
$fecha = date("d-m-Y");
$respuesta = $_REQUEST['action'];

if ($respuesta=='showAll')
{
    $sel_nfact = "";
}
else
{
    $sel_nfact = "SELECT * FROM laboratorios a
                   INNER JOIN productos b ON a.cod_lab=b.cod_lab 
                   WHERE b.cod_lab='".$respuesta."'
                   ORDER BY b.nombre_producto ASC";
}

$eje_nfact = $DBcon->prepare($sel_nfact);
$eje_nfact->execute();

$acum_costo = 0;
$acum_c_tot = 0;
$acum_saldo = 0;
$acum_p_tot = 0;
$acum_precio= 0;
$total_productos = 1;


$contar = 0;

?>
<table class="table table-stripped table-bordered" id="TableTomaInvGen">
    <thead>
        <tr>
            <th>CORR</th>
            <th>NOMBRE DEL PRODUCTO</th>
            <th>LABORATORIO</th>
            <th>COSTO</th>
            <th>SALDO</th>
            <th>FISICO</th>
        </tr>
    </thead>
    <tbody>
       <?php
            while($ver_nfact = $eje_nfact->fetch(PDO::FETCH_ASSOC))
            {
        ?>
        <tr style="font-size: 8pt;">
            <td width="5%"><?php echo $total_productos; ?></td>
            <td width="40%"><?php echo strtoupper($ver_nfact["nombre_producto"]); ?></td>
            <td width="25%"><?php echo $ver_nfact["nombre_lab"]; ?></td>
            <td width="10%"><?php echo number_format($ver_nfact["precio_costo"],4); ?></td>
            <td width="10%"><?php echo number_format($ver_nfact["existencia_prod"],0); ?></td>
            <td width="10%"></td>
        </tr>
        <?php
                $acum_costo = number_format($ver_nfact["precio_costo"]*$ver_nfact["existencia_prod"],4);
                $acum_precio = number_format($ver_nfact["precio_venta"]*$ver_nfact["existencia_prod"],4);
                $acum_c_tot += floatval($acum_costo);
                $acum_p_tot +=floatval($acum_precio);
                $acum_saldo += intval($ver_nfact["existencia_prod"]);
                $contar+=1;
                $total_productos +=1;
            }
        ?>
    </tbody>
</table>


<table class="table table-stripped table-borderless">
    <thead>
        <tr>
            <th width="35%">CONCEPTO</th>
            <th width="65%">VALOR</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="35%">Total de Productos</td>
            <td width="65%"><?php echo $contar; ?></td>
        </tr>
        <tr>
            <td width="35%">Total Unidades en Existencia</td>
            <td width="65%"><?php echo number_format($acum_saldo,0); ?></td>
        </tr>
        <tr>
            <td width="35%">Total Inventario (Costo)</td>
            <td width="65%">$ <?php echo number_format($acum_c_tot,4); ?></td>
        </tr>
        <tr>
            <td width="35%">Total Inventario (Precio Venta)</td>
            <td width="65%">$ <?php echo number_format($acum_p_tot,4); ?></td>
        </tr>
        <tr>
            <td width="35%">Diferencia (Precio Venta - Costo)</td>
            <td width="65%">$ <?php echo number_format($acum_p_tot-$acum_c_tot,4); ?></td>
        </tr>
    </tbody>
</table>



