<!DOCTYPE html>
<?php
require_once '../../conectar.php';

$FechaRepI = $_REQUEST['FechaInicioTxt'];
$FechaRepF = $_REQUEST['FechaFinTxt'];

$FechaPrmI = new datetime($_REQUEST['FechaInicioTxt']);
$FechaPrmF = new datetime($_REQUEST['FechaFinTxt']);

$FechaInicio = $FechaPrmI->format('Y-m-d');
$FechaFin = $FechaPrmF->format('Y-m-d');

$Datos = array();

$sel_nfact = "SELECT * FROM ventas 
              WHERE fecha_venta BETWEEN '".$FechaInicio."' AND '".$FechaFin."' 
              AND venta_anular='NO' 
              ORDER BY date_venta ASC";

$eje_nfact = $DBcon->prepare($sel_nfact);
$eje_nfact->execute();
$con_nfact = $eje_nfact->rowCount();

$total_productos = 1;
$TotalVenta = 0;

if($con_nfact > 0)
{
    while($ver_nfact = $eje_nfact->fetch(PDO::FETCH_ASSOC))
    {
        $Datos[] = $ver_nfact;
    }
}


?>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../../styles/css/bootstrap.min.css">
    <script src="../../styles/js/jquery-3.3.1.min.js"></script>
    <script src="../../styles/js/jspdf.min.js"></script>
    <script src="../../styles/js/jspdf.plugin.autotable.min.js"></script>

</head>

<body>
    <table class="table table-stripped table-bordered" id="TblRepFactFecha">
        <thead>
            <tr>
                <th>CORR</th>
                <th>FECHA</th>
                <th>NOMBRE DEL CLIENTE</th>
                <th>TOTAL</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach ($Datos as $ver_nfact)
            {
        ?>
            <tr style="font-size: 8pt;">
                <td width="5%">
                    <?php echo $total_productos; ?>
                </td>
                <td width="20%">
                    <?php $FechaForm = new datetime($ver_nfact["fecha_venta"]); $FechaFactu = $FechaForm->format('d-m-Y'); echo $FechaFactu; ?>
                </td>
                <td width="55%">
                    <?php echo strtoupper($ver_nfact["nombre_cliente"]); ?>
                </td>
                <td width="10%">
                    <?php echo "$ ".number_format($ver_nfact["total_venta"],4); ?>
                </td>
                <td width="10%">
                </td>
            </tr>
            <?php
                $total_productos +=1;
                $TotalVenta+=$ver_nfact['total_venta'];
            }
        ?>
        </tbody>
    </table>

    <table class="table table-borderless" id="TblTotalFactFecha">
        <thead>
            <tr>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <h3><strong><?php echo "TOTAL EN VENTA: \n $ ".number_format($TotalVenta,4); ?></strong></h3>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
        </tbody>
    </table>

    <script>
    $(function() {

        CargarReporte();

        $('#TblRepFactFecha').empty();

    });

    function CargarReporte() {
        //var pdf = new jsPDF();
        var pdf = new jsPDF('p', 'pt', 'letter');
        var Consolidado = pdf.autoTableHtmlToJson(document.getElementById('TblTotalFactFecha'));

        pdf.setProperties({
            title: 'ReporteFacturasFecha',
            subject: 'Reporte de Facturas por Fecha',
            author: 'SysnetSoftware',
            keywords: 'Generado por JsPdf',
            creator: 'Sysnet Corp'
        });
        pdf.setFontSize(12);
        pdf.text(25, 80, "REPORTE DE FACTURAS POR FECHA");
        pdf.text(25, 95, "<?php echo 'FECHA INICIO: '.$FechaRepI.' \nFECHA FIN: '.$FechaRepF; ?>");
        pdf.autoTable({
            styles: {
                cellPadding: 3,
                rowHeight: 25,
                fontSize: 10,
                columnWidth: 'wrap'
            },
            theme: 'grid',
            margin: {
                top: 113,
                left: 25,
            },
            head: [
                ['NUM', 'FECHA', 'NOMBRE DEL CLIENTE', 'TOTAL']
            ],
            body: [
                <?php $ContarNum=1; foreach($Datos as $d){ ?>["<?php echo $ContarNum; ?>",
                    "<?php $FechaForm = new datetime($d["fecha_venta"]); $FechaFactu = $FechaForm->format('d-m-Y'); echo $FechaFactu; ?>",
                    "<?php echo $d['nombre_cliente']; ?>",
                    "<?php echo '$ '.number_format($d['total_venta'],2); ?>"],
                <?php $ContarNum +=1; }?>
            ]

        });

        pdf.autoTable(Consolidado.columns, Consolidado.data);

        var iframe = document.createElement('iframe');
        iframe.setAttribute('style', 'position:absolute; top:0;bottom:0;right:0;left:0; height:100%; width:100%');
        document.body.appendChild(iframe);
        iframe.src = pdf.output('datauristring');

    }
    </script>


</body>

</html>