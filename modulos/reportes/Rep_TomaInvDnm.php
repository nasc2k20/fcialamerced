<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="../../styles/css/bootstrap.min.css">
    <script src="../../styles/js/jquery-3.3.1.min.js"></script>
    <script src="../../styles/js/jspdf.min.js"></script>
    <script src="../../styles/js/jspdf.plugin.autotable.min.js"></script>
</head>

<?php

$PrecioCb = $_REQUEST['PrecioCb'];
$CostoCb = $_REQUEST['CostoCb'];
$SaldoCb = $_REQUEST['SaldoCb'];
$LaboratorioCb = $_REQUEST['LaboratorioCb'];

$Datos = array();

require_once '../../conectar.php';
$fecha = date("d-m-Y");

$sel_nfact = "SELECT * FROM laboratorios a
			   INNER JOIN productos b ON a.cod_lab=b.cod_lab
			   ORDER BY b.nombre_producto ASC";
$eje_nfact = $DBcon->prepare($sel_nfact);
$eje_nfact->execute();
$con_nfact = $eje_nfact->rowCount();

if($con_nfact > 0)
{
    while($ver_nfact = $eje_nfact->fetch(PDO::FETCH_ASSOC))
    {
        $Datos[] = $ver_nfact;
    }
}
?>
<body>

    <table class="table table-stripped table-bordered" id="TbRepTomaInvGenDnm">
        <thead>
            <tr>
                <th>CORR</th>
                <th>NOMBRE DEL PRODUCTO</th>

                <?php if($LaboratorioCb==1){ ?>
                <th>LABORATORIO</th>
                <?php }

                if($SaldoCb==1){
                ?>
                <th>SALDO</th>
                <?php }
                
                if($CostoCb==1){
                ?>
                <th>COSTO</th>
                <?php }
                
                if($PrecioCb==1){
                ?>
                <th>PRECIO</th>
                <?php }
                ?>
            </tr>
        </thead>
        <tbody>
            <?php
            $total_productos=1;
            foreach ($Datos as $ver_nfact)
            {
        ?>
            <tr style="font-size: 8pt;">
                <td width="7%"><?php echo $total_productos; ?></td>
                <td width="40%"><?php echo strtoupper($ver_nfact["nombre_producto"]); ?></td>
                <?php if($LaboratorioCb==1){ ?>
                <td width="25%"><?php echo $ver_nfact["nombre_lab"]; ?></td>
                <?php } if($SaldoCb==1){ ?>
                <td width="9%"><?php echo $ver_nfact["existencia_prod"]; ?></td>
                <?php } if($CostoCb==1){ ?>
                <td width="9%"><?php echo number_format($ver_nfact["precio_costo"],4); ?></td>
                <?php } if($PrecioCb==1){ ?>
                <td width="10%"><?php echo number_format($ver_nfact["precio_venta"],4); ?></td>
                <?php } ?>
            </tr>
            <?php
                $total_productos +=1;
            }
        ?>
        </tbody>
    </table>


    <script>
        $(function() {

            CargarReporte();
            $('#TbRepTomaInvGenDnm').hide();
        });

        function CargarReporte() {

            var pdf = new jsPDF('p','pt','letter');
            
            pdf.setProperties({
                title: 'ReporteInventarioGeneral',
                subject: 'Reporte de Inventario General',
                author: 'SysnetSoftware',
                keywords: 'Generado por JsPdf',
                creator: 'Sysnet Corp'
            });
            pdf.setFontSize(12);
            pdf.text(25, 80, "REPORTE DE INVENTARIO GENERAL DNM");
            pdf.autoTable({
                styles: {
                    cellPadding: 3,
                    rowHeight: 25,
                    fontSize: 9,
                    overflow: 'ellipsize',
                    valign: 'middle',
                    columnWidth: 'auto'
                },
                theme: 'grid',
                showHead: 'firstPage',
                margin: {
                    top: 90,
                    left: 25,
                },
                html: '#TbRepTomaInvGenDnm'
            });
            
            var iframe = document.createElement('iframe');
            iframe.setAttribute('style', 'position:absolute; top:0;bottom:0;right:0;left:0; height:100%; width:100%');
            document.body.appendChild(iframe);
            iframe.src = pdf.output('datauristring');
        }

    </script>

</body>

</html>
