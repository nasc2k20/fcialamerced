<?php
include '../../login/validar.php';
require '../../conectar.php';


date_default_timezone_set("America/El_Salvador");

$usuario	= $_SESSION["usuario"];
//$usuario = "admin";


if($_POST['SqlQuery']=='verificar')
{
    try
	{   
        $CodProd	= $_POST["CodeProd"];
		$cantidad	= $_POST["CantProd"];
		$bonifica	= ($_POST["BonifProd"]=='') ? '0' : $_POST["BonifProd"];
		$precio		= $_POST["PrecioProd"];
		$descuento	= ($_POST["DescProd"]=='') ? '0' : $_POST["DescProd"];
		$iva_comp	= ($_POST["ImpProd"]==0) ? 0 : 1;
		$cod_compra	= $_POST["CodeCompra"];
                
		$sel_prods = "SELECT * FROM productos WHERE cod_producto='$CodProd'";
		$eje_prods = $DBcon->prepare($sel_prods);
		$eje_prods->execute();
        
		while($ver_prods = $eje_prods->fetch(PDO::FETCH_ASSOC))
		{
			$nombre_prod = strtoupper($ver_prods["nombre_producto"]);
			$precio_cto  = $ver_prods["precio_costo"];
			$prod_existe = $ver_prods["existencia_prod"];
		}
		
			
		if($iva_comp==1)
		{
			$precio_u = number_format($precio,4);
			$precio_factu = number_format($precio*1.13,4);
		}
		else
		{
			$iva_comp=0;
			$precio_u = number_format($precio/1.13,4);
			$precio_factu = number_format($precio,4);
		}
			
		//$precio_factu = number_format($precio*$valor_iva,4);
		//$precio_factu = number_format($precio,4);
		$cant_compra = floatval($cantidad+$bonifica);//5+2=7
		$costo_compra = floatval(number_format(($cantidad*$precio_factu)/$cant_compra,4,'.','')); //(5*1)/7=0.7143
		$desc_compra = number_format(($costo_compra * $descuento)/100,4,'.','');//(0.7143*5)/100=0.0357
		$nuevo_costo = number_format($costo_compra - $desc_compra,4,'.','');//0.7143-0.0357=0.6786
		$total_fact  = number_format($precio_u*$cantidad,4,'.','');
		
		$insert_fact = "INSERT INTO compras_detalle 
					   (cod_prod,cantidad_comp,
					   bonificacion_comp,nombre_prod_comp,precio_facturacion,
					   costo_compra,descuento_ind,total_prod_compra,cod_compra,impuesto_c) 
					   VALUES (:cod_prod,:cantidad_comp,:bonificacion_comp,:nombre_prod_comp,
					   :precio_facturacion,:costo_compra,:descuento_ind,:total_prod_compra,:cod_compra,:impuesto_c)";
		$insert_fact = $DBcon->prepare($insert_fact);
		$insert_fact->bindparam(":nombre_prod_comp", $nombre_prod,PDO::PARAM_STR);
		$insert_fact->bindparam(":precio_facturacion", $precio_u,PDO::PARAM_STR);
		$insert_fact->bindparam(":costo_compra", $nuevo_costo,PDO::PARAM_STR);
		$insert_fact->bindparam(":descuento_ind", $descuento,PDO::PARAM_STR);
		$insert_fact->bindparam(":total_prod_compra", $total_fact,PDO::PARAM_STR);
		$insert_fact->bindparam(":cod_prod", $CodProd,PDO::PARAM_STR);
		$insert_fact->bindparam(":cantidad_comp",$cantidad,PDO::PARAM_STR);
		$insert_fact->bindparam(":bonificacion_comp", $bonifica,PDO::PARAM_STR);
		$insert_fact->bindparam(":cod_compra", $cod_compra,PDO::PARAM_STR);
		$insert_fact->bindparam(":impuesto_c", $iva_comp,PDO::PARAM_STR);
		
        if($insert_fact->execute())
        {
            header("location: index.php");
            /*
            echo "<script>$('#AgregarCompraDet').on('click', function(e) {
                    e.preventDefault();
                    $('html, body').animate({scrollTop: $('#DetalleNuevaCompras').offset().top }, 1000);
                });</script>";
                */
        }
        else
        {
            //echo '<script> alert("Error al Eliminar los Datos: '.$DBcon->errorInfo().' El Codigo de Error es: '.$DBcon->errorCode().' ")</script>';
            echo "\nError al Verificar los Datos:\n";
            print_r($insert_fact->errorInfo());
            echo "<br>El Codigo de Error es: ". $insert_fact->errorCode();
        }
	}
	catch(PDOException $e)
	{
		echo "ERROR AL VERIFICAR LOS DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL VERIFICAR LOS DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL VERIFICAR LOS DATOS 3 ".$s->getMessage();
        exit;
    }
}
elseif($_POST['SqlQuery']=='insertar')
{   
    try
	{
        $ValorInsert = 0;
        $total_fact =0;
        $contar =0;
        $descu =0;
        
        $cod_nfactu = $_POST["CodCompra"];
        
        $sel_tot = "SELECT * FROM compras_detalle WHERE cod_compra=:coddetvta";
        $eje_tot = $DBcon->prepare($sel_tot);
        $eje_tot->execute(array(":coddetvta"=> $cod_nfactu));
        $con_tot = $eje_tot->rowCount();
        
        
        if($con_tot>0)
        {
            while($ver_tot = $eje_tot->fetch(PDO::FETCH_ASSOC))
            {
                $total_fact += $ver_tot["total_prod_compra"];
                $calcular_desc = number_format(($ver_tot["total_prod_compra"]*$ver_tot["descuento_ind"])/100,4,'.','');
                $descu += $calcular_desc;
                $contar +=1;
            }
            $compra_subtotal = number_format($total_fact-$descu,2,'.','');
            $compra_iva	= number_format(($compra_subtotal*13)/100, 2,'.','');
            $total_compra = number_format($compra_subtotal+$compra_iva,4,'.','');



            $fecha_Form = new datetime($_POST["FechaCompra"]);
            $txt_fecha	= $fecha_Form->format('Y-m-d');
            $fecha_det	= date('Y-m-d H:i:s');
            $nombre_pro	= $_POST["CodProveedor"];
            $vendedor	= $_POST["CodVendedor"];
            $num_fact	= $_POST["NumFacCompra"];
            $desc_comp	= ($_POST["DescCompra"]=='') ? 0 : $_POST["DescCompra"];
            $tipo_pago	= $_POST["TipoPago"];
            $compra_anu = "NO";

            $insert_fvta = "INSERT INTO compras (fecha_compra,date_compra,sumas_vta,
                            iva_vta,total_compra,cod_prov,num_factura,
                            cod_empleado,tipo_pago_compra,compra_anular,usuario) 
                            VALUES (:fecha_compra,:date_compra,:sumas_vta,
                            :iva_vta,:total_compra,:cod_prov,:num_factura,
                            :cod_empleado,:tipo_pago_compra,:compra_anular,:usuario)";
            $insert_fvta = $DBcon->prepare($insert_fvta);
            $insert_fvta->bindparam(":compra_anular", $compra_anu);
            $insert_fvta->bindparam(":usuario", $usuario);
            $insert_fvta->bindparam(":cod_empleado", $vendedor);
            $insert_fvta->bindparam(":tipo_pago_compra", $tipo_pago);
            $insert_fvta->bindparam(":fecha_compra", $txt_fecha);
            $insert_fvta->bindparam(":date_compra", $fecha_det);
            $insert_fvta->bindparam(":sumas_vta", $total_fact);
            $insert_fvta->bindparam(":iva_vta", $compra_iva);
            $insert_fvta->bindparam(":total_compra", $total_compra);
            $insert_fvta->bindparam(":cod_prov", $nombre_pro);
            $insert_fvta->bindparam(":num_factura", $num_fact);
            
            
            
            if($insert_fvta->execute())
            {
                $sel_exis = "SELECT * FROM productos a 
                         INNER JOIN compras_detalle b ON a.cod_producto=b.cod_prod
                         WHERE b.cod_compra=:coddetvta";
                $eje_exis = $DBcon->prepare($sel_exis);
                $eje_exis->execute(array(":coddetvta"=> $cod_nfactu));

                while($ver_exis = $eje_exis->fetch(PDO::FETCH_ASSOC))
                {
                    $precio_cto = $ver_exis["precio_costo"]; 
                    $cod_produ = $ver_exis["cod_producto"];
                    $existe_prod = $ver_exis["existencia_prod"];
                    $cantidad = $ver_exis["cantidad_comp"];
                    $bonifica = $ver_exis["bonificacion_comp"];
                    $precio = $ver_exis["precio_facturacion"];
                    $descuento = $ver_exis["descuento_ind"];


                    //10*1.25=12.50
                    $costo_existe = number_format(($existe_prod*$precio_cto),4,'.','');
                    //5+2=7
                    $cant_compra = ($cantidad+$bonifica);
                    //10+7=17
                    $existe_total = $existe_prod+$cant_compra;
                    //(5*1)/7=0.7143
                    $costo_compra = number_format(($cantidad*$precio)/$cant_compra,4,'.',''); 
                    //(0.7143*5)/100=0.0357
                    $desc_compra = number_format(($costo_compra*$descuento)/100,4,'.','');
                    //0.7143-0.0357=0.6786
                    $nuevo_costo = number_format($costo_compra-$desc_compra,4,'.','');
                    //(0.6786*7)+12.5=17.2502
                    $nuevos_tot = number_format(($nuevo_costo*$cant_compra)+$costo_existe,4,'.','');
                    //17.2502/17=1.0147
                    $costo_prom = number_format(($nuevos_tot/$existe_total)*1.13,4,'.','');
                    //$costo_prom = number_format(($nuevos_tot/$existe_total),4);

                    $texto = "compra";

                    $referencia = "COMPRA DE PRODUCTO FACT# ".$num_fact;
                    $salida = 0;

                    $insert_kar = "INSERT INTO kardex (fecha_var,fecha_tot,cod_producto,
                                num_factura,costo_prod_kardex,precio_vta_kardex,
                                referencia_kardex,entrada_kardex,salida_kardex,total,
                                usuario)VALUES (:fecha_var,:fecha_tot,:cod_producto,
                                :num_factura,:costo_prod_kardex,:precio_vta_kardex,
                                :referencia_kardex,:entrada_kardex,:salida_kardex,:total,
                                :usuario)";
                    $insert_kar = $DBcon->prepare($insert_kar);
                    $insert_kar->bindparam(":fecha_var", $txt_fecha);
                    $insert_kar->bindparam(":referencia_kardex", $referencia);
                    $insert_kar->bindparam(":usuario", $usuario);
                    $insert_kar->bindparam(":costo_prod_kardex", $costo_prom);
                    $insert_kar->bindparam(":precio_vta_kardex", $precio);
                    $insert_kar->bindparam(":fecha_tot", $fecha_det);
                    $insert_kar->bindparam(":cod_producto", $cod_produ);
                    $insert_kar->bindparam(":num_factura", $num_fact);
                    $insert_kar->bindparam(":entrada_kardex", $cant_compra);
                    $insert_kar->bindparam(":salida_kardex", $salida);
                    $insert_kar->bindparam(":total", $existe_total);



                    $update_exis = "UPDATE productos 
                                  SET 
                                  precio_costo=:precio_costo,
                                  existencia_prod=:txt_existencia 
                                  WHERE cod_producto=:codproducto";
                    $update_exis = $DBcon->prepare($update_exis);
                    $update_exis->bindparam(":precio_costo", $costo_prom,PDO::PARAM_STR);
                    $update_exis->bindparam(":codproducto", $cod_produ,PDO::PARAM_STR);
                    $update_exis->bindparam(":txt_existencia", $existe_total,PDO::PARAM_STR);




                    if($update_exis->execute())
                    {
                        $insert_kar->execute();
                        $ValorInsert = 1;
                    }
                    else
                    {
                        echo "\nError al Agregar Insertar Factura:\n";
                        print_r($update_exis->errorInfo());
                        echo "\nError al Agregar Insertar Factura:\n";
                        print_r($insert_kar->errorInfo());
                        echo "<br>El Codigo de Error es: ". $update_exis->errorCode();
                        echo "<br>El Codigo de Error es: ". $insert_kar->errorCode();
                    }
               }
            }
            else
            {
                echo "\nError al Agregar Insertar Factura:\n";
                print_r($insert_fvta->errorInfo());
                echo "<br>El Codigo de Error es: ". $insert_fvta->errorCode();
            }
            
            
            if($ValorInsert>0)
            {
                header('location: index.php');
            }
            
            
        }
        else
        {
            ?>
            <script>
                alert("No se puede Guardar una Factura sin Productos.");
            </script>
            <?php
        }
    }
	catch(PDOException $e)
	{
		echo "ERROR AL INSERTAR LOS DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL INSERTAR LOS DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL INSERTAR LOS DATOS 3 ".$s->getMessage();
        exit;
    }
}
elseif($_POST['SqlQuery']=='anular')
{
    try
    {
        $ValorInsert = 0;
        $costo_prom = 0;
        $precio = 0;
        $cod_compra = $_POST["EliminarID"];
        $NumFactura = $_POST["NumFactura"];
        $fecha_Form = new datetime($_POST["FechaCompra"]);
        $txt_fecha	= $fecha_Form->format('Y-m-d');
        $fecha_det	= date('Y-m-d H:i:s');
        $referencia = "ANULACION DE COMPRA FACT# ".$NumFactura;
        $entrada = 0;
        $compra_anular = "SI";
        $texto = "anulada";
        
        
        
        $upda_compra = "UPDATE compras 
                        SET 
                        compra_anular=:compra_anular  
                        WHERE cod_compra=:cod_compra";
        $upda_compra = $DBcon->prepare($upda_compra);
        $upda_compra->bindparam(":compra_anular", $compra_anular,PDO::PARAM_STR);
        $upda_compra->bindparam(":cod_compra", $cod_compra,PDO::PARAM_STR);
                
        if($upda_compra->execute())
        {
            $sel_datcomp = "SELECT * FROM compras_detalle WHERE cod_compra=$cod_compra";
            $eje_datcomp = $DBcon->prepare($sel_datcomp);
            $eje_datcomp->execute();

            while($ver_datcomp = $eje_datcomp->fetch(PDO::FETCH_ASSOC))
            {
                $sel_datprod = "SELECT * FROM productos 
                                WHERE cod_producto='$ver_datcomp[cod_prod]'";
                $eje_datprod = $DBcon->prepare($sel_datprod);
                $eje_datprod->execute();
                $ver_datprod = $eje_datprod->fetch(PDO::FETCH_ASSOC);

                $tot_compra = $ver_datcomp["cantidad_comp"] + $ver_datcomp["bonificacion_comp"];

                $existe_prod = $ver_datprod["existencia_prod"];
                $costo_prom = $ver_datprod["precio_costo"];
                $precio = $ver_datprod["precio_venta"];

                if($existe_prod>0 and $existe_prod>=$tot_compra)
                {
                    $total_existe = $existe_prod-$tot_compra;
                }
                else
                {		
                }

                $upda_existe = "UPDATE productos 
                                SET 
                                existencia_prod=:tot_existe  
                                WHERE cod_producto=:cod_prod";
                $upda_existe = $DBcon->prepare($upda_existe);
                $upda_existe->bindparam(":cod_prod", $ver_datprod["cod_producto"],PDO::PARAM_STR);
                $upda_existe->bindparam(":tot_existe", $total_existe,PDO::PARAM_STR);
                

                $insert_kar = "INSERT INTO kardex (fecha_var,fecha_tot,cod_producto,
                            num_factura,costo_prod_kardex,precio_vta_kardex,
                            referencia_kardex,entrada_kardex,salida_kardex,total,
                            usuario)VALUES (:fecha_var,:fecha_tot,:cod_producto,
                            :num_factura,:costo_prod_kardex,:precio_vta_kardex,
                            :referencia_kardex,:entrada_kardex,:salida_kardex,:total,
                            :usuario)";
                $insert_kar = $DBcon->prepare($insert_kar);
                $insert_kar->bindparam(":fecha_var", $txt_fecha);
                $insert_kar->bindparam(":referencia_kardex", $referencia);
                $insert_kar->bindparam(":usuario", $usuario);
                $insert_kar->bindparam(":costo_prod_kardex", $costo_prom);
                $insert_kar->bindparam(":precio_vta_kardex", $precio);
                $insert_kar->bindparam(":fecha_tot", $fecha_det);
                $insert_kar->bindparam(":cod_producto", $ver_datprod["cod_producto"]);
                $insert_kar->bindparam(":num_factura", $NumFactura);
                $insert_kar->bindparam(":entrada_kardex", $entrada);
                $insert_kar->bindparam(":salida_kardex", $tot_compra);
                $insert_kar->bindparam(":total", $total_existe);
                
                if($insert_kar->execute())
                {
                    $upda_existe->execute();
                    $ValorInsert = 1;
                }
                else
                {
                    echo "\nError al Insertar en Kardex \n";
                    print_r($insert_kar->errorInfo());
                    echo "<br>El Codigo de Error es: ". $insert_kar->errorCode();
                    echo "\nError al Actualizar Existencia de los Productos \n";
                    print_r($upda_existe->errorInfo());
                    echo "<br>El Codigo de Error es: ". $upda_existe->errorCode();
                }
            }
        }
        else
        {
            echo "\nError al Anular la Factura:\n";
            print_r($upda_compra->errorInfo());
            echo "<br>El Codigo de Error es: ". $upda_compra->errorCode();
        }
        
        
        
        if($ValorInsert>0)
        {
            header('location: index.php');
        }
        
        

        
        

        
    }
    catch(PDOException $e)
	{
		echo "ERROR AL ANULAR LOS DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL ANULAR LOS DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL ANULAR LOS DATOS 3 ".$s->getMessage();
        exit;
    }
    
}
elseif($_POST['SqlQuery']=='eliminar')
{
    
    try
	{
        $txt_codigo     = $_POST['EliminarID'];
        $txt_compra     = $_POST['CodigoCompra'];
        $txt_detalle    = $_POST['CodDetCompra'];
		$texto			= "eliminar";
        
        
		$delete_prod = "DELETE FROM compras_detalle WHERE cod_det_compra=:cod_det_compra AND cod_compra = :cod_compra AND cod_prod=:cod_prod";
		$delete_prod = $DBcon->prepare($delete_prod);
        $delete_prod->bindParam(":cod_prod", $txt_codigo);
        $delete_prod->bindParam(":cod_compra", $txt_compra);
        $delete_prod->bindParam(":cod_det_compra", $txt_detalle);
        
        if($delete_prod->execute())
        {
            header("location: index.php?texto=$texto");
        }
		else
        {
            //echo '<script> alert("Error al Eliminar los Datos: '.$DBcon->errorInfo().' El Codigo de Error es: '.$DBcon->errorCode().' ")</script>';
            echo "\nError al Eliminar los Datos:\n";
            print_r($delete_prod->errorInfo());
            echo "<br>El Codigo de Error es: ". $delete_prod->errorCode();
        }
        /**/
        
	}
	catch(PDOException $e)
	{
		echo "ERROR AL ELIMINAR DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL ELIMINAR DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL ELIMINAR DATOS 3 ".$s->getMessage();
        exit;
    }
}
else
{
    
}
?>