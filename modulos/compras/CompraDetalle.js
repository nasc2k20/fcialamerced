$(function () {
    $('#ResultadoAutocomplete').hide();

    $('#BuscarProdCompraDet').typeahead({

        source: function (query, result) {
            $.ajax({
                url: "modulos/compras/ProductosAutcomplete.php",
                data: 'query=' + query,
                dataType: "json",
                type: "POST",
                success: function (data) {
                    result($.map(data, function (item) {
                        return item;
                    }));
                }
            });
        },
        itemSelected: function (data, value, text) {
            console.log(data + ' ' + value + ' ' + text);
        }
    });


    $('#BuscarProdCompraDet').keypress(function (e) {

        var Code = (e.keyCode ? e.keyCode : e.which);
        if (Code == 13) {
            CargarCodigo($(this).val());
        } else {
            $('#ResultadoAutocomplete').hide();
        }
    })

    $('#BtnAgregarCompraProd').click(function () {
        var Datos = $('#BuscarProdCompraDet').val();
        CargarCodigo(Datos);

    });

    $('#GuardarCompra').click(function () {
        var CodCompra = $('#cod_compra').val();
        var Proveedor = $('#proveedor').val();
        var Fecha = $('#fecha').val();
        var Factura = $('#num_fact').val();
        var Pago = $('#tipo_pago').val();
        var Vendedor = $('#vendedor').val();
        var Descuento = $('#desc_gen_fact').val();

    });



    $('#CantidadComprar').keypress(function (e) {
        var Code = (e.keyCode ? e.keyCode : e.which);
        if (Code == 13) {
            if ($(this).val() == '') {
                alert('No has digitado la Cantidad a Comprar');
            } else {
                $('#BonifComprar').focus();
            }
        }
    });

    $('#BonifComprar').keypress(function (e) {
        var Code = (e.keyCode ? e.keyCode : e.which);
        if (Code == 13) {
            if ($(this).val() == '') {
                $(this).val(0);
            } else {
                $('#PrecioComprar').focus();
            }
        }
    });

    $('#PrecioComprar').keypress(function (e) {
        var Code = (e.keyCode ? e.keyCode : e.which);
        if (Code == 13) {
            if ($(this).val() == '') {
                alert('Debe de Digitar el Precio de Compra');
            } else {
                $('#DescComprar').focus();
            }
        }
    });

    $('#DescComprar').keypress(function (e) {
        var Code = (e.keyCode ? e.keyCode : e.which);
        if (Code == 13) {
            if ($(this).val() == '') {
                $(this).val(0);
            } else {
                $('#ImpuestoComprar').focus();
            }
        }
    });

    $('#ImpuestoComprar').keypress(function (e) {
        var Code = (e.keyCode ? e.keyCode : e.which);
        if (Code == 13) {
            if ($(this).val() == '') {
                $(this).val(0);
            } else {
                $(this).val(1);
            }
            $('#AgregarCompraDet').focus();
        }
    });




})


function GuardarCompraTotal(CodCompra)
{
    var CodProveedor = $('#proveedor').val();
    var FechaCompra = $('#fecha').val();
    var NumFacCompra = $('#num_fact').val();
    var CodVendedor = $('#vendedor').val();
    var DescCompra = $('#desc_gen_fact').val();
    var TipoPago = $('#tipo_pago').val();
    var SqlQuery = 'insertar';
    
    var Parametros = {
        "CodCompra": CodCompra,
        "CodProveedor": CodProveedor,
        "FechaCompra": FechaCompra,
        "NumFacCompra": NumFacCompra,
        "CodVendedor": CodVendedor,
        "DescCompra": DescCompra,
        "TipoPago": TipoPago,
        "SqlQuery": SqlQuery
    };
    
    if(CodProveedor==''){
        alert('Debe de Seleccionar el Proveedor');
        $('#proveedor').focus();
    } else if(FechaCompra==''){
        alert('Debe de Seleccionar fecha de compra');
        $('#fecha').focus();
    } else if(NumFacCompra==''){
        alert('Debe de Ingresar Numero de Compra');
        $('#num_fact').focus();
    } else if(NumFacCompra==0){
        alert('Numero de Compra NO Valido');
        $('#num_fact').focus();
    } else if(DescCompra==''){
        $('#desc_gen_fact').val(0);
    } else if(TipoPago==''){
        alert('Debe de Seleccionar el Tipo de Pago');
        $('#tipo_pago').focus();
    } else if(CodCompra==''){
        alert('Debe de Seleccionar el Codigo de Compra');
    } else{
        $.ajax({
            url : "modulos/compras/ProcesosSQL.php", 
            method : "POST",
            data : Parametros,
            beforeSend : function(){
                $("#MostrarDatosCompras").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
            },
            success : function (data){
                $('#NuevaCompraProdModal').modal('hide');

                if ($('.modal-backdrop').is(':visible')) {
                    $('.modal-backdrop').remove();
                };

                $("#MostrarDatosCompras").html(data);
            }
        })
    }
    
    
    
    
}


function CargarCodigo(CodProducto) {
    var CodeData = '';
    var NombreData = '';
    var arrreglo = CodProducto.split(':');
    CodeData = arrreglo[1];
    NombreData = arrreglo[0];
    $('#CodProdCompraDet').val(CodeData);
    $('#NombreProdCompraDet').val(NombreData);

    if ($('#CodProdCompraDet').val() == '') {
        alert('No Hay datos que mostrar');
        $('#ResultadoAutocomplete').hide();
    } else {
        //alert('Producto Elegido es: '+RealData);
        $('#ResultadoAutocomplete').show();
        $('#CantidadComprar').focus();
    }
}


function selectCountry(CodProd, NombreProd) {
    $("#CodProdCompraDet").val(CodProd);
    $("#BuscarProdCompraDet").val(NombreProd);
    $("#ResultadoAutocomplete").hide();
}



function AgregarCompraDetProd() {
    var CodeProd = $('#CodProdCompraDet').val();
    var NombreProd = $('#NombreProdCompraDet').val();
    var CantProd = $('#CantidadComprar').val();
    var BonifProd = $('#BonifComprar').val();
    var PrecioProd = $('#PrecioComprar').val();
    var DescProd = $('#DescComprar').val();
    var ImpProd = '';
    var CodeCompra = $('#cod_compra').val();
    var SqlQuery = 'verificar';

    (CantProd == '') ? '0' : CantProd;
    (BonifProd == '') ? '0' : BonifProd;
    (PrecioProd == '') ? '0' : PrecioProd;
    (DescProd == '') ? '0' : DescProd;

    if (document.getElementById('ImpuestoComprar').checked) {
        ImpProd = 1;
    } else {
        ImpProd = 0;
    }

    var Parametros = {
        "CodeProd": CodeProd,
        "NombreProd": NombreProd,
        "CantProd": CantProd,
        "BonifProd": BonifProd,
        "PrecioProd": PrecioProd,
        "DescProd": DescProd,
        "ImpProd": ImpProd,
        "CodeCompra": CodeCompra,
        "SqlQuery": SqlQuery
    };


    if (CodeProd == '') {
        alert('El Codigo del Producto esta Vacio');
    } else if (NombreProd == '') {
        alert('El Nombre del Producto esta Vacio');
    } else if (CantProd == '') {
        alert('No has digitado la Cantidad a Comprar');
        $('#CantidadComprar').focus();
    } else if (BonifProd == '') {
        $('#BonifComprar').val(0);
    } else if (PrecioProd == '') {
        alert('Debe de Digitar el Precio de Compra');
        $('#PrecioComprar').focus();
    } else if (DescProd == '') {
        DescProd = $('#DescComprar').val(0);
    } else if (CodeCompra == '') {
        alert('No Has Seleccionado el Codigo de Compra');
    } else {
        $.ajax({
            url: "modulos/compras/ProcesosSQL.php",
            method: "POST",
            data: Parametros,
            beforeSend: function () {
                $("#DetalleNuevaCompras").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
            },
            success: function (data) {
                $("#DetalleNuevaCompras").html(data);
                $("#ResultadoAutocomplete").hide();
                $("#BuscarProdCompraDet").focus();
            }
        });
    }

}
