<?php
require_once '../../conectar.php';
$no = 1;

$counter_subto = 0;
$counter_desci = 0;
$sub_total = 0;
$iva_compra = 0;
//$cod_nfactu = $_GET["nfactu"];

$cod_nfactu = $_POST["CodCompra"];
    
    $sel_nfact_temp = "SELECT * FROM compras_detalle  
				  WHERE cod_compra=$cod_nfactu 
				  ORDER BY cod_det_compra ASC";
/*
if (isset($_POST['CodCompra']))
{
    $cod_nfactu = $_POST["CodCompra"];
    
    $sel_nfact_temp = "SELECT * FROM compras_detalle  
				  WHERE cod_compra=$cod_nfactu 
				  ORDER BY cod_det_compra ASC";
    
}
else
{
    $sel_nfact_temp = "";
}
*/
$eje_nfact_temp = $DBcon->prepare($sel_nfact_temp);
$eje_nfact_temp->execute();
?>
       <br>
        <table class="table table-borderless table-striped table-hover" style="font-size: 10pt;">
        <thead>
         
          <tr>
            <th class="center">Cant</th>
            <th class="center">Bonif.</th>
            <th class="center">Nombre de Producto</th>
            <th class="center">Precio Fact.</th>
            <th class="center">Desc.</th>
            <th class="center">Total</th>
            <th class="center"></th>
          </tr>
        </thead>
        <tbody>
        <?php
        while ($ver_nfact_temp = $eje_nfact_temp->fetch(PDO::FETCH_ASSOC)) {
       ?>
          <tr style="height: 5px;">
                  <td class='center' width='10%'><?php echo $ver_nfact_temp["cantidad_comp"]; ?></td>
                  <td class='center' width='10%'><?php echo $ver_nfact_temp["bonificacion_comp"]; ?></td>
                  <td class='left' width='41%'><?php echo $ver_nfact_temp["nombre_prod_comp"]; ?></td>
                  <td class='center' width='12%'>$ <?php echo $ver_nfact_temp["precio_facturacion"]; ?></td>
                  <td class='center' width='10%'><?php echo $ver_nfact_temp["descuento_ind"]; ?></td>
                  <td class='center' width='12%'>$ <?php echo $ver_nfact_temp["total_prod_compra"]; ?></td>
                  <td class='center' width='5%' data-toggle="tooltip" data-placement="top" title="Eliminar">
                    <button class="btn btn-danger btn-sm" onclick="EliminarProdDetCompra('<?php echo $ver_nfact_temp['cod_det_compra']; ?>','<?php echo $ver_nfact_temp["cod_prod"]; ?>','<?php echo $ver_nfact_temp['nombre_prod_comp']; ?>','<?php echo $ver_nfact_temp['cod_compra']; ?>')"><i class="fa fa-trash" aria-hidden="true" title="Eliminar"></i></button>
                  </td>
            </tr>
        <?php
            $counter_subto += $ver_nfact_temp["total_prod_compra"];
            $counter_desci += number_format(($ver_nfact_temp["total_prod_compra"]*$ver_nfact_temp["descuento_ind"])/100,2);
          $no++;
        }
        ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4" width='75%'></td>
                <td  width='10%'>Sumas: </td>
                <td  width='10%'>$<?php echo number_format($counter_subto,2); ?></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="4" width='75%'></td>
                <td  width='10%'>Desc: </td>
                <td  width='10%'>$<?php echo number_format($counter_desci,2); ?></td>
                <td></td>
            </tr>
               <tr>
                <td colspan="4" width='75%'></td>
                <td  width='10%'>Sub-Total: </td>
                <td  width='10%'>$<?php $sub_total = $counter_subto-$counter_desci; echo number_format($sub_total,2); ?></td>
                <td></td>
            </tr>
               <tr>
                <td colspan="4" width='75%'></td>
                <td  width='10%'>Iva: </td>
                <td  width='10%'>$<?php $iva_compra = number_format(($sub_total*13)/100,2); echo number_format($iva_compra,2); ?></td>
                <td></td>
            </tr>
            <tr>
                <td colspan="3" width='75%'></td>
                <td  width='10%' colspan="2" align="right">Total Compra: </td>
                <td  width='10%'>$<?php echo number_format($sub_total+$iva_compra,2); ?></td>
                <td></td>
            </tr>
        </tfoot>
      </table>
