<?php
require_once '../../conectar.php';
include_once '../../login/Paginator.php';

$BusquedaComp = $_REQUEST['BusquedaTxt'];

$ContarNum = 1;
$QueryPag = "";
$per_page = intval($_REQUEST['NumxPag']);

if($BusquedaComp!='')
{
    $QueryPag   = "SELECT count(*) as numrows 
                    FROM compras a 
                    INNER JOIN proveedor b ON b.cod_prov=a.cod_prov 
                    WHERE a.fecha_compra = '%".$BusquedaComp."%' 
                    OR a.num_factura LIKE '%".$BusquedaComp."%' 
                    OR b.nombre_prov LIKE '%".$BusquedaComp."%' 
                    AND compra_anular='NO' 
                    ORDER BY a.date_compra DESC";
}
else
{
    $QueryPag   = "SELECT count(*) AS numrows
                    FROM compras";
}

$eje_compra = $DBcon->prepare($QueryPag);
$eje_compra->execute();
$dat = $eje_compra->fetch(PDO::FETCH_ASSOC);
$numrows = $dat['numrows'];


$page = (isset($_REQUEST['NumPag']) && !empty($_REQUEST['NumPag']))?$_REQUEST['NumPag']:1;

$adjacents  = 4; //gap between pages after number of adjacents
$offset = ($page - 1) * $per_page;


$total_pages = ceil($numrows/$per_page);

if($BusquedaComp!='')
{
    $sel_comp = "SELECT * FROM compras a 
                INNER JOIN proveedor b ON b.cod_prov=a.cod_prov  
                WHERE a.fecha_compra = '%".$BusquedaComp."%' 
                OR a.num_factura LIKE '%".$BusquedaComp."%' 
                OR b.nombre_prov LIKE '%".$BusquedaComp."%' 
                AND compra_anular='NO' 
                ORDER BY a.date_compra DESC 
                LIMIT $offset, $per_page";
}
else
{
    $sel_comp = "SELECT * FROM compras a 
                INNER JOIN proveedor b ON b.cod_prov=a.cod_prov  
                WHERE compra_anular='NO' 
                ORDER BY a.date_compra DESC  
                LIMIT $offset, $per_page";
}

$eje_comp = $DBcon->prepare($sel_comp);
$eje_comp->execute();
?>

<div class="modal fade bs-example-modal-lg" id="DetalleCompra" tabindex="-1" role="dialog"
    aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Detalle de Compra</h4>
            </div>
            <div class="modal-body">
                <div id="loader" style="position: absolute;	text-align: center;	top: 55px;	width: 100%;display:none;">
                </div><!-- Carga gif animado -->
                <div class="resultados_prodDetLista" id="resultados_prodDetLista"></div><!-- Datos ajax Final -->
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times-circle-o"
                        aria-hidden="true"></i> Cerrar</button>
            </div>
        </div>
    </div>
</div>



<table id="dataTables1" class="table table-bordered table-striped table-hover">
    <thead>
        <tr>
            <th class="center">No.</th>
            <th class="center">Fecha</th>
            <th class="center">Num. Fact</th>
            <th>Nombre de Proveedor</th>
            <th class="center">Total</th>
            <th class="center" colspan="3" width="10%">Acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $finales=0;
        while ($ver_comp = $eje_comp->fetch(PDO::FETCH_ASSOC)) {
       ?>
        <tr>
            <td width='5%' class='center'><?php echo $ContarNum; ?></td>
            <td width='15%'>
                <?php $fecha_c_v = new datetime($ver_comp["date_compra"]); echo $fecha_c_v->format('d-m-Y') ?></td>
            <td class='center' width='10%'><?php echo $ver_comp["num_factura"]; ?></td>
            <td width='40%'><?php echo $ver_comp["nombre_prov"]; ?></td>
            <td class='center' width='15%' align='right'>$ <?php echo $ver_comp["total_compra"]; ?></td>
            <td class='center' width='5%'>
                <?php
                        echo ($ver_comp['compra_anular'])=='SI' ? '<i class="fa fa-check-circle" aria-hidden="true"></i>' : '<i class="fa fa-minus-circle" aria-hidden="true"></i>';
                      ?>
            </td>
            <td class='center' width='5%'>
                <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#DetalleCompra"
                    onclick="CargarCompraTemp(<?php echo $ver_comp['cod_compra']; ?>,<?php echo $ver_comp['cod_prov']; ?>)"><i
                        class="fa fa-search" aria-hidden="true"></i> </button>
            </td>
            <td class='center' width='5%' data-toggle="tooltip" data-placement="top" title="Anular">
                <button class="btn btn-warning btn-sm"
                    onclick="AnularCompra('<?php echo $ver_comp['cod_compra']; ?>','<?php echo $ver_comp['num_factura']; ?>')"><i
                        class="fa fa-download" aria-hidden="true"></i> </button>
            </td>
        </tr>
        <?php
          $finales++;
          $ContarNum++;
        }
        ?>
    </tbody>
    <tfoot>
        <tr>
            <td colspan="8">
                <?php 
                            $inicios=$offset+1;
                            $finales+=$inicios -1;
                            echo "Mostrando $inicios al $finales de $numrows registros";
                            echo paginate( $page, $total_pages, $adjacents);
                        ?>
            </td>
        </tr>
    </tfoot>
</table>