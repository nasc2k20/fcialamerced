<?php
include '../../conectar.php';

$sel_c = "SELECT MAX(cod_compra)AS cod_compra FROM compras";
$eje_c = $DBcon->prepare($sel_c);
$eje_c->execute();
$ver_c = $eje_c->fetch(PDO::FETCH_ASSOC);
$CodCompra = $ver_c['cod_compra']+1;

date_default_timezone_set("America/El_Salvador");

//include 'VerProductosModal.php';
?>
<script>
    function formato_fecha_nac() {
        cadena = document.getElementById('fecha').value;
        if (cadena.length == 2) {
            document.getElementById('fecha').value += "-";
        } else if (cadena.length == 5) {
            document.getElementById('fecha').value += "-";
        }
    }
</script>


<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2">
            <label for="">Corr:</label>
            <input type="text" class="form-control" id="cod_compra" value="<?php echo $CodCompra;?>" disabled>
        </div>
        <div class="col-sm-7">
            <label for="">Proveedor: </label>
            <?php
                include('../../conectar.php');
                $sel_prov = "SELECT * FROM proveedor  
                            ORDER BY nombre_prov ASC";
                $eje_prov = $DBcon->prepare($sel_prov);
                $eje_prov->execute();
            ?>
            <select class="form-control" id="proveedor">
                <?php
                while($ver_prov = $eje_prov->fetch(PDO::FETCH_ASSOC)){
            ?>
                <option value="<?php echo $ver_prov['cod_prov']; ?>">
                    <?php echo $ver_prov['nombre_prov']; ?>
                </option>
                <?php
                }
            ?>
            </select>
        </div>
        <div class="col-sm-3">
            <label for="">Fecha: </label>
            <div class="input-group">
                <input type="text" class="form-control" id="fecha" value="<?php echo date('d-m-Y'); ?>" onkeypress="formato_fecha_nac();" maxlength="10" placeholder="00-00-0000">
                <div class="input-group-addon">
                    <a href="#"><i class="fa fa-calendar"></i></a>
                </div>
            </div>
        </div>
    </div>

    <div style="height: 10px;"></div>

    <div class="row">
        <div class="col-sm-2">
            <label for="">Num. Fact:</label>
            <input type="number" class="form-control" id="num_fact" placeholder="0">
        </div>
        <div class="col-sm-2">
            <label for="">Tipo de Pago: </label>
            <select class="form-control" id="tipo_pago">
                <option value="credito">Cr&eacute;dito</option>
                <option value="efectivo" selected>Contado</option>
            </select>
        </div>
        <div class="col-sm-3">
            <label>Vendedor: </label>
            <select class="form-control" id="vendedor">
                <?php
                include('../../conectar.php');
                $sel_empl = "SELECT * FROM empleado  
                            ORDER BY nombre_emp ASC";
                $eje_empl = $DBcon->prepare($sel_empl);
                $eje_empl->execute();
                ?>
                <?php
                while($ver_empl = $eje_empl->fetch(PDO::FETCH_ASSOC)){
                ?>
                <option value="<?php echo $ver_empl['cod_empleado']; ?>">
                    <?php echo $ver_empl['nombre_emp']." ".$ver_empl['apellido_emp']; ?>
                </option>
                <?php
                }
                ?>
            </select>
        </div>
        <div class="col-sm-2">
            <label for="">Descuento: </label>
            <input type="text" class="form-control" id="desc_gen_fact" value="0">
        </div>
        <div class="col-sm-3">
            <div style="height: 25px;"></div>
            <button type="button" class="btn btn-success" id="GuardarCompra" onclick="GuardarCompraTotal(<?php echo $CodCompra; ?>);"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
            <button type="button" class="btn btn-danger" id="CerrarCompra"><i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar</button>
        </div>
    </div>

    <div style="height: 20px;"></div>

    <div class="row">
        <div class="col-sm-12">
            <input type="hidden" id="CodProdCompraDet" class="form-control">
            <input type="hidden" id="NombreProdCompraDet" class="form-control">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Busqueda Por Nombre" id="BuscarProdCompraDet" style="text-transform: uppercase;">

                <span class="input-group-btn">
                    <button class="btn btn-info" type="button" id="BtnAgregarCompraProd"><i class="fa fa-plus-square" aria-hidden="true"></i> Agregar</button>
                </span>
            </div>

        </div>
    </div>
    
    <div style="height: 10px;"></div>

        <div class="row" id="ResultadoAutocomplete">
            <div class="col-sm-2">
                <label for="CantidadComprar">Cant.</label>
                <input type="number" step="1" class="form-control" id="CantidadComprar">
            </div>
            <div class="col-sm-2">
                <label for="BonifComprar">Bonif.</label>
                <input type="number" step="1" class="form-control" id="BonifComprar">
            </div>
            <div class="col-sm-2">
                <label for="PrecioComprar">Precio.</label>
                <input type="text" class="form-control" id="PrecioComprar">
            </div>
            <div class="col-sm-2">
                <label for="DescComprar">Desc.</label>
                <input type="text" class="form-control" id="DescComprar">
            </div>
            <div class="col-sm-2">
                <label for="ImpuestoComprar">Imp.</label>
                <input type="checkbox" class="form-control" checked id="ImpuestoComprar">
            </div>
            <div class="col-sm-2">
                <div style="height: 30px;"></div>
                <button class="btn btn-primary btn-sm" type="submit" id="AgregarCompraDet" onclick="AgregarCompraDetProd();"><i class="fa fa-plus-square" aria-hidden="true"></i> </button>
            </div>
        </div>


    <div id="DetalleNuevaCompras"></div><!-- Carga los datos ajax -->
</div>





<script src="modulos/compras/CompraDetalle.js"></script>
