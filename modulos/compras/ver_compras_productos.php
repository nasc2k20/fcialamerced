<?php
require_once '../../conectar.php';

$sel_c = "SELECT MAX(cod_compra)AS cod_compra FROM compras";
$eje_c = $DBcon->prepare($sel_c);
$eje_c->execute();
$ver_c = $eje_c->fetch(PDO::FETCH_ASSOC);
$CodCompra = $ver_c['cod_compra']+1;

$record_per_page = 7;
$page = '';
$output = '';

if(isset($_POST["page"]))
{
    $page = $_POST["page"];
}
else
{
    $page = 1;
}

$start_from = ($page - 1)*$record_per_page;

//echo $start_from;

if (isset($_POST['query']))
{
    $sql_busqueda = $_POST['query'];
    $sel_prod = "SELECT * FROM productos a 
                INNER JOIN laboratorios b ON a.cod_lab=b.cod_lab 
                WHERE b.nombre_lab LIKE '%".$sql_busqueda."%' 
                OR a.nombre_producto LIKE '%".$sql_busqueda."%' 
                ORDER BY a.nombre_producto ASC 
                LIMIT $start_from, $record_per_page";
}
else
{
    $sel_prod = "SELECT * FROM productos a 
                INNER JOIN laboratorios b ON a.cod_lab=b.cod_lab ORDER BY a.nombre_producto ASC LIMIT $start_from, $record_per_page";
}


$eje_prod = $DBcon->prepare($sel_prod);
$eje_prod->execute();
$con_prod = $eje_prod->rowCount();

?>
<table id="dataTables1" class="table table-bordered table-striped table-hover">
    <thead>

        <tr>
            <th class="center" colspan="2">Cant + Bon</th>
            <th class="center">Nombre de Producto</th>
            <th class="center">Saldo</th>
            <th class="center">Precio Fact.</th>
            <th class="center">Desc.</th>
            <th class="center" colspan="2"></th>
        </tr>
    </thead>
    <tbody>
        <?php
        while ($ver_prod = $eje_prod->fetch(PDO::FETCH_ASSOC)) {
            $Cod_Prod = $ver_prod["cod_producto"];
            $CodProdComp = $CodCompra."".$ver_prod["cod_producto"];
       ?>
        <!-- modulos/compras/ProcesosSQL.php?SqlQuery=verificar 
         <form action="" method="post" name="insertar_det_compra" id="insertar_det_compra"> -->
        <tr>
            <input type="hidden" id="cod_producto_<?php echo $Cod_Prod; ?>" value="<?php echo $Cod_Prod; ?>">
            <input type="hidden" id="cod_compra_<?php echo $Cod_Prod; ?>" value="<?php echo $CodCompra; ?>">
            <input type="hidden" id="cod_producto2" value="<?php echo $Cod_Prod; ?>">
            <input type="hidden" id="cod_compra2" value="<?php echo $CodCompra; ?>">

            <td class='center' width='10%' align='right'><input type="text" id="cant_compra_<?php echo $Cod_Prod; ?>" class="form-control" value="1"></td>
            <td class='center' width='10%' align='right'><input type="text" id="bon_compra_<?php echo $Cod_Prod; ?>" class="form-control" value="0"></td>
            <td width='35%'>
                <?php echo $ver_prod["nombre_producto"]; ?><br><span style="font-size:x-small; color:blue;">
                    <?php echo $ver_prod["codigo_barra_prod"]." - ".$ver_prod["nombre_lab"]; ?></span></td>
            <td class='center' width='10%' align='right'>
                <?php echo $ver_prod["existencia_prod"]; ?>
            </td>
            <td class='center' width='10%' align='right'><input type="text" id="precio_fact_<?php echo $Cod_Prod; ?>" class="form-control" value="0.00"></td>
            <td class='center' width='10%' align='right'><input type="text" id="desc_ind_fact_<?php echo $Cod_Prod; ?>" class="form-control" value="0"></td>
            <td class='center' width='5%'><input type="checkbox" value="1" id="iva_<?php echo $Cod_Prod; ?>" checked></td>
            <td class='center' width='5%'>
                <button type="button" class="btn btn-success" id="BtnAgregarCompraDet" onclick="agregar_compra_det('<?php echo $Cod_Prod; ?>');"><i class="fa fa-cart-plus" aria-hidden="true"></i> </button>
                <!--
                      <a href="#" class="btn btn-success"  id="guardar_datos_det" onclick="agregar_compra_det('<?php //echo $Cod_Prod; ?>');" ><i class="fa fa-cart-plus" aria-hidden="true"></i></a>
                      -->
            </td>
        </tr>
        <!-- </form> -->
        <?php
        }
        ?>
    </tbody>
    <tfoot>

    </tfoot>
</table>
<span class="pull-right">
    <div style="text-aling: center;">
        <?php
                    $page_query = "SELECT * FROM productos a 
                                    INNER JOIN laboratorios b ON a.cod_lab=b.cod_lab 
                                    ORDER BY a.nombre_producto ASC";
                     $page_result = $DBcon->prepare($page_query);
                     $page_result->execute();
                     $total_records = $page_result->rowCount();
                     //echo $total_records;
                     $total_pages = ceil($total_records/$record_per_page);
                     
                     
                     for($i=1; $i<=$total_pages; $i++)
                     {
                          echo "<span class='pagination_link btn btn-default' style='cursor:pointer; padding:6px; border:1px solid #ccc;' id='".$i."'>".$i."</span>";
                     }
                ?>
    </div>
</span>


<script>
    function agregar_compra_det(cod_producto) {

        var cantidad = $('#cant_compra_' + cod_producto).val();
        var bonificacion = $('#bon_compra_' + cod_producto).val();
        var precio = $('#precio_fact_' + cod_producto).val();
        var descuento = $('#desc_ind_fact_' + cod_producto).val();
        var iva = $('#iva_' + cod_producto).val();
        var cod_compra = $('#cod_compra_' + cod_producto).val();
        var SqlQuery = 'verificar';


        if (document.getElementById('iva_' + cod_producto).checked) {
            iva = 1;
        } else {
            iva = 0;
        }





        var parametros = {
            "cod_compra": cod_compra,
            "cantidad": cantidad,
            "bonificacion": bonificacion,
            "precio": precio,
            "descuento": descuento,
            "iva": iva,
            "cod_producto": cod_producto,
            "SqlQuery": SqlQuery
        };

        $.ajax({
            type: "POST",
            url: "modulos/compras/ProcesosSQL.php",
            data: parametros,
            beforeSend: function(objeto) {
                $("#DetalleNuevaCompras").html("Mensaje: Cargando...");
            },
            success: function(datos) {
                $("#DetalleNuevaCompras").html(datos);

                //$('#myModal').modal().hide();
                //$('.modal-backdrop').remove();
                //$("#myModal").modal().hide() ;//ocultamos el modal
                //$('body').removeClass('modal-open');//eliminamos la clase del body para poder hacer scroll
                //$('.modal-backdrop').remove();//eliminamos el backdrop del modal
            }
        });
    }

</script>
