<script>
function formato_telefono(CodigoProv) {
	cadena = document.getElementById('TelefonoProv_'+CodigoProv).value;
	if(cadena.length == 4) { 
		document.getElementById('TelefonoProv_'+CodigoProv).value += "-";
	}
}

function formato_celular(CodigoProv) {
	cadena = document.getElementById('CelularProv_'+CodigoProv).value;
	if(cadena.length == 4) { 
		document.getElementById('CelularProv_'+CodigoProv).value += "-";
	}
}
</script>
<div class="modal fade" tabindex="-1" role="dialog" id="EditarProvModal<?php echo $ver_prov['cod_prov']; ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nuevo Proveedor</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="NrcProv_<?php echo $ver_prov['cod_prov']; ?>">NRC: </label>
                    <input type="text" class="form-control" id="NrcProv_<?php echo $ver_prov['cod_prov']; ?>" placeholder="000000-0" value="<?php echo $ver_prov['nrc_prov']; ?>" readonly>
                </div>
                <div class="form-group">
                    <label for="NombreProv_<?php echo $ver_prov['cod_prov']; ?>">Nombre: </label>
                    <input type="text" class="form-control" id="NombreProv_<?php echo $ver_prov['cod_prov']; ?>" placeholder="Nombre del Proveedor" style="text-transform: uppercase;" value="<?php echo $ver_prov['nombre_prov']; ?>">
                </div>
                <div class="form-group">
                    <label for="TelefonoProv_<?php echo $ver_prov['cod_prov']; ?>">Telefono: </label>
                    <input type="tex" class="form-control" id="TelefonoProv_<?php echo $ver_prov['cod_prov']; ?>" maxlength="9" onkeypress="formato_telefono('<?php echo $ver_prov['cod_prov']; ?>');" placeholder="0000-0000" value="<?php echo $ver_prov['telefono_prov']; ?>">
                </div>
                <div class="form-group">
                    <label for="VendedorProv_<?php echo $ver_prov['cod_prov']; ?>">Vendedor: </label>
                    <input type="text" class="form-control" id="VendedorProv_<?php echo $ver_prov['cod_prov']; ?>" placeholder="Nombre del Vendedor" style="text-transform: uppercase;" value="<?php echo $ver_prov['nombre_vend_prov']; ?>">
                </div>
                <div class="form-group">
                    <label for="CelularProv_<?php echo $ver_prov['cod_prov']; ?>">Celular: </label>
                    <input type="text" class="form-control" id="CelularProv_<?php echo $ver_prov['cod_prov']; ?>" maxlength="9" onkeypress="formato_celular('<?php echo $ver_prov['cod_prov']; ?>');" placeholder="0000-0000" value="<?php echo $ver_prov['telefono_vend_prov']; ?>">
                </div>
                <div class="form-group">
                    <label for="DiasProv_<?php echo $ver_prov['cod_prov']; ?>">Dias Cr&eacute;dito: </label>
                    <select id="DiasProv_<?php echo $ver_prov['cod_prov']; ?>" class="form-control">
                        <option value="<?php 
                                       $ValorDia = 0;
                                       switch($ver_prov['dias_credito_prov'])
                                       {
                                            case "0":
                                               $ValorDia = 0;
                                            break;
                                            case "30":
                                               $ValorDia = 30;
                                            break;
                                            case "60":
                                               $ValorDia = 60;
                                            break;
                                            case "90":
                                               $ValorDia = 90;
                                            break;
                                            case "120":
                                               $ValorDia = 120;
                                            break;
                                       }
                                       echo $ValorDia; 
                                       ?>"><?php
                                            $TextoDia = 0;
                                               switch($ver_prov['dias_credito_prov'])
                                               {
                                                    case "0":
                                                       $TextoDia = "0 D&iacute;as";
                                                    break;
                                                    case "30":
                                                       $TextoDia = "30 D&iacute;as";
                                                    break;
                                                    case "60":
                                                       $TextoDia = "60 D&iacute;as";
                                                    break;
                                                    case "90":
                                                       $TextoDia = "90 D&iacute;as";
                                                    break;
                                                    case "120":
                                                       $TextoDia = "120 D&iacute;as";
                                                    break;
                                               }
                                            echo $TextoDia; 
                                            ?></option>
                        <option value="0">0 D&iacute;as</option>
                        <option value="30">30 D&iacute;as</option>
                        <option value="60">60 D&iacute;as</option>
                        <option value="90">90 D&iacute;as</option>
                        <option value="120">120 D&iacute;as</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar</button>
                <button type="button" class="btn btn-primary" id="GuardarProv" onclick="EditarProv('<?php echo $ver_prov['cod_prov']; ?>');"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->