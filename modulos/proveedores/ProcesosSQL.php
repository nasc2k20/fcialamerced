<?php
require '../../conectar.php';

date_default_timezone_set("America/El_Salvador");

if($_POST['SqlQuery']=='insertar')
{
    try
	{		
		$txt_nrc		= $_POST["NrcProv"];
		$txt_nombre		= strtoupper($_POST["NombreProv"]);
		$txt_vendedor	= strtoupper($_POST["VendedorProv"]);
		$txt_telefono	= ($_POST["TelefonoProv"])=='' ? '0000-0000' : $_POST['TelefonoProv'];
		$txt_credito	= $_POST["DiasProv"];
		$txt_celular	= ($_POST["CelularProv"])=='' ? '0000-0000' : $_POST['CelularProv'];
        $texto          = "agregar";
		
		$inser_prov = "INSERT INTO proveedor (nrc_prov,nombre_prov,
					  telefono_prov,nombre_vend_prov,
					  telefono_vend_prov,dias_credito_prov) 
					  VALUES (:nrc_prov,:nombre_prov,:telefono_prov,:nombre_vend_prov
					  ,:telefono_vend_prov,:dias_credito_prov)";
		$inser_prov = $DBcon->prepare($inser_prov);
		$inser_prov->bindparam(":nrc_prov", $txt_nrc,PDO::PARAM_STR);
		$inser_prov->bindparam(":nombre_prov", $txt_nombre,PDO::PARAM_STR);
		$inser_prov->bindparam(":telefono_prov", $txt_telefono,PDO::PARAM_STR);
		$inser_prov->bindparam(":nombre_vend_prov", $txt_vendedor,PDO::PARAM_STR);
		$inser_prov->bindparam(":telefono_vend_prov", $txt_celular,PDO::PARAM_STR);
		$inser_prov->bindparam(":dias_credito_prov", $txt_credito,PDO::PARAM_STR);
		
        if($inser_prov->execute())
        {
            header("location: index.php?&texto=$texto");
        }
		else
        {
            echo "\nError al Agregar los Datos:\n";
            print_r($inser_prov->errorInfo());
            echo "<br>El Codigo de Error es: ". $inser_prov->errorCode();
        }
	}
	catch(PDOException $e)
	{
		echo "ERROR AL AGREGAR DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL AGREGAR DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL AGREGAR DATOS 3 ".$s->getMessage();
        exit;
    }
}
elseif($_POST['SqlQuery']=='actualizar')
{
    try
	{
        $cod_prov       = $_POST["CodigoProv"];
        $txt_nrc		= $_POST["NrcProv"];
		$txt_nombre		= strtoupper($_POST["NombreProv"]);
		$txt_vendedor	= strtoupper($_POST["VendedorProv"]);
		$txt_telefono	= ($_POST["TelefonoProv"])=='' ? '0000-0000' : $_POST['TelefonoProv'];
		$txt_credito	= $_POST["DiasProv"];
		$txt_celular	= ($_POST["CelularProv"])=='' ? '0000-0000' : $_POST['CelularProv'];
		$texto          = "editar";
        
		$update_prov = "UPDATE proveedor 
					  SET
					  nrc_prov=:nrc_prov,
					  nombre_prov=:nombre_prov,
					  telefono_prov=:telefono_prov,
					  nombre_vend_prov=:nombre_vend_prov,
					  telefono_vend_prov=:telefono_vend_prov,
					  dias_credito_prov=:dias_credito_prov 
					  WHERE cod_prov=:cod_prov";
		$update_prov = $DBcon->prepare($update_prov);
		$update_prov->bindparam(":cod_prov", $cod_prov,PDO::PARAM_STR);
		$update_prov->bindparam(":nrc_prov", $txt_nrc,PDO::PARAM_STR);
		$update_prov->bindparam(":nombre_prov", $txt_nombre,PDO::PARAM_STR);
		$update_prov->bindparam(":telefono_prov", $txt_telefono,PDO::PARAM_STR);
		$update_prov->bindparam(":nombre_vend_prov", $txt_vendedor,PDO::PARAM_STR);
		$update_prov->bindparam(":telefono_vend_prov", $txt_celular,PDO::PARAM_STR);
		$update_prov->bindparam(":dias_credito_prov", $txt_credito,PDO::PARAM_STR);
		
        if($update_prov->execute())
        {
            header("location: index.php?&texto=$texto");
        }
		else
        {
            echo "\nError al Actualizar los Datos:\n";
            print_r($update_prov->errorInfo());
            echo "<br>El Codigo de Error es: ". $update_prov->errorCode();
        }
	}
	catch(PDOException $e)
	{
		echo "ERROR AL ACTUALIZAR LOS DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL ACTUALIZAR LOS DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL ACTUALIZAR LOS DATOS 3 ".$s->getMessage();
        exit;
    }
}
elseif($_POST['SqlQuery']=='eliminar')
{
    
    try
	{
        $txt_codigo     = $_POST['EliminarID'];
		$texto			= "eliminar";
        
        
		$delete_prov = "DELETE FROM proveedor WHERE cod_prov=:CodProv";
		$delete_prov = $DBcon->prepare($delete_prov);
        $delete_prov->bindParam(":CodProv", $txt_codigo);
        
        if($delete_prov->execute())
        {
            header("location: index.php?&texto=$texto");
        }
		else
        {
            echo "\nError al Eliminar los Datos:\n";
            print_r($delete_prov->errorInfo());
            echo "<br>El Codigo de Error es: ". $delete_prov->errorCode();
        }
	}
	catch(PDOException $e)
	{
		echo "ERROR AL ELIMINAR DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL ELIMINAR DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL ELIMINAR DATOS 3 ".$s->getMessage();
        exit;
    }
}
else
{
    
}
?>