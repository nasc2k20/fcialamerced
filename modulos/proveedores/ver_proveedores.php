<?php
require_once '../../conectar.php';
include_once '../../login/Paginator.php';

$BusquedaProv = $_REQUEST['BusquedaProv'];

$ContarNum = 1;
$QueryPag = "";
$per_page = intval($_REQUEST['NumxPag']);

if($BusquedaProv!='')
{
    $QueryPag   = "SELECT count(*) AS numrows 
                    FROM proveedor 
                    WHERE nombre_prov LIKE '%".$BusquedaProv."%' 
                    OR nrc_prov LIKE '%".$BusquedaProv."%' 
                    ORDER BY nombre_prov ASC";
}
else
{
    $QueryPag   = "SELECT count(*) AS numrows
                    FROM proveedor";
}

$eje_prov = $DBcon->prepare($QueryPag);
$eje_prov->execute();
$dat = $eje_prov->fetch(PDO::FETCH_ASSOC);
$numrows = $dat['numrows'];


$page = (isset($_REQUEST['NumPag']) && !empty($_REQUEST['NumPag']))?$_REQUEST['NumPag']:1;

$adjacents  = 4; //gap between pages after number of adjacents
$offset = ($page - 1) * $per_page;


$total_pages = ceil($numrows/$per_page);


if($BusquedaProv!='')
{
    $sel_prov = "SELECT * FROM proveedor  
                WHERE nombre_prov LIKE '%".$BusquedaProv."%' 
                OR nrc_prov LIKE '%".$BusquedaProv."%' 
                ORDER BY nombre_prov ASC 
                LIMIT $offset, $per_page";
}
else
{
    $sel_prov = "SELECT * FROM proveedor 
                ORDER BY nombre_prov ASC   
                LIMIT $offset, $per_page";
}

$eje_prov = $DBcon->prepare($sel_prov);
$eje_prov->execute();
?>
        <table class="table table-bordered table-striped table-hover">
        <thead>
          <tr>
            <th>No.</th>
            <th>N.R.C.</th>
            <th>Nombre deL Proveedor</th>
            <th>Vendedor</th>
            <th>Celular</th>
            <th colspan="2" width="10%">Acciones</th>
          </tr>
        </thead>
        <tbody>
        <?php
        $finales=0;
        while ($ver_prov = $eje_prov->fetch(PDO::FETCH_ASSOC)) {
       ?>
          <tr>
                  <td width='5%'><?php echo $ContarNum; ?></td>
                  <td width='10%'><?php echo $ver_prov["nrc_prov"]; ?></td>
                  <td width='35%'><?php echo $ver_prov["nombre_prov"]; ?></td>
                  <td width='25%'><?php echo $ver_prov["nombre_vend_prov"]; ?></td>
                  <td width='10%' align='right'><?php echo $ver_prov["telefono_vend_prov"]; ?></td>
                  <td width='5%'>
                    <span data-toggle='tooltip' data-placement='top' title='Modificar'>
                        <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#EditarProvModal<?php echo $ver_prov['cod_prov']; ?>"><i class="fa fa-pencil-square" aria-hidden="true"></i> </button>
                    </span>
                  </td>
                  <td class='center' width='5%'>
                    <span data-toggle="tooltip" data-placement="top" title="Eliminar">
                        <button class="btn btn-danger btn-sm" onclick="EliminarProv('<?php echo $ver_prov['cod_prov']; ?>','<?php echo $ver_prov['nombre_prov']; ?>')"><i class="fa fa-trash" aria-hidden="true" title="Eliminar"></i></button>
                    </span>
                  </td>
                </tr>
        <?php
            
            include 'EditarProvModal.php';
            
            $finales++;
            $ContarNum++;
        }
        ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="7">
                    <?php 
                            $inicios=$offset+1;
                            $finales+=$inicios -1;
                            echo "Mostrando $inicios al $finales de $numrows registros";
                            echo paginate( $page, $total_pages, $adjacents);
                        ?>
                </td>
            </tr>
        </tfoot>
      </table>
