<script>
function formato_telefono() {
	cadena = document.getElementById('TelefonoProv').value;
	if(cadena.length == 4) { 
		document.getElementById('TelefonoProv').value += "-";
	}
}

function formato_celular() {
	cadena = document.getElementById('CelularProv').value;
	if(cadena.length == 4) { 
		document.getElementById('CelularProv').value += "-";
	}
}
</script>
<div class="modal fade" tabindex="-1" role="dialog" id="AgregarProvModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nuevo Proveedor</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="NrcProv">NRC: </label>
                    <input type="text" class="form-control" id="NrcProv" placeholder="000000-0">
                    <div id="ComprobarNrc"></div>
                </div>
                <div class="form-group">
                    <label for="NombreProv">Nombre: </label>
                    <input type="text" class="form-control" id="NombreProv" placeholder="Nombre del Proveedor" style="text-transform: uppercase;">
                </div>
                <div class="form-group">
                    <label for="TelefonoProv">Telefono: </label>
                    <input type="tex" class="form-control" id="TelefonoProv" maxlength="9" onkeypress="formato_telefono();" placeholder="0000-0000">
                </div>
                <div class="form-group">
                    <label for="VendedorProv">Vendedor: </label>
                    <input type="text" class="form-control" id="VendedorProv" placeholder="Nombre del Vendedor" style="text-transform: uppercase;">
                </div>
                <div class="form-group">
                    <label for="CelularProv">Celular: </label>
                    <input type="text" class="form-control" id="CelularProv" maxlength="9" onkeypress="formato_celular();" placeholder="0000-0000">
                </div>
                <div class="form-group">
                    <label for="DiasProv">Dias Cr&eacute;dito: </label>
                    <select id="DiasProv" class="form-control">
                        <option value="0">0 D&iacute;as</option>
                        <option value="30">30 D&iacute;as</option>
                        <option value="60">60 D&iacute;as</option>
                        <option value="90">90 D&iacute;as</option>
                        <option value="120">120 D&iacute;as</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar</button>
                <button type="button" class="btn btn-primary" id="GuardarProv" onclick="AgregarProv();"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->