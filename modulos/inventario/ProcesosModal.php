<div class="modal fade" tabindex="-1" role="dialog" id="AumentarTomaModal<?php echo $ver_prod['cod_producto']; ?>">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Aumentar Existencia</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <!--<input type="hidden" id="Codeprod" value="<?php echo $ver_prod['cod_producto']; ?>">-->
                    <label for="SaldoProd_<?php echo $ver_prod['cod_producto']; ?>">Saldo: </label>
                    <input type="number" min="1" class="form-control" id="SaldoProd_<?php echo $ver_prod['cod_producto']; ?>" value="<?php echo $ver_prod['existencia_prod']; ?>" readonly>
                </div>
                <div class="form-group">
                    <label for="AumentarCant_<?php echo $ver_prod['cod_producto']; ?>">Cantidad: </label>
                    <input type="number" min="1" class="form-control" id="AumentarCant_<?php echo $ver_prod['cod_producto']; ?>" onkeyup="CalcularExistenciaAum('<?php echo $ver_prod['cod_producto']; ?>');" placeholder="0">
                </div>
                <div class="form-group">
                    <label for="NuevoSaldo_<?php echo $ver_prod['cod_producto']; ?>">Nueva Existencia: </label>
                    <input type="number" min="1" class="form-control" id="NuevoSaldo_<?php echo $ver_prod['cod_producto']; ?>" readonly>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="AumentarExistencia('<?php echo $ver_prod['cod_producto']; ?>');"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="DisminuirTomaModal<?php echo $ver_prod['cod_producto']; ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Disminuir Existencia</h4>
      </div>
      <div class="modal-body">
        <div class="form-group">
          <!--<input type="hidden" id="CodeprodDis" value="<?php echo $ver_prod['cod_producto']; ?>">-->
          <label for="SaldoProdDis_<?php echo $ver_prod['cod_producto']; ?>">Saldo: </label>
          <input type="number" min="1" class="form-control" id="SaldoProdDis_<?php echo $ver_prod['cod_producto']; ?>" value="<?php echo $ver_prod['existencia_prod']; ?>" readonly>
        </div>
        <div class="form-group">
          <label for="AumentarCantDis_<?php echo $ver_prod['cod_producto']; ?>">Cantidad: </label>
          <input type="number" min="1" class="form-control" id="AumentarCantDis_<?php echo $ver_prod['cod_producto']; ?>" onkeyup="CalcularExistenciaDis('<?php echo $ver_prod['cod_producto']; ?>','<?php echo $ver_prod['existencia_prod']; ?>');" placeholder="0">
        </div>
        <div class="form-group">
          <label for="NuevoSaldoDis_<?php echo $ver_prod['cod_producto']; ?>">Nueva Existencia: </label>
          <input type="number" min="1" class="form-control" id="NuevoSaldoDis_<?php echo $ver_prod['cod_producto']; ?>" readonly>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="DisminuirExistencia('<?php echo $ver_prod['cod_producto']; ?>');"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->