<?php
require_once '../../conectar.php';
$no = 1;
$InventarioEstado = "";

if (isset($_POST['query']))
{
    //echo "aqui";
    $sql_busqueda = $_POST['query'];
    //$sel_lab = "SELECT * FROM laboratorios WHERE nombre_lab LIKE '%".$sql_busqueda."%' ORDER BY nombre_lab ASC LIMIT $start_from, $record_per_page";
    $sel_invs = "SELECT * FROM alta_inventario WHERE fecha_inv LIKE '%".$sql_busqueda."%' ORDER BY date_inv DESC";
}
else
{
    $sel_invs = "SELECT * FROM alta_inventario ORDER BY date_inv DESC";
}


$eje_invs = $DBcon->prepare($sel_invs);
$eje_invs->execute();
?>
        <table id="dataTables1" class="table table-bordered table-striped table-hover">
        <thead>
          <tr>
            <th class="center">No.</th>
            <th class="center">Identificador</th>
            <th class="center">Fecha Inicio</th>
            <th class="center">Fecha Cierre</th>
            <th class="center">Responsable</th>
            <th class="center" colspan="3" width="10%">Acciones</th>
          </tr>
        </thead>
        <tbody>
        <?php
        while ($ver_invs = $eje_invs->fetch(PDO::FETCH_ASSOC)) {
            $name_inv = $ver_invs["abreviacion_inv_letras"]."-".$ver_invs["abreviacion_inv_num"];
            $InventarioEstado = $ver_invs["estado_inv"];
            //$estado_inv = "";
            /**/
            if($ver_invs["estado_inv"]=='EN PROCESO'){
                $estado_inv = '<i class="fa fa-folder-open fa-2x" aria-hidden="true" style="color: orange;" title="En Proceso"></i>';
                echo '<script>$("#Reabrir").attr("disabled", true);</script>';
                echo '<script>$("#NuevoInv").attr("disabled", true);</script>';
            }elseif($ver_invs["estado_inv"]=='FINALIZADO'){
                $estado_inv = '<i class="fa fa-folder-open fa-2x" aria-hidden="true" style="color: green;" title="Finalizado"></i>';
                echo '<script>$("#Reabrir").attr("enable", true);</script>';
                echo '<script>$("#NuevoInv").attr("enable", true);</script>';
                echo '<script>$("#Cerrar").attr("disabled", true); $("a").removeAttr("href");</script>';
                //echo '<script>document.getElementById("Cerrar").disabled = true; document.getElementById("Cerrar").removeAttribute("href");</script>';
            }else{
                $estado_inv = '<i class="fa fa-folder-open fa-2x" aria-hidden="true" style="color: rose;" title="<?php echo $estado_inv; ?>"></i>';
            }
            
       ?>
          <tr>
              <td width='10%' class='center'><?php echo $no; ?></td>
              <td width='25%'><?php echo $name_inv; ?></td>
              <td class='center' width='15%'><?php $fecha_form = new datetime($ver_invs["fecha_inv"]); echo $fecha_form->format('d-m-Y'); ?></td>
              <td class='center' width='15%' align='right'>
              <?php 
                    $fecha_formf = new datetime($ver_invs["fecha_inv_cierre"]);
                    $fecha_FinInv = $fecha_formf->format('d-m-Y');
                    echo ($ver_invs["fecha_inv_cierre"]) ? $fecha_FinInv : '<i class="fa fa-window-close" aria-hidden="true" style="color: red;"></i>'; 
                  ?>
              </td>
              <td class='center' width='20%' align='right'><?php echo $ver_invs["usuario"]; ?></td>
              <td class='center' width='5%'><?php echo $estado_inv; ?></td>
              <td class='center' width='5%'>
                 <button class="btn btn-primary btn-sm" title="Reabrir" id="Reabrir" onclick="ReabrirInventario('<?php echo $ver_invs['cod_inventario']; ?>')"><i class="fa fa-reply" aria-hidden="true"></i></button>
                  <!--
                  <a data-toggle='tooltip' data-placement='top' title='Reabrir' style='margin-right:5px' class='btn btn-primary btn-sm' href='?module=EditarLab&accion=editar&CodLab=<?php //echo $ver_invs['cod_inventario']; ?>' id='Reabrir'><i class="fa fa-reply" aria-hidden="true"></i>
                  </a>-->
              </td>
              <td class='center' width='5%'>
                 <button class="btn btn-danger btn-sm" title="Cerrar" onclick="CerrarInventario('<?php echo $ver_invs['cod_inventario']; ?>');" id="Cerrar"><i class="fa fa-reply" aria-hidden="true"></i></button>
                 <!--
                  <a data-toggle="tooltip" data-placement="top" title="Cerrar" class="btn btn-danger btn-sm" href="modulos/laboratorios/ProcesosSQL.php?SqlQuery=eliminar&CodLab=<?php //echo $ver_invs['cod_inventario']; ?>" onclick="return confirm('¿Desea Eliminar el Laboratorio: <?php //echo $name_inv; ?> ?');" id="Cerrar">
                      <i class="fa fa-lock" aria-hidden="true" title="Cerrar" ></i>
                  </a>
                  -->
              </td>
        </tr>
        <?php
            
          $no++;
        }
        ?>
        </tbody>
      </table>
<script>
    /*
    $(document).ready(function(){
        $('#Reabrir').click(function(){
            CerrarInventario();
        });
        
        $('#Cerrar').click(function(){
            CerrarInventario();
        });
    });
    */
    function ReabrirInventario(CodInv){
        var accion = "reabrir_inv";
        var dataString = {"CodInv":CodInv,"SqlQuery":accion};
        var Confirmacion = confirm('¿Desea Reabrir el Inventario: <?php echo $name_inv; ?> ?');
        
        if(Confirmacion==true){
            $.ajax({
                type: 'POST',
                url: 'modulos/inventario/ProcesosSQL.php',
                data: dataString,
                success: function(data){
                    $("#outer_div_inventario").html(data);
                }
            });
        }
    }
    
    
    function CerrarInventario(CodInv){
        var accion = "cerrar_inv";
        //var dataString = 'CodInv='+CodInv, 'SqlQuery='+accion;
        var dataString = {"CodInv":CodInv,"SqlQuery":accion};
        var Confirmacion = confirm('¿Desea Cerrar el Inventario: <?php echo $name_inv; ?> ?');
        
        if(Confirmacion==true){
            $.ajax({
                type: 'POST',
                url: 'modulos/inventario/ProcesosSQL.php',
                data: dataString,
                success: function(data){
                    $("#outer_div_inventario").html(data);
                }
            });
        }
    }
</script>
