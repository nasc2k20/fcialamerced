<?php
include '../../login/validar.php';

require '../../conectar.php';


date_default_timezone_set("America/El_Salvador");

$usuario = $_SESSION['usuario'];

if($_GET['SqlQuery']=='insertar')
{
    try
	{
        $txt_corrNum       = $_POST['InvNum'];
        $txt_corrLet       = $_POST['InvLetras'];
        $txt_estado        = $_POST['InvEstado'];
		$txt_fecha_f       = new datetime($_POST["FechaIniInv"]);
        $txt_fechaInicio   = $txt_fecha_f->format('Y-m-d');
		$txt_digit         = date('Y-m-d H:i:s');
		$texto             = "agregar";
	
		$insert_invs = "INSERT INTO alta_inventario (abreviacion_inv_num, abreviacion_inv_letras,fecha_inv,date_inv,estado_inv,usuario) 
						VALUES (:abreviacion_inv_num, :abreviacion_inv_letras, :fecha_inv, :date_inv, :estado_inv, :usuario)";
		$insert_invs = $DBcon->prepare($insert_invs);
		$insert_invs->bindparam(":abreviacion_inv_num", $txt_corrNum);
		$insert_invs->bindparam(":abreviacion_inv_letras", $txt_corrLet);
		$insert_invs->bindparam(":fecha_inv", $txt_fechaInicio);
		$insert_invs->bindparam(":date_inv", $txt_digit);
		$insert_invs->bindparam(":estado_inv", $txt_estado);
		$insert_invs->bindparam(":usuario", $usuario);
		
        
        if($insert_invs->execute())
        {
            header("location: ../../main.php?module=DetalleInventarios&texto=$texto");
        }
		else
        {
            echo "\nError al Agregar los Datos:\n";
            print_r($insert_invs->errorInfo());
            echo "<br>El Codigo de Error es: ". $insert_invs->errorCode();
        }
	}
	catch(PDOException $e)
	{
		throw "ERROR AL INSERTAR DATOS ".$e->getMessage();		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL ELIMINAR DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL ELIMINAR DATOS 3 ".$s->getMessage();
        exit;
    }
}
elseif($_POST['SqlQuery']=='aumentar')
{
    try
	{
        $txt_codigo     = $_POST['CodeProd'];
		$SaldoAnte		= $_POST["SaldoProd"];
		$SaldoAume		= $_POST["AumentoSaldo"];
		$TotalExis		= $_POST["NuevoSaldo"];
		$texto			= "aumentar";
        
        $fecha_var = date('Y-m-d');
        $fecha_tot = date('Y-m-d H:i:s');
        $Referencia = "AUMENTO DE PRODUCTO";
        $NumFactura = 0;
        $salidaKard = 0;
        
        $sel_datprod = "SELECT * FROM productos WHERE cod_producto='$txt_codigo'";
        $sel_datprod = $DBcon->prepare($sel_datprod);
        $sel_datprod->execute();
        $ver_invs = $sel_datprod->fetch(PDO::FETCH_ASSOC);
        $PrecioCosto = $ver_invs['precio_costo'];
        $precioVenta = $ver_invs['precio_venta'];
        
        
        $insert_kard = "INSERT INTO kardex(fecha_var,fecha_tot,cod_producto,num_factura,
                        costo_prod_kardex,precio_vta_kardex,referencia_kardex,
                        entrada_kardex,salida_kardex,total,usuario) 
                        VALUES (:fecha_var,:fecha_tot,:cod_producto,:num_factura,
                        :costo_prod_kardex,:precio_vta_kardex,:referencia_kardex,
                        :entrada_kardex,:salida_kardex,:total,:usuario)";
        $insert_kard = $DBcon->prepare($insert_kard);
        $insert_kard->bindparam(":fecha_var", $fecha_var);
		$insert_kard->bindparam(":fecha_tot", $fecha_tot);
		$insert_kard->bindparam(":cod_producto", $txt_codigo);
		$insert_kard->bindparam(":num_factura", $NumFactura);
		$insert_kard->bindparam(":costo_prod_kardex", $PrecioCosto);
		$insert_kard->bindparam(":precio_vta_kardex", $precioVenta);
		$insert_kard->bindparam(":referencia_kardex", $Referencia);
		$insert_kard->bindparam(":entrada_kardex", $SaldoAume);
		$insert_kard->bindparam(":salida_kardex", $salidaKard);
		$insert_kard->bindparam(":total", $TotalExis);
		$insert_kard->bindparam(":usuario", $usuario);
            
            
		$update_saldo = "UPDATE productos 
                       SET 
                       existencia_prod=:existencia_prod 
                       WHERE cod_producto=:cod_producto";
		$update_saldo = $DBcon->prepare($update_saldo);
        $update_saldo->bindparam(":existencia_prod", $TotalExis);
		$update_saldo->bindparam(":cod_producto", $txt_codigo);
        
        
        
        
		if($update_saldo->execute())
        {
            $insert_kard->execute();
            header("location: index.php?&texto=$texto");
        }
        else
        {
            echo "\nError al Aumentar los Datos:\n";
            print_r($update_saldo->errorInfo());
            echo "<br>El Codigo de Error es: ". $update_saldo->errorCode();
        }
	}
	catch(PDOException $e)
	{
		throw "ERROR AL AUMENTAR LOS DATOS ".$e->getMessage();		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL AUMENTAR LOS DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL AUMENTAR LOS DATOS 3 ".$s->getMessage();
        exit;
    }
}
elseif($_POST['SqlQuery']=='disminuir')
{
    try
    {
        $txt_codigo     = $_POST['CodeProd'];
        $SaldoAnte      = $_POST["SaldoProd"];
        $SaldoAume      = $_POST["AumentoSaldo"];
        $TotalExis      = $_POST["NuevoSaldo"];
        $texto          = "disminuir";
        
        $fecha_var = date('Y-m-d');
        $fecha_tot = date('Y-m-d H:i:s');
        $Referencia = "DISMINUCION DE PRODUCTO";
        $NumFactura = 0;
        $EntradaKard = 0;
        
        $sel_datprod = "SELECT * FROM productos WHERE cod_producto='$txt_codigo'";
        $sel_datprod = $DBcon->prepare($sel_datprod);
        $sel_datprod->execute();
        $ver_invs = $sel_datprod->fetch(PDO::FETCH_ASSOC);
        $PrecioCosto = $ver_invs['precio_costo'];
        $precioVenta = $ver_invs['precio_venta'];
        
        
        $insert_kard = "INSERT INTO kardex(fecha_var,fecha_tot,cod_producto,num_factura,
                        costo_prod_kardex,precio_vta_kardex,referencia_kardex,
                        entrada_kardex,salida_kardex,total,usuario) 
                        VALUES (:fecha_var,:fecha_tot,:cod_producto,:num_factura,
                        :costo_prod_kardex,:precio_vta_kardex,:referencia_kardex,
                        :entrada_kardex,:salida_kardex,:total,:usuario)";
        $insert_kard = $DBcon->prepare($insert_kard);
        $insert_kard->bindparam(":fecha_var", $fecha_var);
        $insert_kard->bindparam(":fecha_tot", $fecha_tot);
        $insert_kard->bindparam(":cod_producto", $txt_codigo);
        $insert_kard->bindparam(":num_factura", $NumFactura);
        $insert_kard->bindparam(":costo_prod_kardex", $PrecioCosto);
        $insert_kard->bindparam(":precio_vta_kardex", $precioVenta);
        $insert_kard->bindparam(":referencia_kardex", $Referencia);
        $insert_kard->bindparam(":entrada_kardex", $EntradaKard);
        $insert_kard->bindparam(":salida_kardex", $SaldoAume);
        $insert_kard->bindparam(":total", $TotalExis);
        $insert_kard->bindparam(":usuario", $usuario);
            
            
        $update_saldo = "UPDATE productos 
                       SET 
                       existencia_prod=:existencia_prod 
                       WHERE cod_producto=:cod_producto";
        $update_saldo = $DBcon->prepare($update_saldo);
        $update_saldo->bindparam(":existencia_prod", $TotalExis);
        $update_saldo->bindparam(":cod_producto", $txt_codigo);
        
        
        
        
        if($update_saldo->execute())
        {
            $insert_kard->execute();
            header("location: index.php?&texto=$texto");
        }
        else
        {
            echo "\nError al Aumentar los Datos:\n";
            print_r($update_saldo->errorInfo());
            echo "<br>El Codigo de Error es: ". $update_saldo->errorCode();
        }
    }
    catch(PDOException $e)
    {
        throw "ERROR AL AUMENTAR LOS DATOS ".$e->getMessage();      
    }
    catch (Throwable $t)
    {
        echo "ERROR AL AUMENTAR LOS DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL AUMENTAR LOS DATOS 3 ".$s->getMessage();
        exit;
    }
}
elseif($_POST['SqlQuery']=='cerrar_inv')
{
    try
	{
        $txt_codigo     = $_POST['CodInv'];
        $estado_inv     = "FINALIZADO";
		$texto			= "cerrado";
        $fecha_cierre   = date('Y-m-d');
        
		$cerrrar_inv = "UPDATE alta_inventario 
                        SET 
                        estado_inv=:estado_inv,
                        fecha_inv_cierre=:fecha_inv_cierre 
                        WHERE cod_inventario=:cod_inventario";
		$cerrrar_inv = $DBcon->prepare($cerrrar_inv);
        $cerrrar_inv->bindParam(":estado_inv", $estado_inv);
        $cerrrar_inv->bindParam(":fecha_inv_cierre", $fecha_cierre);
        $cerrrar_inv->bindParam(":cod_inventario", $txt_codigo);
        $cerrrar_inv->execute();
        if($cerrrar_inv->execute())
        {
            header("location: ../../main.php?module=DetalleInventarios&texto=$texto");
        }
		else
        {
            echo "\nError al Cerrar el Inventario:\n";
            print_r($cerrrar_inv->errorInfo());
            echo "<br>El Codigo de Error es: ". $cerrrar_inv->errorCode();
        }
	}
	catch(PDOException $e)
	{
		echo "ERROR AL ELIMINAR DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL ELIMINAR DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL ELIMINAR DATOS 3 ".$s->getMessage();
        exit;
    }
}
elseif($_POST['SqlQuery']=='reabrir_inv')
{
    try
	{
        $txt_codigo     = $_POST['CodInv'];
        $estado_inv     = "EN PROCESO";
		$texto			= "Reabierto";
        $fecha_cierre   = null;
        
		$reabrir_inv = "UPDATE alta_inventario 
                        SET 
                        estado_inv=:estado_inv,
                        fecha_inv_cierre=:fecha_inv_cierre 
                        WHERE cod_inventario=:cod_inventario";
		$reabrir_inv = $DBcon->prepare($reabrir_inv);
        $reabrir_inv->bindParam(":estado_inv", $estado_inv);
        $reabrir_inv->bindParam(":fecha_inv_cierre", $fecha_cierre);
        $reabrir_inv->bindParam(":cod_inventario", $txt_codigo);
        $reabrir_inv->execute();
        if($reabrir_inv->execute())
        {
            header("location: ../../main.php?module=DetalleInventarios&texto=$texto");
        }
		else
        {
            echo "\nError al Reabrir Inventario:\n";
            print_r($reabrir_inv->errorInfo());
            echo "<br>El Codigo de Error es: ". $reabrir_inv->errorCode();
        }
	}
	catch(PDOException $e)
	{
		echo "ERROR AL ELIMINAR DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL ELIMINAR DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL ELIMINAR DATOS 3 ".$s->getMessage();
        exit;
    }
}
?>