<?php
//include 'login/validar.php';

include '../../conectar.php';

$sel_inv = "SELECT * FROM alta_inventario WHERE estado_inv='EN PROCESO'";
$eje_inv = $DBcon->prepare($sel_inv);
$eje_inv->execute();
$con_inv = $eje_inv->rowCount();

if($con_inv>0){

//include 'AumentarTomaModal.php';
?>
<div class="panel panel-success panel-mb-6">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-folder-o icon-title"></i> Toma de Inventario General
            <button class="close" arial-label="close" id="btnCerrarToma">
                <span aria-hidden="true">&times;</span>
            </button>
        </h3>
    </div>
    <div class="panel-body">
        <section class="content-header">
            <div class="row">
                <div class="col-lg-6">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Busqueda por Nombre de Producto o por Nombre de Laboratorio" id="BusquedaTxt" name="BusquedaTxt" style="text-transform: uppercase;">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
                        </span>
                    </div><!-- /input-group -->
                </div><!-- /.col-lg-6 -->
                <div class="col-lg-6">
                    <span class="pull-right" data-toggle="tooltip" title="Nuevo">
                        <button type="button" class="btn btn-success btn-social" data-toggle="modal" data-target="#AumentarTomaModal"><i class="fa fa-plus-square" aria-hidden="true"></i> Nuevo</button>
                    </span>
                </div>
            </div><!-- /.row -->
        </section>

        <?php  
        $texto = "";
        if(isset($_GET["texto"]))
        {
            $texto = $_GET["texto"];
        } 
        
        if($texto=="agregar"){ ?>
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Producto Aumentado con Exito</strong></div>
        <?php }elseif($texto=="editar"){ ?>

        <div class="alert alert-info" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Producto Disminuido con Exito</strong></div>
        <?php }elseif($texto=="eliminar"){ ?>

        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Producto Eliminado con Exito</strong></div>
        <?php } ?>

        <div style="height:10px;"></div>

        <section class="content">
            <div class="box box-primary">
                <div class="box-body">
                    <div id="MostrarDatosTomaGen"></div><!-- Carga los datos ajax -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content-->

    </div>
</div>
<?php
}
else
{
?>
<script>
    alert('No Hay Inventario en Proceso, Debe de Abrir un Nuevo Inventario');
    //windows.location.href('main.php?module=start');
</script>

<?php
}
?>
<script src="styles/querys_modulos/InvQuerys.js"></script>
<script src="modulos/inventario/QuerysInv.js"></script>