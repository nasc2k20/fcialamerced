<?php
include 'AgregarLabModal.php';
?>
   <div class="panel panel-success panel-mb-6">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-folder-o icon-title"></i> Listado de Laboratorios
            <button class="close" arial-label="close" id="btnCerrarLab">
                <span aria-hidden="true">&times;</span>
            </button>
        </h3>
    </div>
    <div class="panel-body">
        <section class="content-header">
            <div class="row">
                <span class="pull-right" data-toggle="tooltip" title="Nuevo">
                    <button type="button" class="btn btn-success btn-social" data-toggle="modal" data-target="#AgregarLabModal"><i class="fa fa-plus-square" aria-hidden="true"></i> Nuevo</button>
                </span>
            </div>
        </section>

        <div style="height:10px;"></div>

        <section class="content">
            <div class="box box-primary">
                <div class="box-body">
                    <div id="MostrarDatosFab"></div>
                </div>
            </div>
        </section>
    </div>
</div>
<script src="styles/querys_modulos/LabQuerys.js"></script>
<!--<script src="modulos/laboratorios/QuerysLab.js"></script>-->