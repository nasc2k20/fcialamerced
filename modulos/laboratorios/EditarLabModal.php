<!-- Modal -->
<div class="modal fade" id="EditarLabModal<?php echo $ver_lab['cod_lab']; ?>" tabindex="-1" role="dialog" aria-labelledby="EditarLabModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="EditarLabModalLabel">Editar Laboratorio</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label" for="nombre_<?php echo $ver_lab['cod_lab']; ?>">Nombre Laboratorio:</label>
                    <input type="text" class="form-control" id="nombre_<?php echo $ver_lab['cod_lab']; ?>" style="text-transform:uppercase" value="<?php echo $ver_lab['nombre_lab']; ?>" />
                </div>

                <div class="form-group">
                    <label class="control-label" for="descuento_<?php echo $ver_lab['cod_lab']; ?>">Descuento que Aplica:</label>
                    <input type="text" class="form-control" id="descuento_<?php echo $ver_lab['cod_lab']; ?>" style="text-transform:uppercase" value="<?php echo $ver_lab['descuento_aplicar']; ?>" />

                </div>

                <div class="form-group">
                    <label class="control-label" for="retira_<?php echo $ver_lab['cod_lab']; ?>">Dias Retira Vence:</label>
                    <input type="number" class="form-control" id="retira_<?php echo $ver_lab['cod_lab']; ?>" style="text-transform:uppercase" value="<?php echo $ver_lab['dias_vencimiento']; ?>" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="EditarLab('<?php echo $ver_lab['cod_lab']; ?>');"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>
