<?php
require_once '../../conectar.php';
include_once '../../login/Paginator.php';

$ContarNum = 1;
$QueryPag = "";
$per_page = intval($_REQUEST['NumxPag']);

$QueryPag   = "SELECT count(*) AS numrows
                FROM laboratorios";

$eje_lab = $DBcon->prepare($QueryPag);
$eje_lab->execute();
$dat = $eje_lab->fetch(PDO::FETCH_ASSOC);
$numrows = $dat['numrows'];

$page = (isset($_REQUEST['NumPag']) && !empty($_REQUEST['NumPag']))?$_REQUEST['NumPag']:1;

$adjacents  = 4;
$offset = ($page - 1) * $per_page;

$total_pages = ceil($numrows/$per_page);

$sel_lab = "SELECT * FROM laboratorios 
            ORDER BY nombre_lab ASC 
            LIMIT $offset, $per_page";


$eje_lab = $DBcon->prepare($sel_lab);
$eje_lab->execute();
?>
<link rel="stylesheet" href="styles/css/datatables.min.css">
<script src="styles/js/datatables.min.js"></script>
<table id="LaboratorioTable" class="table table-striped table-hover">
    <thead>
        <tr>
            <th class="center">No.</th>
            <th class="center">Nombre</th>
            <th class="center">Dias Retira Vence</th>
            <th class="center">Desc. Aplica</th>
            <th class="center" colspan="2" width="10%">Acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php
        $finales=0;
        while ($ver_lab = $eje_lab->fetch(PDO::FETCH_ASSOC)) {
       ?>
        <tr>
            <td width='10%' class='center'>
                <?php echo $ContarNum; ?>
            </td>
            <td width='55%'>
                <?php echo $ver_lab["nombre_lab"]; ?>
            </td>
            <td class='center' width='15%'>
                <?php echo $ver_lab["dias_vencimiento"]; ?>
            </td>
            <td class='center' width='10%' align='center'>
                <?php echo $ver_lab["descuento_aplicar"]; ?>%</td>
            <td class='center' width='5%' data-toggle="tooltip" data-placement="top" title="Modificar">
                <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#EditarLabModal<?php echo $ver_lab['cod_lab']; ?>"><i class="fa fa-pencil-square" aria-hidden="true"></i> </button>
            </td>
            <td class='center' width='5%' data-toggle="tooltip" data-placement="top" title="Eliminar">
                <button class="btn btn-danger btn-sm" onclick="EliminarLab('<?php echo $ver_lab['cod_lab']; ?>','<?php echo $ver_lab['nombre_lab']; ?>')"><i class="fa fa-trash" aria-hidden="true" title="Eliminar"></i></button>
            </td>
        </tr>
        <?php
            include 'EditarLabModal.php';
            
            $finales++;
          $ContarNum++;
        }
        ?>
    </tbody>
    <tfoot>
            <tr>
                <td colspan="7">
                    <?php 
                            $inicios=$offset+1;
                            $finales+=$inicios -1;
                            echo "Mostrando $inicios al $finales de $numrows registros";
                            echo paginate( $page, $total_pages, $adjacents);
                        ?>
                </td>
            </tr>
        </tfoot>
</table>
