<?php
require '../../conectar.php';

if($_POST['SqlQuery']=='agregar')
{
    try
	{
		$txt_nombre		= strtoupper($_POST["NombreLab"]);
		$txt_desc		= $_POST["Descuento"];
		$txt_vence		= $_POST["DiasRetira"];
		$texto			= "agregar";
	
		$insert_lab = "INSERT INTO laboratorios (nombre_lab, descuento_aplicar,dias_vencimiento) 
						VALUES (:nombre_lab,:descuento_aplicar,:dias_vencimiento)";
		$insert_lab = $DBcon->prepare($insert_lab);
		$insert_lab->bindparam(":nombre_lab", $txt_nombre);
		$insert_lab->bindparam(":descuento_aplicar", $txt_desc);
		$insert_lab->bindparam(":dias_vencimiento", $txt_vence);
        
        if($insert_lab->execute())
        {
            header("location: index.php?&texto=$texto");
        }
		else
        {
            echo "\nError al Agregar los Datos:\n";
            print_r($insert_lab->errorInfo());
            echo "<br>El Codigo de Error es: ". $insert_lab->errorCode();
        }
	}
	catch(PDOException $e)
	{
		echo "ERROR AL AGREGAR DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL AGREGAR DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL AGREGAR DATOS 3 ".$s->getMessage();
        exit;
    }
}
elseif($_POST['SqlQuery']=='actualizar')
{
    try
	{
        $txt_codigo     = $_POST['CodeLab'];
		$txt_nombre		= strtoupper($_POST["NombreLab"]);
		$txt_desc		= $_POST["Descuento"];
		$txt_vence		= $_POST["DiasRetira"];
		$texto			= "editar";
	
		$update_lab = "UPDATE laboratorios 
                       SET 
                       nombre_lab=:nombre_lab,
                       descuento_aplicar=:descuento_aplicar,
                       dias_vencimiento=:dias_vencimiento 
                       WHERE cod_lab=:cod_lab";
		$update_lab = $DBcon->prepare($update_lab);
        $update_lab->bindparam(":nombre_lab", $txt_nombre);
		$update_lab->bindparam(":descuento_aplicar", $txt_desc);
		$update_lab->bindparam(":dias_vencimiento", $txt_vence);
        $update_lab->bindParam(":cod_lab", $txt_codigo);
        
        if($update_lab->execute())
        {
            header("location: index.php?&texto=$texto");
        }
		else
        {
            echo "\nError al Actualizar los Datos:\n";
            print_r($update_lab->errorInfo());
            echo "<br>El Codigo de Error es: ". $update_lab->errorCode();
        }
	}
	catch(PDOException $e)
	{
		echo "ERROR AL ACTUALIZAR LOS DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL ACTUALIZAR LOS DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL ACTUALIZAR LOS DATOS 3 ".$s->getMessage();
        exit;
    }
}
elseif($_POST['SqlQuery']=='eliminar')
{
    
    try
	{
        $txt_codigo     = $_POST['EliminarID'];
		$texto			= "eliminar";
		$delete_lab = "DELETE FROM laboratorios WHERE cod_lab=:cod_lab";
		$delete_lab = $DBcon->prepare($delete_lab);
        $delete_lab->bindParam(":cod_lab", $txt_codigo);
                
        if($delete_lab->execute())
        {
            header("location: index.php?&texto=$texto");
        }
		else
        {
            //echo '<script> alert("Error al Eliminar los Datos: '.$DBcon->errorInfo().' El Codigo de Error es: '.$DBcon->errorCode().' ")</script>';
            echo "\nError al Eliminar los Datos:\n";
            print_r($delete_lab->errorInfo());
            echo "<br>El Codigo de Error es: ". $delete_lab->errorCode();
        }
        
	}
	catch(PDOException $e)
	{
		echo "ERROR AL ELIMINAR DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL ELIMINAR DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL ELIMINAR DATOS 3 ".$s->getMessage();
        exit;
    }
}
else
{
    
}
?>