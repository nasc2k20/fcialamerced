<!-- Modal -->
<div class="modal fade" id="AgregarLabModal" tabindex="-1" role="dialog" aria-labelledby="AgregarLabModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="AgregarLabModalLabel">Nuevo Laboratorio</h4>
            </div>
            <div class="modal-body">
                <form method="post" name="form1" id="form1" class="form-horizontal" action="">
                    <div class="form-group">
                        <div class="col-md-8">
                            <label class="control-label" for="nombre">Nombre Laboratorio:</label>
                            <input type="text" class="form-control" id="nombre" name="nombre" style="text-transform:uppercase" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="control-label" for="descuento">Descuento que Aplica:</label>
                            <input type="text" class="form-control" id="descuento" name="descuento" style="text-transform:uppercase" required />
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6">
                            <label class="control-label" for="retira">Dias Retira Vence:</label>
                            <input type="number" class="form-control" id="retira" name="retira" style="text-transform:uppercase" required />
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="AgregarLab();"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>
