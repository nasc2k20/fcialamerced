<div class="panel panel-success panel-mb-6">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-folder-o icon-title"></i> Kardex de Productos
            <button class="close" arial-label="close" id="btnCerrarKardex">
                <span aria-hidden="true">&times;</span>
            </button>
        </h3>
    </div>
    <div class="panel-body">
        <section class="content-header">
            <div class="row">
                <div class="col-lg-6">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Busqueda por Nombre de Producto o por Nombre de Laboratorio" id="BusquedaTxt" name="BusquedaTxt" style="text-transform: uppercase;">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="fa fa-search" aria-hidden="true"></i> Buscar</button>
                        </span>
                    </div>
                </div>
                <div class="col-lg-6">
                    <span class="pull-right" data-toggle="tooltip" title="Nuevo">
                        <button type="button" class="btn btn-success btn-social" data-toggle="modal" data-target="#AgregarProdModal"><i class="fa fa-plus-square" aria-hidden="true"></i> Nuevo</button>
                    </span>
                </div>
            </div>
        </section>

        <div style="height:10px;"></div>

        <section class="content">
            <div class="box box-primary">
                <div class="box-body">
                    <div id="MostrarDatosKardex"></div>
                </div>
            </div>
        </section>
    </div>
</div>
<script src="styles/querys_modulos/KardexQuerys.js"></script>
<!--<script src="modulos/kardex/QuerysKardex.js"></script>-->
