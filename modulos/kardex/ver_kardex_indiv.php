<?php
require_once '../../conectar.php';

$cod_prod = $_POST["CodigoProd"];

$sel_prod = "SELECT * FROM productos
			WHERE cod_producto='$cod_prod'";
$eje_prod = $DBcon->prepare($sel_prod);
$eje_prod->execute();
$ver_prod= $eje_prod->fetch(PDO::FETCH_ASSOC);
$nombre_comp = $ver_prod["nombre_producto"];



$sel_lab = "SELECT * FROM productos a 
			INNER JOIN kardex b ON a.cod_producto=b.cod_producto 
			WHERE a.cod_producto='$cod_prod' 
            ORDER BY b.fecha_tot DESC
            LIMIT 0,10";
$eje_lab = $DBcon->prepare($sel_lab);
$eje_lab->execute();
$con_kar = $eje_lab->rowCount();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>

<body>
    <table class="table table-bordered table-hover table-striped table-sm" id="tabla_ting">
        <thead>
           <tr>
               <th colspan="6"><h3><strong><center><?php echo $nombre_comp; ?></center></strong></h3></th>
           </tr>
            <tr>
                <th width="10%">Corr</th>
                <th width="25%">Fecha</th>
                <th width="35%">Evento</th>
                <th width="10%">Entrada</th>
                <th width="10%">Salida</th>
                <th width="10%">Saldo</th>
            </tr>
        </thead>

        <tbody>
            <?php
                                    $contar_corr = 1;

                                    if ($con_kar>0)
                                    {
                                    while($ver_lab = $eje_lab->fetch(PDO::FETCH_ASSOC))
                                    {
                                    ?>
                <tr>
                    <td>
                        <?php echo $contar_corr; ?>
                    </td>
                    <td>
                        <?php $FechaGen = new datetime($ver_lab["fecha_tot"]); echo $FechaGen->format('d-m-Y H:i:s'); ?>
                    </td>
                    <td>
                        <?php echo $ver_lab["referencia_kardex"]; ?>
                    </td>
                    <td>
                        <?php echo $ver_lab["entrada_kardex"]; ?>
                    </td>
                    <td>
                        <?php echo $ver_lab["salida_kardex"]; ?>
                    </td>
                    <td width="10%">
                        <?php echo $ver_lab["total"]; ?>
                    </td>
                </tr>
                <?php
                                     $contar_corr++;
                                    }
                                    }
                                    else
                                    {
                                    ?>
                    <tr>
                        <td colspan="5">No Hay Informacion que Mostrar.</td>
                    </tr>
                    <?php
                                    }
                                    ?>
        </tbody>
    </table>
</body>

</html>
