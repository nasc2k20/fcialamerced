<?php
require_once '../../conectar.php';

include_once '../../login/Paginator.php';

$BusquedaProd = $_REQUEST['BusquedaProd'];

$ContarNum = 1;
$QueryPag = "";
$per_page = intval($_REQUEST['NumxPag']);

if($BusquedaProd!='')
{
    $QueryPag   = "SELECT count(*) as numrows 
                    FROM productos a 
                    INNER JOIN laboratorios b ON a.cod_lab=b.cod_lab 
                    WHERE b.nombre_lab LIKE '%".$BusquedaProd."%' 
                    OR a.nombre_producto LIKE '%".$BusquedaProd."%' 
                    ORDER BY a.nombre_producto ASC";
}
else
{
    $QueryPag   = "SELECT count(*) AS numrows
                    FROM productos";
}

$eje_prod = $DBcon->prepare($QueryPag);
$eje_prod->execute();
$dat = $eje_prod->fetch(PDO::FETCH_ASSOC);
$numrows = $dat['numrows'];


$page = (isset($_REQUEST['NumPag']) && !empty($_REQUEST['NumPag']))?$_REQUEST['NumPag']:1;

$adjacents  = 4; //gap between pages after number of adjacents
$offset = ($page - 1) * $per_page;


$total_pages = ceil($numrows/$per_page);






if($BusquedaProd!='')
{
    $sel_prod = "SELECT * FROM productos a 
                INNER JOIN laboratorios b ON a.cod_lab=b.cod_lab 
                WHERE b.nombre_lab LIKE '%".$BusquedaProd."%' 
                OR a.nombre_producto LIKE '%".$BusquedaProd."%' 
                ORDER BY a.nombre_producto ASC 
                LIMIT $offset, $per_page";
}
else
{
    $sel_prod = "SELECT * FROM productos a 
                INNER JOIN laboratorios b ON a.cod_lab=b.cod_lab ORDER BY a.nombre_producto ASC  
                LIMIT $offset, $per_page";
}


$eje_prod = $DBcon->prepare($sel_prod);
$eje_prod->execute();

include 'VerKardexModal.php';
?>
        <table id="dataTables1" class="table table-bordered table-striped table-hover">
        <thead>
          <tr>
            <th class="center">No.</th>
            <th class="center">Nombre de Producto</th>
            <th class="center">Saldo</th>
            <th class="center">Laboratorio</th>
            <th class="center" colspan="2" width="10%">Acciones</th>
          </tr>
        </thead>
        <tbody>
        <?php
        $finales=0;
        while ($ver_prod = $eje_prod->fetch(PDO::FETCH_ASSOC)) {
       ?>
          <tr>
                  <td width='5%' class='center'><?php echo $ContarNum; ?></td>
                  <td width='35%'><?php echo $ver_prod["nombre_producto"]; ?></td>
                  <td class='center' width='10%'><?php echo $ver_prod["existencia_prod"]; ?></td>
                  <td class='center' width='35%' align='right'><?php echo $ver_prod["nombre_lab"]; ?></td>
                  <td class='center' width='5%'>
                      <?php
                        echo ($ver_prod['estado_prod'])=='s' ? '<i class="fa fa-check-circle" aria-hidden="true"></i>' : '<i class="fa fa-minus-circle" aria-hidden="true"></i>';
                      ?>
                  </td>
                  <td class='center' width='5%' data-toggle='tooltip' data-placement='top' title='Ver Kardex'>
                     <button class="btn btn-default btn-sm" data-toggle="modal" data-target="#ListarKardexProd" onclick="ConsultarKardex('<?php echo $ver_prod['cod_producto']; ?>');"><i class="fa fa-th-list" aria-hidden="true"></i> </button>
                  </td>
                </tr>
        <?php
        $finales++;
        $ContarNum++;
        }
        ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="7">
                    <?php 
                            $inicios=$offset+1;
                            $finales+=$inicios -1;
                            echo "Mostrando $inicios al $finales de $numrows registros";
                            echo paginate( $page, $total_pages, $adjacents);
                        ?>
                </td>
            </tr>
        </tfoot>
      </table>
