function ConsultarKardex(CodigoProd) {
    $.ajax({
        url: "modulos/kardex/ver_kardex_indiv.php",
        method: "POST",
        data: {
            CodigoProd: CodigoProd
        },
        beforeSend: function () {
            $("#ListadoKardex").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
        },
        success: function (data) {
            $('#ListadoKardex').html(data);
        }
    });
}
