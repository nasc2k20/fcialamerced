function AgregarUsu() {
    var Usuario = $('#Usuario').val();
    var Contrasena = $('#Contrasena').val();
    var NombreUsu = $('#NombreUsu').val();
    var NivelUsu = $('#NivelUsu').val();
    var SqlQuery = "insertar";

    var Parametros = {
        "Usuario": Usuario,
        "Contrasena": Contrasena,
        "NombreUsu": NombreUsu,
        "NivelUsu": NivelUsu,
        "SqlQuery": SqlQuery
    };

    if (Usuario == '') {
        alert('Debe de Digitar el Usuario');
        $('#Usuario').focus();
    } else if (Contrasena == '') {
        alert('Debe de Digitar la Contrase&ntilde;a');
        $('#Contrasena').focus();
    } else if (NombreUsu == '') {
        alert('Debe de Digitar el Nombre Completo del Usuario');
        $('#NombreUsu').focus();
    } else if (NivelUsu == '') {
        alert('Debe de Elegir el Nivel del Usuario');
        $('#NivelUsu').focus();
    } else {
        $.ajax({
            type: "POST",
            url: "modulos/usuarios/ProcesosSQL.php",
            data: Parametros,
            beforeSend: function (data) {
                $("#MostrarDatosUsu").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
            },
            success: function (datos) {
                $('#AgregarUsuModal').modal('hide');
                if ($('.modal-backdrop').is(':visible')) {
                    $('.modal-backdrop').remove();
                };
                $('#MostrarDatosUsu').html(datos);
            }
        });
    }
}




function EditarUsu(LoginUsuario) {
    
    var Contrasena = $('#Contrasena_'+LoginUsuario).val();
    var NombreUsu = $('#NombreUsu_'+LoginUsuario).val();
    var NivelUsu = $('#NivelUsu_'+LoginUsuario).val();
    var SqlQuery = "actualizar";
    
    var Parametros = {
        "Usuario": LoginUsuario,
        "Contrasena": Contrasena,
        "NombreUsu": NombreUsu,
        "NivelUsu": NivelUsu,
        "SqlQuery": SqlQuery
    };

    if (Usuario == '') {
        alert('Debe de Digitar el Usuario');
        $('#Usuario').focus();
    } else if (Contrasena == '') {
        alert('Debe de Digitar la Contrase&ntilde;a');
        $('#Contrasena').focus();
    } else if (NombreUsu == '') {
        alert('Debe de Digitar el Nombre Completo del Usuario');
        $('#NombreUsu').focus();
    } else if (NivelUsu == '') {
        alert('Debe de Elegir el Nivel del Usuario');
        $('#NivelUsu').focus();
    } else {
        $.ajax({
            type: "POST",
            url: "modulos/usuarios/ProcesosSQL.php",
            data: Parametros,
            beforeSend: function (data) {
                $("#MostrarDatosUsu").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
            },
            success: function (datos) {
                $('#EditarUsuModal'+LoginUsuario).modal('hide');
                if ($('.modal-backdrop').is(':visible')) {
                    $('.modal-backdrop').remove();
                };
                $('#MostrarDatosUsu').html(datos);
            }
        });
    }
}







function EliminarUsu(CodigoUsu, NombreUsu) {
    var SqlQuery = 'eliminar';
    var Parametros = {
        "SqlQuery": SqlQuery,
        "EliminarID": CodigoUsu
    };

    $.confirm({
        icon: 'fa fa-smile-o',
        title: 'Eliminaci&oacute;n',
        theme: 'modern',
        closeIcon: true,
        content: 'Desea Eliminar El Usuario: <br> <strong><h3>' + NombreUsu + '</h3></strong>',
        animation: 'scale',
        icon: 'fa fa-warning',
        type: 'orange',
        buttons: {
            ok: {
                text: 'Confirmar',
                btnClass: 'btn-blue',
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: "modulos/usuarios/ProcesosSQL.php",
                        data: Parametros,
                        beforeSend: function (objeto) {
                            $("#MostrarDatosUsu").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
                        },
                        success: function (datos) {
                            $("#MostrarDatosUsu").html(datos);
                        }
                    });
                },
            },
            cancel: {
                text: 'Cancelar',
                btnClass: 'btn-red',
                action: function () {}
            }
        }
    });
}