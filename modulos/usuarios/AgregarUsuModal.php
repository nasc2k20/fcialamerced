<div class="modal fade" tabindex="-1" role="dialog" id="AgregarUsuModal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Nuevo Usuario</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="Usuario">Usuario: </label>
                    <input type="text" class="form-control" id="Usuario" placeholder="Digite Usuario">
                    <div id="ValidarUsu"></div>
                </div>
                <div class="form-group">
                    <label for="Contrasena">Contrase&ntilde;a: </label>
                    <input type="password" class="form-control" id="Contrasena" placeholder="Digite Contrase&ntilde;a">
                </div>
                <div class="form-group">
                    <label for="NombreUsu">Nombre Completo: </label>
                    <input type="text" class="form-control" id="NombreUsu" placeholder="Digite Nombre Completo de Usuario" style="text-transform: uppercase;">
                </div>
                <div class="form-group">
                    <label for="NivelUsu">Nivel:</label>
                       <?php
                        include('../../conectar.php');
                        $sel_niv = "SELECT * FROM nivel 
                                    WHERE valor_nivel>0";
                        $eje_niv = $DBcon->prepare($sel_niv);
                        $eje_niv->execute();
                        ?>
                        <select id="NivelUsu" class="form-control">
                           <?php while($ver_niv = $eje_niv->fetch(PDO::FETCH_ASSOC)){ ?>
                            <option value="<?php echo $ver_niv['cod_nivel']; ?>"><?php echo $ver_niv['nombre_nivel']; ?></option>
                            <?php } ?>
                        </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar</button>
                <button type="button" class="btn btn-primary" id="GuardarUsu" onclick="AgregarUsu();"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->