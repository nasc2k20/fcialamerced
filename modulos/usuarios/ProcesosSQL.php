<?php
require '../../conectar.php';

if($_POST['SqlQuery']=='insertar')
{
    try
	{
		$txt_usu		= strtolower($_POST["Usuario"]);
		//$txt_pas		= md5($_POST["Contrasena"]);
		$txt_pas		= base64_encode($_POST["Contrasena"]);
		$txt_nom		= strtoupper($_POST["NombreUsu"]);
        $txt_act        = 'activo';
        $txt_niv        = $_POST["NivelUsu"];
		$texto			= "agregar";
	
		$insert_usu = "INSERT INTO usuarios (login_usuario, login_password,nombre_completo,cod_nivel,estado_usu) 
						VALUES (:login_usuario,:login_password,:nombre_completo,:cod_nivel,:estado_usu)";
		$insert_usu = $DBcon->prepare($insert_usu);
		$insert_usu->bindparam(":login_usuario", $txt_usu);
		$insert_usu->bindparam(":login_password", $txt_pas);
		$insert_usu->bindparam(":nombre_completo", $txt_nom);
		$insert_usu->bindparam(":estado_usu", $txt_act);
		$insert_usu->bindparam(":cod_nivel", $txt_niv);
		
		
	    if($insert_usu->execute())
        {
            header("location: index.php?texto=$texto");
        }
		else
        {
            //echo '<script> alert("Error al Eliminar los Datos: '.$DBcon->errorInfo().' El Codigo de Error es: '.$DBcon->errorCode().' ")</script>';
            echo "\nError al Agregar los Datos:\n";
            print_r($insert_usu->errorInfo());
            echo "<br>El Codigo de Error es: ". $insert_usu->errorCode();
        }
        
	}
	catch(PDOException $e)
	{
		echo "ERROR AL INSERTAR LOS DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL INSERTAR LOS DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL INSERTAR LOS DATOS 3 ".$s->getMessage();
        exit;
    }
}
elseif($_POST['SqlQuery']=='actualizar')
{
    try
	{
        $txt_usu		= strtolower($_POST["Usuario"]);
		$txt_pas		= base64_encode($_POST["Contrasena"]);
		$txt_nom		= strtoupper($_POST["NombreUsu"]);
        $txt_act        = 'activo';
        $txt_niv        = $_POST["NivelUsu"];
		$texto			= "editar";
	
		$update_usu = "UPDATE usuarios 
                       SET 
                       login_password=:login_password,
                       nombre_completo=:nombre_completo,
                       cod_nivel=:cod_nivel, 
                       estado_usu=:estado_usu 
                       WHERE login_usuario='$txt_usu'";
		$update_usu = $DBcon->prepare($update_usu);
        //$update_usu->execute(array(":login_usuario"=> $codigo_prod));
        $update_usu->bindparam(":login_password", $txt_pas);
		$update_usu->bindparam(":nombre_completo", $txt_nom);
		$update_usu->bindparam(":cod_nivel", $txt_niv);
		$update_usu->bindparam(":estado_usu", $txt_act);
		
		
	    if($update_usu->execute())
        {
            header("location: index.php?texto=$texto");
        }
		else
        {
            echo "\nError al Agregar los Datos:\n";
            print_r($update_usu->errorInfo());
            echo "<br>El Codigo de Error es: ". $update_usu->errorCode();
        }
        
	}
	catch(PDOException $e)
	{
		echo "ERROR AL MODIFICAR LOS DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL MODIFICAR LOS DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL MODIFICAR LOS DATOS 3 ".$s->getMessage();
        exit;
    }
}
elseif($_POST['SqlQuery']=='eliminar')
{
    
    try
	{
        $txt_codigo     = $_POST['EliminarID'];
		$texto			= "eliminar";
        
		$delete_usu = "DELETE FROM usuarios WHERE login_usuario=:login_usuario";
		$delete_usu = $DBcon->prepare($delete_usu);
        $delete_usu->bindParam(":login_usuario", $txt_codigo);
        
        if($delete_usu->execute())
        {
            header("location: index.php?texto=$texto");
        }
		else
        {
            //echo '<script> alert("Error al Eliminar los Datos: '.$DBcon->errorInfo().' El Codigo de Error es: '.$DBcon->errorCode().' ")</script>';
            echo "\nError al Eliminar los Datos:\n";
            print_r($delete_usu->errorInfo());
            echo "<br>El Codigo de Error es: ". $delete_usu->errorCode();
        }
	}
	catch(PDOException $e)
	{
		echo "ERROR AL ELIMINAR DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL ELIMINAR DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL ELIMINAR DATOS 3 ".$s->getMessage();
        exit;
    }
}
else
{
    
}
?>