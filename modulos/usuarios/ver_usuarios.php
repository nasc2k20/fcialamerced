<?php

$respuesta = $_REQUEST['action'];

require_once '../../conectar.php';
$no = 1;


if ($respuesta=='showAll')
{
    $sel_usu = "SELECT * FROM usuarios a 
                INNER JOIN nivel b ON b.cod_nivel=a.cod_nivel 
                WHERE b.valor_nivel>0 
                ORDER BY a.nombre_completo ASC";
}
else
{
    //$sel_usu = "SELECT * FROM usuarios WHERE cod_nivel = $respuesta ORDER BY nombre_completo ASC";
    $sel_usu = "SELECT * FROM usuarios a 
                INNER JOIN nivel b ON b.cod_nivel=a.cod_nivel 
                WHERE b.valor_nivel>0  AND a.cod_nivel=$respuesta 
                ORDER BY a.nombre_completo ASC";
}


$eje_usu = $DBcon->prepare($sel_usu);
$eje_usu->execute();
?>
        <table id="dataTables1" class="table table-bordered table-striped table-hover table-sm">
        <thead>
          <tr>
            <th class="center">No.</th>
            <th class="center">Nombre Completo</th>
            <th class="center">Usuario</th>
            <th class="center" colspan="3" width="10%">Acciones</th>
          </tr>
        </thead>
        <tbody>
        <?php
        while ($ver_usu = $eje_usu->fetch(PDO::FETCH_ASSOC)) {
       ?>
          <tr>
                  <td width='10%' class='center'><?php echo $no; ?></td>
                  <td width='60%'><?php echo $ver_usu["nombre_completo"]; ?></td>
                  <td class='center' width='15%'><?php echo $ver_usu["login_usuario"]; ?></td>
                  <td class='center' width='5%'>
                      <?php
                        echo ($ver_usu['estado_usu'])=='activo' ? '<i class="fa fa-check-circle" aria-hidden="true"></i>' : '<i class="fa fa-minus-circle" aria-hidden="true"></i>';
                      ?>
                  </td>
                  <td class='center' width='5%'>
                     <span data-toggle='tooltip' data-placement='top' title='Modificar'>
                        <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#EditarUsuModal<?php echo $ver_usu['login_usuario']; ?>"><i class="fa fa-pencil-square" aria-hidden="true"></i> </button>
                    </span>
                  </td>
                  <td class='center' width='5%'>
                     <span data-toggle="tooltip" data-placement="top" title="Eliminar">
                        <button class="btn btn-danger btn-sm" onclick="EliminarUsu('<?php echo $ver_usu['login_usuario']; ?>','<?php echo $ver_usu['nombre_completo']; ?>')"><i class="fa fa-trash" aria-hidden="true" title="Eliminar"></i></button>
                    </span>
                  </td>
                </tr>
        <?php
            
            include 'EditarUsuModal.php';
            
          $no++;
        }
        ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="6">
                    <?php
                     /*
                     $eje_lab = $DBcon->prepare($sel_lab);
                     $eje_lab->execute();
                     $con_lab = $eje_lab->rowCount(); 
                     $total_pages = ceil($con_lab/$record_per_page);  
                     for($i=1; $i<=$total_pages; $i++)  
                     {  
                          $output .= "<span class='pagination_link' style='cursor:pointer; padding:6px; border:1px solid #ccc;' id='".$i."'>".$i."</span>";  
                     }
                     echo $output;
                     */
                    ?>
                </td>
            </tr>
        </tfoot>
      </table>
