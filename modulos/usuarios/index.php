<?php
include 'AgregarUsuModal.php';
?>
   <div class="panel panel-success panel-mb-6">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-folder-o icon-title"></i> Listado de Usuarios
            <button class="close" arial-label="close" id="btnCerrarUsu">
                <span aria-hidden="true">&times;</span>
            </button>
        </h3>
    </div>
    <div class="panel-body">
        <section class="content-header">
            <div class="row">
               <div class="col-lg-4">
                   <label for="nivel" class="control-label">Nivel:</label>
                <?php
                include('../../conectar.php');
                $sel_niv = "SELECT * FROM nivel 
                        WHERE valor_nivel>0";
                $eje_niv = $DBcon->prepare($sel_niv);
                $eje_niv->execute();
                ?>
                <select name="nivel" id="nivel" class="form-control">
                  <option value="showAll" selected>Mostrar Todos</option>
                   <?php
                    while($ver_niv = $eje_niv->fetch(PDO::FETCH_ASSOC)){
                    ?>
                    <option value="<?php echo $ver_niv['cod_nivel']; ?>"><?php echo $ver_niv['nombre_nivel']; ?></option>
                    <?php
                    }
                    ?>
                </select>
               </div>
                <span class="pull-right" data-toggle="tooltip" title="Nuevo">
                    <button type="button" class="btn btn-success btn-social" data-toggle="modal" data-target="#AgregarUsuModal"><i class="fa fa-plus-square" aria-hidden="true"></i> Nuevo</button>
                </span>
            </div>
            <!-- /.row -->
        </section>

        <?php  
        $texto = "";
        if(isset($_GET["texto"]))
        {
            $texto = $_GET["texto"];
        } 
        
        if($texto=="agregar"){ ?>
        <div class="alert alert-success" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Usuario Agregado con Exito</strong></div>
        <?php }elseif($texto=="editar"){ ?>

        <div class="alert alert-info" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Usuario Modificado con Exito</strong></div>
        <?php }elseif($texto=="eliminar"){ ?>

        <div class="alert alert-danger" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><strong>Usuario Eliminado con Exito</strong></div>
        <?php } ?>

        <div style="height:10px;"></div>

        <section class="content">
            <div class="box box-primary">
                <div class="box-body">
                    <div id="MostrarDatosUsu"></div><!-- Carga los datos ajax -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content-->

    </div>
</div>
<!---->
<script src="styles/querys_modulos/UsusQuerys.js"></script>
<script src="modulos/usuarios/QuerysUsu.js"></script>