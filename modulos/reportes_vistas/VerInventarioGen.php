<div class="panel panel-success panel-mb-6">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-folder-o icon-title"></i> Listado de Inventario General
            <button class="close" arial-label="close" id="btnCerrarTIG">
                <span aria-hidden="true">&times;</span>
            </button>
        </h3>
    </div>
    <div class="panel-body">
        <section class="content-header">
            <div class="row">
                <center></center>
                <span data-toggle="tooltip" title="Reporte Toma Inventario General" class="pull-right">
                    <button type="button" id="BtnTomaInvGen" class="btn btn-primary btn-social"><i class="fa fa-print" aria-hidden="true"></i> Imprimir Reporte</button>
                </span>

            </div>
        </section>

        <div style="height:10px;"></div>

        <section class="content">
            <div class="box box-primary">
                <div class="box-body">
                    <div id="editor"></div>
                    <div id="MostrarDatosTomaInvGen"></div>
                </div>
            </div>
        </section>
    </div>
</div>
<script src="modulos/reportes_querys/QueryTomaInvGen.js"></script>