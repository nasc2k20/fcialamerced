   <div class="panel panel-success panel-mb-6">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-folder-o icon-title"></i> Listado de Inventario Por Laboratorio
            <button class="close" arial-label="close" id="btnCerrarTIG">
                <span aria-hidden="true">&times;</span>
            </button>
        </h3>
    </div>
    <div class="panel-body">
        <section class="content-header">
            <div class="row">
                   <span data-toggle="tooltip" title="Reporte Toma Inventario General" class="pull-left">
                        <div class="col-lg-12">
                           <label for="nivel" class="control-label">Seleccione Laboratorio:</label>
                        <?php
                        include('../../conectar.php');
                        $sel_lab = "SELECT * FROM laboratorios ORDER BY nombre_lab ASC";
                        $eje_lab = $DBcon->prepare($sel_lab);
                        $eje_lab->execute();
                        ?>
                        <select name="CodLab" id="CodLab" class="form-control">
                          <option value="showAll" selected>Mostrar Todos</option>
                           <?php
                            while($ver_lab = $eje_lab->fetch(PDO::FETCH_ASSOC)){
                            ?>
                            <option value="<?php echo $ver_lab['cod_lab']; ?>"><?php echo $ver_lab['nombre_lab']; ?></option>
                            <?php
                            }
                            ?>
                        </select>
                       </div>
                    </span>
                    <div style="height: 10px;"></div>
                    <span data-toggle="tooltip" title="Reporte Toma Inventario General" class="pull-right">
                        <button type="button" id="BtnTomaInvLab" class="btn btn-primary btn-social" ><i class="fa fa-print" aria-hidden="true"></i> Imprimir Reporte</button>
                    </span>
            </div>
            <!-- /.row -->
        </section>

        <div style="height:10px;"></div>

        <section class="content">
            <div class="box box-primary">
                <div class="box-body">
                   <div id="editor"></div>
                    <div id="MostrarDatosTomaInvLab"></div><!-- Carga los datos ajax -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content-->

    </div>
</div>
<script src="modulos/reportes_querys/QueryTomaInvLab.js"></script>
<!--
<script src="styles/querys_modulos/LabQuerys.js"></script>