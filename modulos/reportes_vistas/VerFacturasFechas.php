<script>
    function FormatoFechaIni() {
	cadena = document.getElementById('FechaInicioTxt').value;
	if(cadena.length == 2) { 
		document.getElementById('FechaInicioTxt').value += "-";
	} else if (cadena.length == 5) {
		document.getElementById('FechaInicioTxt').value += "-";
	}
    }
    
    function FormatoFechaFin() {
	cadena = document.getElementById('FechaFinTxt').value;
	if(cadena.length == 2) { 
		document.getElementById('FechaFinTxt').value += "-";
	} else if (cadena.length == 5) {
		document.getElementById('FechaFinTxt').value += "-";
	}
    }
</script>


<div class="panel panel-success panel-mb-6">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-folder-o icon-title"></i> Listado de Facturas por Fecha
            <button class="close" arial-label="close" id="btnCerrarFactVta">
                <span aria-hidden="true">&times;</span>
            </button>
        </h3>
    </div>
    <div class="panel-body">
        <section class="content-header">
            <div class="row">
                <div class="col-xs-3">
                    <label for="" class="label-control">Fecha Inicio:</label>
                    <input type="text" class="form-control" id="FechaInicioTxt" placeholder="00-00-0000" maxlength="10" onkeypress="FormatoFechaIni();">
                </div>
                <div class="col-xs-3">
                    <label for="" class="label-control">Fecha Fin:</label>
                    <input type="text" class="form-control" id="FechaFinTxt" placeholder="00-00-0000" maxlenght="10" onkeypress="FormatoFechaFin();">
                </div>
            </div>

            <div style="height:10px;"></div>

            <div class="row">
                <div class="col-xs-6">
                    <button class="btn btn-success btn-sm" type="button" id="btnFactFecha">Generar Datos</button>
                </div>
                <div class="col-xs-6 text-right">
                    <button class="btn btn-primary btn-sm" type="button" id="btnFactImprimir"> <i class="fa fa-print" aria-hidden="true"></i></button>
                </div>
            </div>
        </section>

        <div style="height:10px;"></div>

        <section class="content">
            <div class="box box-primary">
                <div class="box-body">
                    <div id="MostrarFacturasFecha"></div>
                </div>
            </div>
        </section>

    </div>
</div>
<script src="modulos/reportes_querys/QuerysFacturaFechas.js"></script>
