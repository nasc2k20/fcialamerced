<div class="panel panel-success panel-mb-6">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-folder-o icon-title"></i> Listado de Inventario General DNM
            <button class="close" arial-label="close" id="btnCerrarTIGDNM">
                <span aria-hidden="true">&times;</span>
            </button>
        </h3>
    </div>
    <div class="panel-body">

        <section class="content">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col">
                                <h3>Acontinuacion debera de configurar el reporte a Generar.</h3>
                            </div>
                        </div>
                        <div class="row">
                            <table class="table table-bordered table-sm">
                                <thead>
                                    <tr>
                                        <th>Opcion</th>
                                        <th>Seleccionar</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Mostrar Precios</td>
                                        <td>
                                            <label class="checkbox-inline">
                                                <input type="checkbox" id="PrecioCb">.
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mostrar Costos</td>
                                        <td>
                                            <label class="checkbox-inline">
                                                <input type="checkbox" id="CostoCb">.
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mostrar Existencias</td>
                                        <td>
                                            <label class="checkbox-inline">
                                                <input type="checkbox" id="SaldoCb">.
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Mostrar Laboratorio</td>
                                        <td>
                                            <label class="checkbox-inline">
                                                <input type="checkbox" id="LaboratorioCb">.
                                            </label>
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2"><button type="button" id="BtnGenerarInvDnm" class="btn btn-primary btn-sm">Generar Reporte</button></td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    </body>
                </div>
            </div>
        </section>
    </div>
</div>

<script src="modulos/reportes_querys/QuerysTomaInvDnm.js"></script>