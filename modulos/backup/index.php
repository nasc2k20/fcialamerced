<?php
//include 'AgregarLabModal.php';
?>
<div class="panel panel-success panel-mb-6">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-folder-o icon-title"></i> Crear Copia de Seguridad
            <button class="close" arial-label="close" id="btnCerrarBackup">
                <span aria-hidden="true">&times;</span>
            </button>
        </h3>
    </div>
    <div class="panel-body">

        <div style="height:10px;"></div>

        <section class="content">
            <div class="box box-primary">
                <div class="box-body">
                    <div id="MostrarDatosBackup">
                        <button type="button" class="btn btn-success btn-social" id="BtnGenerarBackup"><i class="fa fa-plus-square"
                                aria-hidden="true"></i>
                            Generar Nueva Copia de Seguridad</button>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<script src="styles/querys_modulos/BackupQuerys.js"></script>