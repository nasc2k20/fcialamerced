$(function () {

    

    $('#btnFactFecha').click(function(){
        MostrarFactImpDatos();
    });

    $('#btnFactImprimir').click(function () {
        var FechaInicioTxt = $('#FechaInicioTxt').val();
        var FechaFinTxt = $('#FechaFinTxt').val();
        VentanaCentrada('modulos/reportes/Rep_FacturaFecha.php?FechaInicioTxt='+FechaInicioTxt+'&FechaFinTxt='+FechaFinTxt, 'Reporte de Facturas por Fecha', '', '960', '450', 'true');
        //$('#TblRepFactFecha').empty();        
    });

    $('#FechaInicioTxt').datepicker({
        "format": "dd-mm-yyyy",
        "endDate": "0d",
        "keyboardNavigation": false
    });

    $('#FechaFinTxt').datepicker({
        "format": "dd-mm-yyyy",
        "endDate": "0d",
        "keyboardNavigation": true
    });

})



function MostrarFactImpDatos() {

    var FechaInicioTxt = $('#FechaInicioTxt').val();
    var FechaFinTxt = $('#FechaFinTxt').val();

    var Parametros = {
        "FechaInicioTxt": FechaInicioTxt,
        "FechaFinTxt": FechaFinTxt
    };

    if (FechaInicioTxt == '') {
        alert('Debe de Ingresar una Fecha de Inicio');
        $('#FechaInicioTxt').focus();
    } else if (FechaFinTxt == '') {
        alert('Debe de Ingresar una Fecha de Fin');
        $('#FechaFinTxt').focus();
    } else {
        $.ajax({
            type: 'POST',
            url: 'modulos/reportes/Rep_FacturaFecha.php',
            data: Parametros,
            cache: false,
            beforeSend: function () {
                $("#MostrarFacturasFecha").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
            },
            success: function (r) {
                $("#MostrarFacturasFecha").html(r);
                //$('#ReporteFechasIF').attr('src', r);
            }
        });
    }
}

function VentanaCentrada(theURL, winName, features, myWidth, myHeight, isCenter) { //v3.0
    if (window.screen)
        if (isCenter)
            if (isCenter == "true") {
                var myLeft = (screen.width - myWidth) / 2;
                var myTop = (screen.height - myHeight) / 2;
                features += (features != '') ? ',' : '';
                features += ',left=' + myLeft + ',top=' + myTop;
            }
    window.open(theURL, winName, features + ((features != '') ? ',' : '') + 'width=' + myWidth + ',height=' + myHeight);
}