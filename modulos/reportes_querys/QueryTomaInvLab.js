$(function () {
    getAll();


    $('#BtnTomaInvLab').click(function () {
        window.print();
    });

})


function getAll() {
    $.ajax({
        url: 'modulos/reportes/Rep_TomaInvLab.php',
        data: 'action=showAll',
        cache: false,
        beforeSend: function () {
            $("#MostrarDatosTomaInvLab").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
        },
        success: function (r) {
            $("#MostrarDatosTomaInvLab").html(r);
        }
    });
}



$("#CodLab").change(function () {
    var id = $(this).find(":selected").val();
    var dataString = 'action=' + id;
    $.ajax({
        url: 'modulos/reportes/Rep_TomaInvLab.php',
        data: dataString,
        cache: false,
        beforeSend: function () {
            $("#MostrarDatosTomaInvLab").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
        },
        success: function (r) {
            $("#MostrarDatosTomaInvLab").html(r);
        }
    });
});
