$(function () {
    MostrarTomaInvDatos();

    $('#BtnTomaInvGen').click(function () {
        VentanaCentrada('modulos/reportes/Rep_TomaInvGen.php', 'Reporte de Inventario General', '', '960', '450', 'true');
        //$('#MostrarDatosTomaInvGen').fadeIn('slow');
        $('#RepTomaInvGen').empty();
    });

});


function MostrarTomaInvDatos() {

    $.ajax({
        type: "POST",
        url: "modulos/reportes/Rep_TomaInvGen.php",
        beforeSend: function (objeto) {
            $("#MostrarDatosTomaInvGen").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
        },
        success: function (datos) {
            $('#MostrarDatosTomaInvGen').html(datos);
        }
    });
}

function VentanaCentrada(theURL, winName, features, myWidth, myHeight, isCenter) { //v3.0
    if (window.screen)
        if (isCenter)
            if (isCenter == "true") {
                var myLeft = (screen.width - myWidth) / 2;
                var myTop = (screen.height - myHeight) / 2;
                features += (features != '') ? ',' : '';
                features += ',left=' + myLeft + ',top=' + myTop;
            }
    window.open(theURL, winName, features + ((features != '') ? ',' : '') + 'width=' + myWidth + ',height=' + myHeight);
}
