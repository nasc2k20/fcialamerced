$(function () {

    $('#BtnGenerarInvDnm').click(function () {
        var PrecioCb = ((document.getElementById('PrecioCb').checked) ? 1 : 0);
        var CostoCb = ((document.getElementById('CostoCb').checked) ? 1 : 0);
        var SaldoCb = ((document.getElementById('SaldoCb').checked) ? 1 : 0);
        var LaboratorioCb = ((document.getElementById('LaboratorioCb').checked) ? 1 : 0);

        VentanaCentrada('modulos/reportes/Rep_TomaInvDnm.php?PrecioCb='+PrecioCb+'&CostoCb='+CostoCb+'&SaldoCb='+SaldoCb+'&LaboratorioCb='+LaboratorioCb, 'Reporte de Inventario General DNM', '', '960', '450', 'true');
        //$('#RepTomaInvGen').empty();
    });

});

function VentanaCentrada(theURL, winName, features, myWidth, myHeight, isCenter) { //v3.0
    if (window.screen)
        if (isCenter)
            if (isCenter == "true") {
                var myLeft = (screen.width - myWidth) / 2;
                var myTop = (screen.height - myHeight) / 2;
                features += (features != '') ? ',' : '';
                features += ',left=' + myLeft + ',top=' + myTop;
            }
    window.open(theURL, winName, features + ((features != '') ? ',' : '') + 'width=' + myWidth + ',height=' + myHeight);
}
