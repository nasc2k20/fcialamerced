<?php
require_once '../../conectar.php';
$no = 1;

$cod_actividad = $_POST['CodActividad'];
$sel_actividad = "SELECT * FROM desechos WHERE cod_desecho='$cod_actividad'";
$eje_actividad = $DBcon->prepare($sel_actividad);
$eje_actividad->execute();
?>

<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th style='text-align:center;'>Pisos</th>
            <th style='text-align:center;'>Ventanas</th>
            <th style='text-align:center;'>Vitrinas</th>
            <th style='text-align:center;'>Estantes</th>
            <th style='text-align:center;'>Paredes</th>
        </tr>
    </thead>
    <tbody>
        <?php
        while ($ver_actividad = $eje_actividad->fetch(PDO::FETCH_ASSOC)) {
       ?>
        <tr>
            <td style='text-align:center;' width='20%'>
                <?php echo ($ver_actividad["desecho_pisos"])==1 ? '<i class="fa fa-check-circle-o fa-3x text-success" aria-hidden="true" ></i>' : '<i class="fa fa-times-circle-o fa-3x text-danger" aria-hidden="true"></i>' ; ?>
            </td>
            <td style='text-align:center;' width='20%'>
                <?php echo ($ver_actividad["desecho_ventanas"])==1 ? '<i class="fa fa-check-circle-o fa-3x text-success" aria-hidden="true"></i>' : '<i class="fa fa-times-circle-o fa-3x text-danger" aria-hidden="true"></i>' ; ?>
            </td>
            <td style='text-align:center;' width='20%' align='left'>
                <?php echo ($ver_actividad["desecho_vitrinas"])==1 ? '<i class="fa fa-check-circle-o fa-3x text-success" aria-hidden="true"></i>' : '<i class="fa fa-times-circle-o fa-3x text-danger" aria-hidden="true"></i>' ; ?>
            </td>
            <td style='text-align:center;' width='20%'>
                <?php echo ($ver_actividad["desecho_estantes"])==1 ? '<i class="fa fa-check-circle-o fa-3x text-success" aria-hidden="true"></i>' : '<i class="fa fa-times-circle-o fa-3x text-danger" aria-hidden="true"></i>' ; ?>
            </td>
            <td style='text-align:center;' width='20%'>
                <?php echo ($ver_actividad["desecho_paredes"])==1 ? '<i class="fa fa-check-circle-o fa-3x text-success" aria-hidden="true"></i>' : '<i class="fa fa-times-circle-o fa-3x text-danger" aria-hidden="true"></i>' ; ?>
            </td>
        </tr>
        <?php
          $no++;
        }
        ?>
    </tbody>
</table>
