<?php
require_once '../../conectar.php';
$no = 1;
if (isset($_POST['query']))
{
    $sql_busqueda = $_POST['query'];
    $sel_des = "SELECT * FROM desechos WHERE fecha_desecho LIKE '%".$sql_busqueda."%' ORDER BY date_desecho DESC";
}
else
{
    $sel_des = "SELECT * FROM desechos ORDER BY date_desecho DESC";
}


$eje_des = $DBcon->prepare($sel_des);
$eje_des->execute();
?>

<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th class="center">No.</th>
            <th class="center">Fecha</th>
            <th class="center">Descripcion</th>
            <th class="center">Usuario</th>
            <th class="center" colspan="3" width="10%">Acciones</th>
        </tr>
    </thead>
    <tbody>
        <?php
        while ($ver_des = $eje_des->fetch(PDO::FETCH_ASSOC)) {
       ?>
        <tr>
            <td width='10%' class='center'>
                <?php echo $no; ?>
            </td>
            <td width='15%'>
                <?php $fecha_F = new datetime($ver_des["fecha_desecho"]); $FechaDes = $fecha_F->format('d-m-Y'); echo $FechaDes; ?>
            </td>
            <td class='center' width='50%'>
                <?php echo $ver_des["descripcion_actividad"]; ?>
            </td>
            <td class='center' width='10%' align='left'>
                <?php echo $ver_des["usuario"]; ?></td>
            <td class='center' width='5%' data-toggle="tooltip" data-placement="top" title="Listado">
                <button class="btn btn-default btn-sm" data-toggle="modal" data-target="#VerActividadModal" onclick="CargarActividad('<?php echo $ver_des['cod_desecho']; ?>')"><i class="fa fa-list-ol" aria-hidden="true"></i> </button>
            </td>
            <td class='center' width='5%' data-toggle="tooltip" data-placement="top" title="Editar">
                <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#EditarDesModal<?php echo $ver_des['cod_desecho']; ?>"><i class="fa fa-pencil-square-o" aria-hidden="true" title="Editar"></i></button>
            </td>
            <td class='center' width='5%' data-toggle="tooltip" data-placement="top" title="Eliminar">
                <button class="btn btn-danger btn-sm" onclick="EliminarDesecho('<?php echo $ver_des['cod_desecho']; ?>');"><i class="fa fa-trash" aria-hidden="true" title="Editar"></i></button>
            </td>
        </tr>
        <?php
            include 'EditarDesModal.php';
            include 'VerActividadModal.php';
            
            
          $no++;
        }
        ?>
    </tbody>
</table>
