<?php
include 'AgregarDesModal.php';
?>
<div class="panel panel-success panel-mb-6">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-folder-o icon-title"></i> Mantenimiento de Farmacia
            <button class="close" arial-label="close" id="btnCerrarDes">
                <span aria-hidden="true">&times;</span>
            </button>
        </h3>
    </div>
    <div class="panel-body">
        <section class="content-header">
            <div class="row">
                <span class="pull-right" data-toggle="tooltip" title="Nuevo">
                    <button type="button" class="btn btn-success btn-social" data-toggle="modal" data-target="#AgregarDesModal"><i class="fa fa-plus-square" aria-hidden="true"></i> Nuevo</button>
                </span>
            </div>
            <!-- /.row -->
        </section>

        <div style="height:10px;"></div>

        <section class="content">
            <div class="box box-primary">
                <div class="box-body">
                    <div id="MostrarDatosDese"></div><!-- Carga los datos ajax -->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content-->

    </div>
</div>
<!---->
<script src="styles/querys_modulos/DeseQuerys.js"></script>
<script src="modulos/desechos/QuerysDes.js"></script>
