<?php
date_default_timezone_set("America/El_Salvador");
?>
<!-- Modal -->
<div class="modal fade" id="EditarDesModal<?php echo $ver_des['cod_desecho']; ?>" tabindex="-1" role="dialog" aria-labelledby="EditarDesModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="EditarDesModalLabel">Modificar Mantenimiento</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-xs-12">
                        <label class="control-label" for="FechaDesecho">Fecha:</label>
                        <input type="text" class="form-control" id="FechaDesecho" readonly value="<?php 
                                                                                                  $FechaF = new datetime($ver_des['date_desecho']);
                                                                                                  $FechaAct = $FechaF->format("d-m-Y H:i:s");
                                                                                                  echo $FechaAct; ?>" />
                    </div>

                </div>
                
                <div class="row">
                    <div class="form-group col-xs-2">
                        <label for="" class="control-label">Pisos</label>
                        <input type="checkbox" id="PisosChk_<?php echo $ver_des['cod_desecho']; ?>" class="form-control" <?php echo ($ver_des['desecho_pisos']==1) ? 'checked' : '';?>>
                    </div>
                    <div class="form-group col-xs-2">
                        <label for="" class="control-label">Ventanas</label>
                        <input type="checkbox" id="VentanasChk_<?php echo $ver_des['cod_desecho']; ?>" class="form-control" <?php echo ($ver_des['desecho_ventanas']==1) ? 'checked' : '';?>>
                    </div>
                    <div class="form-group col-xs-2">
                        <label for="" class="control-label">Vitrinas</label>
                        <input type="checkbox" id="VitrinasChk_<?php echo $ver_des['cod_desecho']; ?>" class="form-control" <?php echo ($ver_des['desecho_vitrinas']==1) ? 'checked' : '';?>>
                    </div>
                    <div class="form-group col-xs-2">
                        <label for="" class="control-label">Estantes</label>
                        <input type="checkbox" id="EstantesChk_<?php echo $ver_des['cod_desecho']; ?>" class="form-control" <?php echo ($ver_des['desecho_estantes']==1) ? 'checked' : '';?>>
                    </div>
                    <div class="form-group col-xs-2">
                        <label for="" class="control-label">Paredes</label>
                        <input type="checkbox" id="ParedesChk_<?php echo $ver_des['cod_desecho']; ?>" class="form-control" <?php echo ($ver_des['desecho_paredes']==1) ? 'checked' : '';?>>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="DescripcionTxt">Descripcion de Actividad:</label>
                    <textarea id="DescripcionTxt_<?php echo $ver_des['cod_desecho']; ?>" cols="20" rows="5" class="form-control" style="text-transform:uppercase; resize:none;"><?php echo $ver_des['descripcion_actividad']; ?></textarea>
                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="EditarDesecho('<?php echo $ver_des['cod_desecho']; ?>');"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>
