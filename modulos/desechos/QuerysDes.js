function AgregarDesecho() {
    var FechaDesecho = $('#FechaDesecho').val();
    var HoraDesecho = $('#HoraDesecho').val();
    /*
    var PisosChk = $('#PisosChk').val();
    var VentanasChk = $('#VentanasChk').val();
    var VitrinasChk = $('#VitrinasChk').val();
    var EstantesChk = $('#EstantesChk').val();
    var ParedesChk = $('#ParedesChk').val();
    */

    var PisosChk = ((document.getElementById('PisosChk').checked) ? 1 : 0);
    var VentanasChk = ((document.getElementById('VentanasChk').checked) ? 1 : 0);
    var VitrinasChk = ((document.getElementById('VitrinasChk').checked) ? 1 : 0);
    var EstantesChk = ((document.getElementById('EstantesChk').checked) ? 1 : 0);
    var ParedesChk = ((document.getElementById('ParedesChk').checked) ? 1 : 0);


    var DescripcionTxt = $('#DescripcionTxt').val();
    var SqlQuery = 'agregar';

    var Parametros = {
        "SqlQuery": SqlQuery,
        "FechaDesecho": FechaDesecho,
        "HoraDesecho": HoraDesecho,
        "PisosChk": PisosChk,
        "VentanasChk": VentanasChk,
        "VitrinasChk": VitrinasChk,
        "EstantesChk": EstantesChk,
        "ParedesChk": ParedesChk,
        "DescripcionTxt": DescripcionTxt
    };

    if (PisosChk == 0 && VentanasChk == 0 && VitrinasChk == 0 && EstantesChk == 0 && ParedesChk == 0) {
        alert('Seleccione al Menos una Actividad');
    } else if (DescripcionTxt == '') {
        alert('Debe de Digitar la Descripcion de la Actividad');
        $('#DescripcionTxt').focus();
    } else {
        $.ajax({
            type: "POST",
            url: "modulos/desechos/ProcesosSQL.php",
            data: Parametros,
            beforeSend: function (objeto) {
                $("#MostrarDatosDese").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
            },
            success: function (datos) {
                $('#AgregarDesModal').modal('hide');

                if ($('.modal-backdrop').is(':visible')) {
                    //$('body').removeClass('modal-open'); 
                    $('.modal-backdrop').remove();

                };

                $('#AgregarDesModal').find("input,textarea,select,checkbox").val('').end();
                $('#AgregarDesModal').find("checkbox", false);
                /*
                $('#AgregarDesModal').on('hidden', function(){
                    $(this).removeData("modal");
                })
                
                $('body').on('hidden.bs.modal', '.modal', function () {
                    $(this).removeData('bs.modal');
                  });
                  
                  $('#my-form').find('input:radio, input:checkbox').prop('checked', false);
                */


                $("#MostrarDatosDese").html(datos);
            }
        });
    }
}


function EditarDesecho(CodigoDesecho) {
    var PisosChk = ((document.getElementById('PisosChk_' + CodigoDesecho).checked) ? 1 : 0);
    var VentanasChk = ((document.getElementById('VentanasChk_' + CodigoDesecho).checked) ? 1 : 0);
    var VitrinasChk = ((document.getElementById('VitrinasChk_' + CodigoDesecho).checked) ? 1 : 0);
    var EstantesChk = ((document.getElementById('EstantesChk_' + CodigoDesecho).checked) ? 1 : 0);
    var ParedesChk = ((document.getElementById('ParedesChk_' + CodigoDesecho).checked) ? 1 : 0);


    var DescripcionTxt = $('#DescripcionTxt_' + CodigoDesecho).val();
    var SqlQuery = 'actualizar';

    var Parametros = {
        "SqlQuery": SqlQuery,
        "PisosChk": PisosChk,
        "VentanasChk": VentanasChk,
        "VitrinasChk": VitrinasChk,
        "EstantesChk": EstantesChk,
        "ParedesChk": ParedesChk,
        "DescripcionTxt": DescripcionTxt,
        "CodDesecho": CodigoDesecho
    };


    if (PisosChk == 0 && VentanasChk == 0 && VitrinasChk == 0 && EstantesChk == 0 && ParedesChk == 0) {
        alert('Seleccione al Menos una Actividad');
    } else if (DescripcionTxt == '') {
        alert('Debe de Digitar la Descripcion de la Actividad');
        $('#DescripcionTxt_'+CodigoDesecho).focus();
    } else {
        $.ajax({
            type: "POST",
            url: "modulos/desechos/ProcesosSQL.php",
            data: Parametros,
            beforeSend: function (objeto) {
                $("#MostrarDatosDese").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
            },
            success: function (datos) {
                $('#EditarDesModal'+CodigoDesecho).modal('hide');

                if ($('.modal-backdrop').is(':visible')) {
                    //$('body').removeClass('modal-open'); 
                    $('.modal-backdrop').remove();

                };

                $("#MostrarDatosDese").html(datos);
            }
        });
    }

}

function EliminarDesecho(CodigoDesecho) {
    var SqlQuery = 'eliminar';
    var Parametros = {
        "SqlQuery": SqlQuery,
        "EliminarID": CodigoDesecho
    };

    $.confirm({
        icon: 'fa fa-smile-o',
        title: 'Eliminaci&oacute;n',
        theme: 'modern',
        closeIcon: true,
        content: '¿Desea Eliminar La Actividad realizada?</h3></strong>',
        animation: 'scale',
        icon: 'fa fa-warning',
        type: 'orange',
        buttons: {
            ok: {
                text: 'Confirmar',
                btnClass: 'btn-blue',
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: "modulos/desechos/ProcesosSQL.php",
                        data: Parametros,
                        beforeSend: function (objeto) {
                            $("#MostrarDatosDese").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
                        },
                        success: function (datos) {
                            $("#MostrarDatosDese").html(datos);
                        }
                    });
                },
            },
            cancel: {
                text: 'Cancelar',
                btnClass: 'btn-red',
                action: function () {}
            }
        }
    });
}
