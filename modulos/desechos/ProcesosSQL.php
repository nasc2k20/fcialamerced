<?php
include '../../login/validar.php';
require '../../conectar.php';

$usuario	= $_SESSION["usuario"];

if($_POST['SqlQuery']=='agregar')
{
    try
	{
		$SqlQuery     = $_POST['SqlQuery'];
        $FechaDes_f   = new datetime($_POST['FechaDesecho']);
        $HoraDesecho  = $_POST['HoraDesecho'];
        $PisosChk     = $_POST['PisosChk'];
        $VentanasChk  = $_POST['VentanasChk'];
        $VitrinasChk  = $_POST['VitrinasChk'];
        $EstantesChk  = $_POST['EstantesChk'];
        $ParedesChk   = $_POST['ParedesChk'];
        $DescripcionTxt= strtoupper($_POST['DescripcionTxt']);
        $FechaDesecho = $FechaDes_f->format('Y-m-d');
        
        $FechaHora = $FechaDesecho.' '.$HoraDesecho;
	
		$insert_des = "INSERT INTO desechos (fecha_desecho, date_desecho, desecho_pisos, desecho_ventanas, 
                      desecho_vitrinas, desecho_estantes, desecho_paredes, descripcion_actividad, usuario) 
                      VALUES (:fecha_desecho, :date_desecho, :desecho_pisos, :desecho_ventanas, 
                      :desecho_vitrinas, :desecho_estantes, :desecho_paredes, :descripcion_actividad, :usuario)";
		$insert_des = $DBcon->prepare($insert_des);
		$insert_des->bindparam(":fecha_desecho", $FechaDesecho);
		$insert_des->bindparam(":date_desecho", $FechaHora);
		$insert_des->bindparam(":desecho_pisos", $PisosChk);
		$insert_des->bindparam(":desecho_ventanas", $VentanasChk);
		$insert_des->bindparam(":desecho_vitrinas", $VitrinasChk);
		$insert_des->bindparam(":desecho_estantes", $EstantesChk);
		$insert_des->bindparam(":desecho_paredes", $ParedesChk);
		$insert_des->bindparam(":descripcion_actividad", $DescripcionTxt);
		$insert_des->bindparam(":usuario", $usuario);
        
        if($insert_des->execute())
        {
            header("location: index.php?&texto=$texto");
        }
		else
        {
            echo "\nError al Agregar los Datos:\n";
            print_r($insert_des->errorInfo());
            echo "<br>El Codigo de Error es: ". $insert_des->errorCode();
        }
	}
	catch(PDOException $e)
	{
		echo "ERROR AL AGREGAR DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL AGREGAR DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL AGREGAR DATOS 3 ".$s->getMessage();
        exit;
    }
}
elseif($_POST['SqlQuery']=='actualizar')
{
    try
	{
        $CodDesecho     = $_POST['CodDesecho'];
        $PisosChk     = $_POST['PisosChk'];
        $VentanasChk  = $_POST['VentanasChk'];
        $VitrinasChk  = $_POST['VitrinasChk'];
        $EstantesChk  = $_POST['EstantesChk'];
        $ParedesChk   = $_POST['ParedesChk'];
        $DescripcionTxt= strtoupper($_POST['DescripcionTxt']);
        
        $update_des = "UPDATE desechos 
                      SET 
                      desecho_pisos = '$PisosChk', 
                      desecho_ventanas = '$VentanasChk', 
                      desecho_vitrinas = '$VitrinasChk', 
                      desecho_estantes = '$EstantesChk', 
                      desecho_paredes = '$ParedesChk', 
                      descripcion_actividad = '$DescripcionTxt' 
                      WHERE cod_desecho = '$CodDesecho'";
		$update_des = $DBcon->prepare($update_des);
		$update_des->bindparam(":desecho_pisos", $PisosChk);
		$update_des->bindparam(":desecho_ventanas", $VentanasChk);
		$update_des->bindparam(":desecho_vitrinas", $VitrinasChk);
		$update_des->bindparam(":desecho_estantes", $EstantesChk);
		$update_des->bindparam(":desecho_paredes", $ParedesChk);
		$update_des->bindparam(":descripcion_actividad", $DescripcionTxt);
        
        if($update_des->execute())
        {
            header("location: index.php?&texto=$texto");
        }
		else
        {
            echo "\nError al Actualizar los Datos:\n";
            print_r($update_des->errorInfo());
            echo "<br>El Codigo de Error es: ". $update_des->errorCode();
        }
	}
	catch(PDOException $e)
	{
		echo "ERROR AL ACTUALIZAR LOS DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL ACTUALIZAR LOS DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL ACTUALIZAR LOS DATOS 3 ".$s->getMessage();
        exit;
    }
}
elseif($_POST['SqlQuery']=='eliminar')
{
    
    try
	{
        $txt_codigo     = $_POST['EliminarID'];

		$delete_des = "DELETE FROM desechos WHERE cod_desecho=:cod_desecho";
		$delete_des = $DBcon->prepare($delete_des);
        $delete_des->bindParam(":cod_desecho", $txt_codigo);
                
        if($delete_des->execute())
        {
            header("location: index.php");
        }
		else
        {
            //echo '<script> alert("Error al Eliminar los Datos: '.$DBcon->errorInfo().' El Codigo de Error es: '.$DBcon->errorCode().' ")</script>';
            echo "\nError al Eliminar los Datos:\n";
            print_r($delete_des->errorInfo());
            echo "<br>El Codigo de Error es: ". $delete_des->errorCode();
        }
        
	}
	catch(PDOException $e)
	{
		echo "ERROR AL ELIMINAR DATOS ".$e->getMessage(); 
        exit;		
	}
    catch (Throwable $t)
    {
        echo "ERROR AL ELIMINAR DATOS 2 ".$t->getMessage();
        exit;
    }
    catch (Exception $s)
    {
        echo "ERROR AL ELIMINAR DATOS 3 ".$s->getMessage();
        exit;
    }
}
else
{
    
}
?>