<?php
date_default_timezone_set("America/El_Salvador");
?>

<div class="modal fade" id="VerActividadModal" tabindex="-1" role="dialog" aria-labelledby="VerActividadModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="VerActividadModalLabel">Detalle de Actividad</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="MostrarActividadDiv"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i> Close</button>
            </div>
        </div>
    </div>
</div>
