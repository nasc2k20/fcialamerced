<?php
date_default_timezone_set("America/El_Salvador");
?>
<!-- Modal -->
<div class="modal fade" id="AgregarDesModal" tabindex="-1" role="dialog" aria-labelledby="AgregarDesModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="AgregarDesModalLabel">Nuevo Mantenimiento</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-xs-5">
                        <label class="control-label" for="FechaDesecho">Fecha:</label>
                        <input type="text" class="form-control" id="FechaDesecho" readonly value="<?php echo date('d-m-Y'); ?>" />
                    </div>

                    <div class="form-group col-xs-5">
                        <label class="control-label" for="HoraDesecho">Hora:</label>
                        <input type="text" class="form-control" id="HoraDesecho" readonly value="<?php echo date('H:i:s'); ?>" />
                    </div>

                </div>
                
                <div class="row">
                    <div class="form-group col-xs-2">
                        <label for="" class="control-label">Pisos</label>
                        <input type="checkbox" id="PisosChk" class="form-control">
                    </div>
                    <div class="form-group col-xs-2">
                        <label for="" class="control-label">Ventanas</label>
                        <input type="checkbox" id="VentanasChk" class="form-control">
                    </div>
                    <div class="form-group col-xs-2">
                        <label for="" class="control-label">Vitrinas</label>
                        <input type="checkbox" id="VitrinasChk" class="form-control">
                    </div>
                    <div class="form-group col-xs-2">
                        <label for="" class="control-label">Estantes</label>
                        <input type="checkbox" id="EstantesChk" class="form-control">
                    </div>
                    <div class="form-group col-xs-2">
                        <label for="" class="control-label">Paredes</label>
                        <input type="checkbox" id="ParedesChk" class="form-control">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label" for="DescripcionTxt">Descripcion de Actividad:</label>
                    <textarea id="DescripcionTxt" cols="20" rows="5" class="form-control" style="text-transform:uppercase; resize:none;"></textarea>
                    
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" data-dismiss="modal"><i class="fa fa-times-circle" aria-hidden="true"></i> Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="AgregarDesecho();"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
            </div>
        </div>
    </div>
</div>
