<?php
if (isset($_POST["Button1"]))
{
	$usu = $_POST["TextBox1"];
	//$pas = md5($_POST["TextBox2"]);
	$pas = base64_encode($_POST["TextBox2"]);
	$est = "activo";
	
	require_once '../conectar.php';
	
	$sel_usu = "SELECT * FROM usuarios a 
                INNER JOIN nivel b ON b.cod_nivel=a.cod_nivel 
				WHERE a.login_usuario=:usuario AND a.login_password=:passwords 
				AND a.estado_usu=:estado";
	$eje_usu = $DBcon->prepare($sel_usu);
	$eje_usu->bindParam(":usuario",$usu,PDO::PARAM_STR);
	$eje_usu->bindParam(":passwords",$pas,PDO::PARAM_STR);
	$eje_usu->bindParam(":estado",$est,PDO::PARAM_STR);
	$eje_usu->execute();
	$con = $eje_usu->rowCount();
	//$dat = $eje_usu->fetch(PDO::FETCH_OBJ);
	
	if($con > 0)
	{
		$dat = $eje_usu->fetch(PDO::FETCH_ASSOC);
		session_start();
		header("location: ../login/pasarela.php");
		$_SESSION["nombre_usu"] = $dat["nombre_completo"];
		$_SESSION["nivel"] = $dat["valor_nivel"];
		$_SESSION["usuario"] = $dat["login_usuario"];
		$_SESSION["practicas"] = "InciarSesion";
	}
	else
	{
		?>
        <script language="javascript">
		location.href = '../index.php'
		alert('Usuario No Registrado')
        </script>
        <?php
	}
	
}
else
{
}

?>