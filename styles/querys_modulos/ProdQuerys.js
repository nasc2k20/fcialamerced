var DirectorioProd = 'productos/';
$(function () {

    CargarProcesos(1);


    $('#BusquedaTxt').keyup(function () {
        var search = $(this).val();
        if (search != '') {
            CargarProcesos(1);
        } else {
            CargarProcesos(1);
        }
    });

});

function CargarProcesos(NumPag) {
    var NumxPag = 10;
    var BusquedaProd = $('#BusquedaTxt').val();

    var Datos = {
        "BusquedaProd": BusquedaProd,
        "NumxPag": NumxPag,
        "NumPag": NumPag
    };

    //$('#VerDatosProcesos').fadeIn('slow');

    $.ajax({
        url: DirectorioProd + "ver_productos.php",
        type: "POST",
        data: Datos,
        beforeSend: function () {
            $("#MostrarDatosProd").html("<center><img src='../images/ajax-loader.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
        },
        success: function (data) {
            $('#MostrarDatosProd').html(data).fadeIn('slow');
            //$('#VerDatosProcesos').html("");
        }
    });
}

function AgregarProd() {
    var CodigoProd = $('#codigo').val();
    var CodBarraProd = $('#codigo_barra').val();
    var NombreProd = $('#nombre_producto').val();
    var IndicaProd = $('#indicaciones').val();
    var CodeLab = $('#laboratorio').val();
    var PrecioDnm = $('#precio_dnm').val();
    var PrecioPub = $('#precio_publico').val();
    var PrecioCosto = $('#precio_costo').val();
    var PrecioVenta = $('#precio_venta').val();
    var PresentaProd = $('#presentacion').val();
    var VitrinaProd = $('#vitrina').val();
    var ImpuestoProd = $('#impuesto').val();
    var SaldoProd = $('#existencia').val();
    var VenceProd = $('#fecha_vence').val();
    var EstadoProd = $('#estado_prod').val();
    var SqlQuery = 'insertar';

    var Parametros = {
        "SqlQuery": SqlQuery,
        "CodigoProd": CodigoProd,
        "CodBarraProd": CodBarraProd,
        "NombreProd": NombreProd,
        "IndicaProd": IndicaProd,
        "CodeLab": CodeLab,
        "PrecioDnm": PrecioDnm,
        "PrecioPub": PrecioPub,
        "PrecioCosto": PrecioCosto,
        "PrecioVenta": PrecioVenta,
        "PresentaProd": PresentaProd,
        "VitrinaProd": VitrinaProd,
        "ImpuestoProd": ImpuestoProd,
        "SaldoProd": SaldoProd,
        "VenceProd": VenceProd,
        "EstadoProd": EstadoProd
    };

    if (CodigoProd == '') {
        alert('Debe de Digitar el Codigo del Producto');
        $('#codigo').focus();
    } else if (NombreProd == '') {
        alert('Debe de Digitar el Nombre del Producto');
        $('#nombre_producto').focus();
    } else if (CodeLab == '') {
        alert('Debe de Elegir el Laboratorio');
        $('#laboratorio').focus();
    } else if (PrecioPub == '') {
        alert('Debe de Digitar el Precio Publico');
        $('#precio_publico').focus();
    } else if (PrecioCosto == '') {
        alert('Debe de Digitar el Costo del Producto');
        $('#precio_costo').focus();
    } else if (PrecioVenta == '') {
        alert('Debe de Digitar el Precio del Producto');
        $('#precio_venta').focus();
    } else if (SaldoProd == '') {
        alert('Debe de Digitar el Vencimiento del Producto');
        $('#fecha_vence').focus();
    } else if (EstadoProd == '') {
        alert('Debe de Elegir el Estado del Producto');
        $('#estado_prod').focus();
    } else {
        $.ajax({
            type: "POST",
            url: DirectorioProd + "ProcesosSQL.php",
            data: Parametros,
            beforeSend: function (objeto) {
                $("#MostrarDatosProd").html("<center><img src='../images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
            },
            success: function (datos) {
                $('#AgregarProdModal').modal('hide');

                if ($('.modal-backdrop').is(':visible')) {
                    $('.modal-backdrop').remove();
                };

                $("#MostrarDatosProd").html(datos);
            }
        });
    }
}


function EditarProd(CodeProd) {
    //var CodigoProd = $('#codigo').val();
    var CodBarraProd = $('#codigo_barra_' + CodeProd).val();
    var NombreProd = $('#nombre_producto_' + CodeProd).val();
    var IndicaProd = $('#indicaciones_' + CodeProd).val();
    var CodeLab = $('#laboratorio_' + CodeProd).val();
    var PrecioDnm = $('#precio_dnm_' + CodeProd).val();
    var PrecioPub = $('#precio_publico_' + CodeProd).val();
    var PrecioCosto = $('#precio_costo_' + CodeProd).val();
    var PrecioVenta = $('#precio_venta_' + CodeProd).val();
    var PresentaProd = $('#presentacion_' + CodeProd).val();
    var VitrinaProd = $('#vitrina_' + CodeProd).val();
    var ImpuestoProd = $('#impuesto_' + CodeProd).val();
    var SaldoProd = $('#existencia_' + CodeProd).val();
    var VenceProd = $('#fecha_vence_' + CodeProd).val();
    var EstadoProd = $('#estado_prod_' + CodeProd).val();
    var SqlQuery = 'actualizar';

    var Parametros = {
        "SqlQuery": SqlQuery,
        "CodigoProd": CodeProd,
        "CodBarraProd": CodBarraProd,
        "NombreProd": NombreProd,
        "IndicaProd": IndicaProd,
        "CodeLab": CodeLab,
        "PrecioDnm": PrecioDnm,
        "PrecioPub": PrecioPub,
        "PrecioCosto": PrecioCosto,
        "PrecioVenta": PrecioVenta,
        "PresentaProd": PresentaProd,
        "VitrinaProd": VitrinaProd,
        "ImpuestoProd": ImpuestoProd,
        "SaldoProd": SaldoProd,
        "VenceProd": VenceProd,
        "EstadoProd": EstadoProd
    };

    if (CodeProd == '') {
        alert('Debe de Digitar el Codigo del Producto');
        $('#codigo').focus();
    } else if (NombreProd == '') {
        alert('Debe de Digitar el Nombre del Producto');
        $('#nombre_producto').focus();
    } else if (CodeLab == '') {
        alert('Debe de Elegir el Laboratorio');
        $('#laboratorio').focus();
    } else if (PrecioPub == '') {
        alert('Debe de Digitar el Precio Publico');
        $('#precio_publico').focus();
    } else if (SaldoProd=''){
        alert('Debe de Digitar La Existencia del Producto');
        $('#existencia').focus();
    } else if (VenceProd == '') {
        alert('Debe de Digitar el Vencimiento del Producto');
        $('#fecha_vence').focus();
    } else if (EstadoProd == '') {
        alert('Debe de Elegir el Estado del Producto');
        $('#estado_prod').focus();
    } else {
        $.ajax({
            type: "POST",
            url: DirectorioProd + "ProcesosSQL.php",
            data: Parametros,
            beforeSend: function (objeto) {
                $("#MostrarDatosProd").html("<center><img src='../images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
            },
            success: function (datos) {
                $('#EditarProdModal' + CodeProd).modal('hide');

                if ($('.modal-backdrop').is(':visible')) {
                    //$('body').removeClass('modal-open'); 
                    $('.modal-backdrop').remove();
                };

                $("#MostrarDatosProd").html(datos);
            }
        });
    }
}

function EliminarProd(CodigoProd, NombreProd) {
    var SqlQuery = 'eliminar';
    var Parametros = {
        "SqlQuery": SqlQuery,
        "EliminarID": CodigoProd
    }

    $.confirm({
        icon: 'fa fa-smile-o',
        title: 'Eliminaci&oacute;n',
        theme: 'modern',
        closeIcon: true,
        content: 'Desea Eliminar El Producto: <br> <strong><h3>' + NombreProd + '</h3></strong>',
        animation: 'scale',
        icon: 'fa fa-warning',
        type: 'orange',
        buttons: {
            ok: {
                text: 'Confirmar',
                btnClass: 'btn-blue',
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: DirectorioProd + "ProcesosSQL.php",
                        data: Parametros,
                        beforeSend: function (objeto) {
                            $("#MostrarDatosProd").html("<center><img src='../images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
                        },
                        success: function (datos) {
                            $("#MostrarDatosProd").html(datos);
                        }
                    });
                },
            },
            cancel: {
                text: 'Cancelar',
                btnClass: 'btn-red',
                action: function () {}
            }
        }
    });
}