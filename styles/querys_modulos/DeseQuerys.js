/*
$(document).ready(function () {
    load_data();
});
*/

$(function(){
    load_data();
});

function load_data(query) {
    $.ajax({
        url: "modulos/desechos/ver_desechos.php",
        method: "POST",
        data: {
            query: query
        },
        success: function (data) {
            $('#MostrarDatosDese').html(data);
        }
    });
}

function CargarActividad(CodActividad) {
    $.ajax({
        url: "modulos/desechos/ver_actividad.php",
        method: "POST",
        data: {
            CodActividad: CodActividad
        },
        success: function (data) {
            $('#MostrarActividadDiv').html(data);
        }
    });
}

