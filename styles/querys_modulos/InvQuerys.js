var DirectorioInv = 'modulos/inventario/';
$(function () {

     CargarProcesos(1);

     $('#BusquedaTxt').keyup(function () {
          var search = $(this).val();
          if (search != '') {
               CargarProcesos(1);
          } else {
               CargarProcesos(1);
          }
     });

});

function CargarProcesos(NumPag) {
     var NumxPag = 10;
     var BusquedaProd = $('#BusquedaTxt').val();

     var Datos = {
          "BusquedaProd": BusquedaProd,
          "NumxPag": NumxPag,
          "NumPag": NumPag
     };

     $.ajax({
          url: DirectorioInv + "productos_inventario.php",
          type: "POST",
          data: Datos,
          beforeSend: function () {
               $("#MostrarDatosTomaGen").html("<center><img src='images/ajax-loader.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
          },
          success: function (data) {
               $('#MostrarDatosTomaGen').html(data).fadeIn('slow');
          }
     });
}

function CalcularExistenciaAum(CodigoProd)
{
    //$('#AumentarCant_'+CodigoProd).keyup(function(){});
      var SaldoProd = $('#SaldoProd_'+CodigoProd).val();
      var AumentoSaldo = $('#AumentarCant_'+CodigoProd).val();
      //var TotalNuevaExiste = SaldoProd + AumentoSaldo;
      var TotalNuevaExiste = parseFloat($('#SaldoProd_'+CodigoProd).val()) + parseFloat($('#AumentarCant_'+CodigoProd).val());
      //alert(TotalNuevaExiste);
      document.getElementById('NuevoSaldo_'+CodigoProd).value = TotalNuevaExiste;
      //$('#NuevoSaldo_'+CodigoProd).html(TotalNuevaExiste);
    
}

function CalcularExistenciaDis(CodigoProd, ExistenciaProd)
{
	var DisminuirSaldo = $('#AumentarCantDis_'+CodigoProd).val();
	if(parseInt(DisminuirSaldo)>parseInt(ExistenciaProd))
	{
		alert('La Cantidad a Disminuir es Mayor que el Saldo');
	}
	else
	{
	    var TotalNuevaExisteDis = parseFloat(ExistenciaProd) - parseFloat($('#AumentarCantDis_'+CodigoProd).val());
	    //alert(TotalNuevaExiste);
	    document.getElementById('NuevoSaldoDis_'+CodigoProd).value = TotalNuevaExisteDis;
	    //$('#NuevoSaldo_'+CodigoProd).html(TotalNuevaExiste);
	}
    
}

function AumentarExistencia(CodigoProd)
{
	var SaldoProd = $('#SaldoProd_'+CodigoProd).val();
    var AumentoSaldo = $('#AumentarCant_'+CodigoProd).val();
    var NuevoSaldo = $('#NuevoSaldo_'+CodigoProd).val();
    var SqlQuery = "aumentar";

    var Parametros = {
        "SqlQuery": SqlQuery,
        "SaldoProd": SaldoProd,
        "CodeProd": CodigoProd,
        "AumentoSaldo": AumentoSaldo,
        "NuevoSaldo": NuevoSaldo
    };

    if(AumentoSaldo=='')
    {
        alert('Debe de Digitar la Cantidad a Aumentar');
        $('#AumentarCant_'+CodigoProd).focus();
    }
    else if(AumentoSaldo<=0)
    {
        alert('La Cantidad a Aumentar no Puede ser Menor o Igual a Cero');
        $('#AumentarCant_'+CodigoProd).focus();
    }
    else
    {
        $.ajax({
            type: "POST",
            url: DirectorioInv + "ProcesosSQL.php",
            data: Parametros,
            beforeSend: function (objeto) {
                $("#MostrarDatosTomaGen").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
            },
            success: function (datos) {
                $('#AumentarTomaModal'+CodigoProd).modal('hide');

                if ($('.modal-backdrop').is(':visible')) {
                    //$('body').removeClass('modal-open'); 
                    $('.modal-backdrop').remove();
                };

                $("#MostrarDatosTomaGen").html(datos);
            }
        });
    }
}

function DisminuirExistencia(CodigoProd)
{
	var SaldoProd = $('#SaldoProdDis_'+CodigoProd).val();
    var AumentoSaldo = $('#AumentarCantDis_'+CodigoProd).val();
    var NuevoSaldo = $('#NuevoSaldoDis_'+CodigoProd).val();
    var SqlQuery = "disminuir";

    var Parametros = {
        "SqlQuery": SqlQuery,
        "SaldoProd": SaldoProd,
        "CodeProd": CodigoProd,
        "AumentoSaldo": AumentoSaldo,
        "NuevoSaldo": NuevoSaldo
    };

    if(parseInt(AumentoSaldo)>parseInt(SaldoProd))
    {
        alert('La Cantidad a Disminuir No puede ser Mayor a la Existencia');
        $('#AumentarCantDis_'+CodigoProd).focus();
    }
    else
    {
        $.ajax({
            type: "POST",
            url: DirectorioInv + "ProcesosSQL.php",
            data: Parametros,
            beforeSend: function (objeto) {
                $("#MostrarDatosTomaGen").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
            },
            success: function (datos) {
                $('#DisminuirTomaModal'+CodigoProd).modal('hide');

                if ($('.modal-backdrop').is(':visible')) {
                    //$('body').removeClass('modal-open'); 
                    $('.modal-backdrop').remove();
                };

                $("#MostrarDatosTomaGen").html(datos);
            }
        });
    }
}