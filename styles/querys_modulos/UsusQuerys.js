$(document).ready(function () {
    //load_data_usuarios();
    //load_data_pagination();
    /*
    function load_data_usuarios(query)
     {
      $.ajax({
       url:"modulos/usuarios/ver_usuarios.php",
       method:"POST",
       data:{query:query},
       success:function(data)
       {
        $('#outer_div_usuarios').html(data);
       }
      });
     }
    
    $('#BusquedaTxt').keyup(function(){
      var search = $(this).val();
      if(search != '')
      {
       load_data_usuarios(search);
      }
      else
      {
       load_data_usuarios();
      }
     });
    */

    $('#login_usu').keyup(function (e) {
        var log_user = $(this).val();
        var dataString = 'usuario=' + log_user;
        $('#Verificando').html('<img src="images/ajax-loader.gif" />');
        $.ajax({
            type: "POST",
            url: "modulos/usuarios/ComprobarUsuario.php",
            data: dataString,
            beforeSend: function(){
                $("#MostrarDatosUsu").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
            },
            success: function (data) {
                $('#Verificando').html(data);
                //$('#Verificando').fadeIn(1000).html(data);
            }
        });
    });


    function getAll() {
        $.ajax({
            url: 'modulos/usuarios/ver_usuarios.php',
            data: 'action=showAll',
            cache: false,
            beforeSend: function(){
                $("#MostrarDatosUsu").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
            },
            success: function (r) {
                $("#MostrarDatosUsu").html(r);
            }
        });
    }
    getAll();


    $("#nivel").change(function () {
        var id = $(this).find(":selected").val();
        var dataString = 'action=' + id;
        $.ajax({
            url: 'modulos/usuarios/ver_usuarios.php',
            data: dataString,
            cache: false,
            beforeSend: function(){
                $("#MostrarDatosUsu").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
            },
            success: function (r) {
                $("#MostrarDatosUsu").html(r);
            }
        });
    });
    
    
    var consulta;
    $("#Usuario").focus();
    $("#Usuario").keyup(function (e) {
        consulta = $("#Usuario").val();
        $("#ValidarUsu").delay(3000).queue(function (n) {
            $("#ValidarUsu").html('<img src="images/ajax-loader.gif" width="10" height="10" />');
            $.ajax({
                type: "POST",
                url: "modulos/usuarios/ValidarUsu.php",
                data: "b=" + consulta,
                dataType: "html",
                beforeSend: function () {
                    $("#ValidarUsu").html("<center><img src='images/ajax-loader.gif' width='15' height='15' class='display:block; margin:auto;'></center>");
                },
                success: function (data) {
                    $("#ValidarUsu").html(data);
                    n();
                }
            });
        });
    });
    
    
});
