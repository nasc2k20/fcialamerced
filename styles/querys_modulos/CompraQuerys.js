var DirectorioComp = 'modulos/compras/';
$(function () {

    CargarProcesos(1);
    NuevaCompraHeader();

    $('#BusquedaTxt').keyup(function () {
        var search = $(this).val();
        if (search != '') {
            CargarProcesos(1);
        } else {
            CargarProcesos(1);
        }
    });
});

function CargarProcesos(NumPag) {
    var NumxPag = 10;
    var BusquedaTxt = $('#BusquedaTxt').val();

    var Datos = {
        "NumxPag": NumxPag,
        "BusquedaTxt": BusquedaTxt,
        "NumPag": NumPag
    };

    $.ajax({
        url: DirectorioComp + "ver_compras.php",
        type: "POST",
        data: Datos,
        beforeSend: function () {
            $("#MostrarDatosCompras").html("<center><img src='images/ajax-loader.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
        },
        success: function (data) {
            $('#MostrarDatosCompras').html(data).fadeIn('slow');
        }
    });
}

function CargarCompraTemp(CodCompra, CodProv) {
    var Datos = {
        "CodCompra": CodCompra,
        "CodProv": CodProv
    };

    $.ajax({
        url: "modulos/reportes/Rep_CompraFact.php",
        type: "POST",
        data: Datos,
        beforeSend: function () {
            $('#resultados_prodDetLista').html("<center><img src='images/ajax-loader.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
        },
        success: function (data) {
            $('#resultados_prodDetLista').html(data);
        }
    });
}

function NuevaCompraHeader() {
    $.ajax({
        url: DirectorioComp + "NuevaCompraHeader.php",
        method: "POST",
        data: {
            query: ""
        },
        beforeSend: function () {
            $("#HeaderNuevaCompras").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
        },
        success: function (data) {
            $('#HeaderNuevaCompras').html(data);
            var CodCompra = $('#cod_compra').val();
        
            CargarCompraProdDet(CodCompra);
            
        }
    });
}

function CargarCompraProdDet(CodCompra) {
    $.ajax({
        url: DirectorioComp + "ver_compras_detalle.php",
        method: "POST",
        data: {
            CodCompra: CodCompra
        },
        beforeSend: function () {
            $("#DetalleNuevaCompras").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
        },
        success: function (data) {
            $('#DetalleNuevaCompras').html(data);
            //$('#resultados').html(data);
        }
    });
}

function EliminarProdDetCompra(CodDetCompra, CodigoProd, NombreProd, CodCompra) {
    var SqlQuery = 'eliminar';
    var Parametros = {
        "SqlQuery": SqlQuery,
        "EliminarID": CodigoProd,
        "CodDetCompra": CodDetCompra,
        "CodigoCompra": CodCompra
    };

    $.confirm({
        icon: 'fa fa-smile-o',
        title: 'Eliminaci&oacute;n',
        theme: 'modern',
        closeIcon: true,
        columnClass: 'col-md-4 col-md-offset-4',
        content: 'Desea Eliminar El producto: <br> <strong><h3>' + NombreProd + ' de la Factura #' + CodCompra + '</h3></strong>',
        animation: 'scale',
        icon: 'fa fa-warning',
        type: 'orange',
        draggable: true,
        buttons: {
            ok: {
                text: 'Confirmar',
                btnClass: 'btn-blue',
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: DirectorioComp + "ProcesosSQL.php",
                        data: Parametros,
                        beforeSend: function (objeto) {
                            $("#DetalleNuevaCompras").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
                        },
                        success: function (datos) {
                            $("#DetalleNuevaCompras").html(datos);
                        }
                    });
                },
            },
            cancel: {
                text: 'Cancelar',
                btnClass: 'btn-red',
                action: function () {}
            }
        }
    });
}

function AnularCompra(CodCompra, NumFactura) {
    var SqlQuery = 'anular';
    var Parametros = {
        "SqlQuery": SqlQuery,
        "EliminarID": CodCompra,
        "NumFactura": NumFactura
    };

    $.confirm({
        icon: 'fa fa-smile-o',
        title: 'Anulaci&oacute;n',
        theme: 'modern',
        closeIcon: true,
        content: 'Desea Anular la Compra Numero: <br> <strong><h3>' + NumFactura + ' </h3></strong>',
        animation: 'scale',
        icon: 'fa fa-check-circle',
        type: 'blue',
        draggable: true,
        buttons: {
            ok: {
                text: 'Confirmar',
                btnClass: 'btn-red',
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: DirectorioComp + "ProcesosSQL.php",
                        data: Parametros,
                        beforeSend: function (objeto) {
                            $("#DetalleNuevaCompras").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
                        },
                        success: function (datos) {
                            $("#DetalleNuevaCompras").html(datos);
                        }
                    });
                },
            },
            cancel: {
                text: 'Cancelar',
                btnClass: 'btn-gray',
                action: function () {}
            }
        }
    });
}