var DirectorioProv = 'modulos/proveedores/';
$(function () {
    var consulta;

    CargarProcesos(1);


    $('#BusquedaTxt').keyup(function () {
        var search = $(this).val();
        if (search != '') {
            CargarProcesos(1);
        } else {
            CargarProcesos(1);
        }
    });

    $("#NrcProv").focus();
    $("#NrcProv").keyup(function (e) {
        consulta = $("#NrcProv").val();
        $("#ComprobarNrc").delay(3000).queue(function (n) {
            $("#ComprobarNrc").html('<img src="images/ajax-loader.gif" width="10" height="10" />');
            $.ajax({
                type: "POST",
                url: DirectorioProv + "ComprobarNrc.php",
                data: "b=" + consulta,
                dataType: "html",
                beforeSend: function () {
                    $("#ComprobarNrc").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
                },
                success: function (data) {
                    $("#ComprobarNrc").html(data);
                    n();
                }
            });
        });
    });

});

function CargarProcesos(NumPag) {
    var NumxPag = 10;
    var BusquedaProv = $('#BusquedaTxt').val();

    var Datos = {
        "BusquedaProv": BusquedaProv,
        "NumxPag": NumxPag,
        "NumPag": NumPag
    };

    $.ajax({
        url: DirectorioProv + "ver_proveedores.php",
        type: "POST",
        data: Datos,
        beforeSend: function () {
            $("#MostrarDatosProv").html("<center><img src='images/ajax-loader.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
        },
        success: function (data) {
            $('#MostrarDatosProv').html(data).fadeIn('slow');
            //$('#VerDatosProcesos').html("");
        }
    });
}

function AgregarProv() {
    var NrcProv = $('#NrcProv').val();
    var NombreProv = $('#NombreProv').val();
    var TelefonoProv = $('#TelefonoProv').val();
    var VendedorProv = $('#VendedorProv').val();
    var CelularProv = $('#CelularProv').val();
    var DiasProv = $('#DiasProv').val();
    var SqlQuery = "insertar";

    var Parametros = {
        "NrcProv": NrcProv,
        "NombreProv": NombreProv,
        "TelefonoProv": TelefonoProv,
        "VendedorProv": VendedorProv,
        "CelularProv": CelularProv,
        "DiasProv": DiasProv,
        "SqlQuery": SqlQuery
    };

    if (NrcProv == '') {
        alert('Digite el NRC del Proveedor');
        $('#NrcProv').focus();
    } else if (NombreProv == '') {
        alert('Digite el Nombre del Proveedor');
        $('#NombreProv').focus();
    } else if (VendedorProv == '') {
        alert('Digite el Nombre del Vendedor');
        $('#VendedorProv').focus();
    } else if (DiasProv == '') {
        alert('Elija Los Dias de Credito');
        $('#DiasProv').focus();
    } else {
        $.ajax({
            type: "POST",
            url: DirectorioProv + "ProcesosSQL.php",
            data: Parametros,
            beforeSend: function (data) {
                $("#MostrarDatosProv").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
            },
            success: function (datos) {
                $('#AgregarProvModal').modal('hide');
                if ($('.modal-backdrop').is(':visible')) {
                    $('.modal-backdrop').remove();
                };
                $("#MostrarDatosProv").html(datos);
            }
        });
    }
}



function EditarProv(CodigoProv) {
    var NrcProv = $('#NrcProv_'+CodigoProv).val();
    var NombreProv = $('#NombreProv_'+CodigoProv).val();
    var TelefonoProv = $('#TelefonoProv_'+CodigoProv).val();
    var VendedorProv = $('#VendedorProv_'+CodigoProv).val();
    var CelularProv = $('#CelularProv_'+CodigoProv).val();
    var DiasProv = $('#DiasProv_'+CodigoProv).val();
    var SqlQuery = "actualizar";

    var Parametros = {
        "NrcProv": NrcProv,
        "NombreProv": NombreProv,
        "TelefonoProv": TelefonoProv,
        "VendedorProv": VendedorProv,
        "CelularProv": CelularProv,
        "DiasProv": DiasProv,
        "CodigoProv": CodigoProv,
        "SqlQuery": SqlQuery
    };

    if (NrcProv == '') {
        alert('Digite el NRC del Proveedor');
        $('#NrcProv_'+CodigoProv).focus();
    } else if (NombreProv == '') {
        alert('Digite el Nombre del Proveedor');
        $('#NombreProv_'+CodigoProv).focus();
    } else if (VendedorProv == '') {
        alert('Digite el Nombre del Vendedor');
        $('#VendedorProv_'+CodigoProv).focus();
    } else if (DiasProv == '') {
        alert('Elija Los Dias de Credito');
        $('#DiasProv_'+CodigoProv).focus();
    } else {
        $.ajax({
            type: "POST",
            url: DirectorioProv + "ProcesosSQL.php",
            data: Parametros,
            beforeSend: function (data) {
                $("#MostrarDatosProv").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
            },
            success: function (datos) {
                $('#EditarProvModal'+CodigoProv).modal('hide');
                if ($('.modal-backdrop').is(':visible')) {
                    $('.modal-backdrop').remove();
                };
                $("#MostrarDatosProv").html(datos);
            }
        });
    }
}


function EliminarProv(CodigoProv, NombreProv) {
    var SqlQuery = 'eliminar';
    var Parametros = {
        "SqlQuery": SqlQuery,
        "EliminarID": CodigoProv
    };

    $.confirm({
        icon: 'fa fa-smile-o',
        title: 'Eliminaci&oacute;n',
        theme: 'modern',
        closeIcon: true,
        content: 'Desea Eliminar El Proveedor: <br> <strong><h3>' + NombreProv + '</h3></strong>',
        animation: 'scale',
        icon: 'fa fa-warning',
        type: 'orange',
        buttons: {
            ok: {
                text: 'Confirmar',
                btnClass: 'btn-blue',
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: DirectorioProv + "ProcesosSQL.php",
                        data: Parametros,
                        beforeSend: function (objeto) {
                            $("#MostrarDatosProv").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
                        },
                        success: function (datos) {
                            $("#MostrarDatosProv").html(datos);
                        }
                    });
                },
            },
            cancel: {
                text: 'Cancelar',
                btnClass: 'btn-red',
                action: function () {}
            }
        }
    });
}