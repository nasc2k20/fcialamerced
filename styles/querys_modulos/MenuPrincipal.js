$(document).ready(function(){
    $('.MostrarLabItem').click(function () {
        $('#MostrarLab').load('laboratorios/index.php', function () {
            $('#btnCerrarLab').click(function () {
                $('#MostrarLab').empty();
            });
        });
    });

    $('.MostrarProdItem').click(function () {
        $('#MostrarProd').load('productos/index.php', function () {
            $('#btnCerrarProd').click(function () {
                $('#MostrarProd').empty();
            });
        });
    });

    $('.MostrarProdTomaInv').click(function () {
        $('#MostrarProdToma').load('inventario/index_inventario.php', function () {
            $('#btnCerrarToma').click(function () {
                $('#MostrarProdToma').empty();
            });
        });
    });

    $('.MostrarKardexItem').click(function () {
        $('#MostrarKardex').load('kardex/index.php', function () {
            $('#btnCerrarKardex').click(function () {
                $('#MostrarKardex').empty();
            });
        });
    });

    $('.MostrarProvItem').click(function () {
        $('#MostrarProv').load('proveedores/index.php', function () {
            $('#btnCerrarProv').click(function () {
                $('#MostrarProv').empty();
            });
        });
    });

    $('.MostrarUsusItem').click(function () {
        $('#MostrarUsuario').load('usuarios/index.php', function () {
            $('#btnCerrarUsu').click(function () {
                $('#MostrarUsuario').empty();
            });
        });
    });

    $('.MostrarTomaInvGenItem').click(function () {
        $('#ReporteTomaInvGen').load('reportes_vistas/VerInventarioGen.php', function () {
            $('#btnCerrarTIG').click(function () {
                $('#ReporteTomaInvGen').empty();
            });
        });
    });

    $('.MostrarTomaInvLabItem').click(function () {
        $('#ReporteTomaInvLab').load('reportes_vistas/VerInventarioLab.php', function () {
            $('#btnCerrarTIG').click(function () {
                $('#ReporteTomaInvLab').empty();
            });
        });
    });

    $('.MostrarComprasItem').click(function () {
        $('#MostrarCompras').load('compras/index.php', function () {
            $('#btnCerrarCompra').click(function () {
                $('#MostrarCompras').empty();
            });
        });
    });

    $('.PerfilUsuarioItem').click(function () {
        $('#MostrarUsuarioPerfil').load('usuarios/Formularios.php', function () {
            $('#btnCerrarUsuPerfil').click(function () {
                $('#MostrarUsuarioPerfil').empty();
            });
        });
    });

    $('.VerFacturasItem').click(function () {
        $('#MostrarFacturas').load('facturacion/index.php', function () {
            $('#btnCerrarFactura').click(function () {
                $('#MostrarFacturas').empty();
            });
        });
    });

    $('.MostrarDeseItem').click(function () {
        $('#MostrarDesechos').load('desechos/index.php', function () {
            $('#btnCerrarDes').click(function () {
                $('#MostrarDesechos').empty();
            });
        });
    });

    $('.VerFacturasFechasItem').click(function () {
        $('#ReporteVentasFecha').load('reportes_vistas/VerFacturasFechas.php', function () {
            $('#btnCerrarFactVta').click(function () {
                $('#ReporteVentasFecha').empty();
            });
        });
    });

    $('.MostrarBackupItem').click(function () {
        $('#MostrarBackup').load('backup/index.php', function () {
            $('#btnCerrarBackup').click(function () {
                $('#MostrarBackup').empty();
            });
        });
    });

    $('.MostrarInvDnm').click(function () {
        $('#MostrarInvDnmDv').load('reportes_vistas/VerInventarioDnm.php', function () {
            $('#btnCerrarTIGDNM').click(function () {
                $('#MostrarInvDnmDv').empty();
            });
        });
    });
});