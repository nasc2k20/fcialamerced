var DirectorioLab = 'modulos/laboratorios/';
$(function () {

    CargarProcesos(1);

});

function CargarProcesos(NumPag) {
    var NumxPag = 10;

    var Datos = {
        "NumxPag": NumxPag,
        "NumPag": NumPag
    };

    $.ajax({
        url: DirectorioLab + "ver_laboratorio.php",
        type: "POST",
        data: Datos,
        beforeSend: function () {
            $("#MostrarDatosFab").html("<center><img src='images/ajax-loader.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
        },
        success: function (data) {
            $('#MostrarDatosFab').html(data).fadeIn('slow');
        }
    });
}

function AgregarLab() {
    var NombreLab = $('#nombre').val();
    var Descuento = $('#descuento').val();
    var DiasRetira = $('#retira').val();
    var SqlQuery = 'agregar';

    var Parametros = {
        "SqlQuery": SqlQuery,
        "NombreLab": NombreLab,
        "Descuento": Descuento,
        "DiasRetira": DiasRetira
    };

    if (NombreLab == '') {
        alert('Debe de Digitar el Nombre del Fabricante');
        $('#nombre').focus();
    } else if (Descuento == '') {
        alert('Debe de Digitar el Descuento');
        $('#descuento').focus();
    } else if (DiasRetira == '') {
        alert('Debe de Digitar Los Dias de Retiro');
        $('#retira').focus();
    } else {
        $.ajax({
            type: "POST",
            url: "modulos/laboratorios/ProcesosSQL.php",
            data: Parametros,
            beforeSend: function (objeto) {
                $("#MostrarDatosFab").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
            },
            success: function (datos) {
                $('#AgregarLabModal').modal('hide');

                if ($('.modal-backdrop').is(':visible')) {
                    //$('body').removeClass('modal-open'); 
                    $('.modal-backdrop').remove();
                };

                $("#MostrarDatosFab").html(datos);
            }
        });
    }
}


function EditarLab(CodigoLab) {
    var NombreLab = $('#nombre_' + CodigoLab).val();
    var Descuento = $('#descuento_' + CodigoLab).val();
    var DiasRetira = $('#retira_' + CodigoLab).val();
    var SqlQuery = 'actualizar';

    var Parametros = {
        "SqlQuery": SqlQuery,
        "NombreLab": NombreLab,
        "Descuento": Descuento,
        "DiasRetira": DiasRetira,
        "CodeLab": CodigoLab
    };

    if (NombreLab == '') {
        alert('Debe de Digitar el Nombre del Fabricante');
        $('#nombre_' + CodigoLab).focus();
    } else if (Descuento == '') {
        alert('Debe de Digitar el Descuento');
        $('#descuento_' + CodigoLab).focus();
    } else if (DiasRetira == '') {
        alert('Debe de Digitar Los Dias de Retiro');
        $('#retira_' + CodigoLab).focus();
    } else {
        $.ajax({
            type: "POST",
            url: "modulos/laboratorios/ProcesosSQL.php",
            data: Parametros,
            beforeSend: function (objeto) {
                $("#MostrarDatosFab").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
            },
            success: function (datos) {
                $('#EditarLabModal' + CodigoLab).modal('hide');

                if ($('.modal-backdrop').is(':visible')) {
                    //$('body').removeClass('modal-open'); 
                    $('.modal-backdrop').remove();
                };

                $("#MostrarDatosFab").html(datos);
            }
        });
    }
}

function EliminarLab(CodigoLab, NombreLab) {
    var SqlQuery = 'eliminar';
    var Parametros = {
        "SqlQuery": SqlQuery,
        "EliminarID": CodigoLab
    };

    $.confirm({
        icon: 'fa fa-smile-o',
        title: 'Eliminaci&oacute;n',
        theme: 'modern',
        closeIcon: true,
        content: 'Desea Eliminar El Laboratorio: <br> <strong><h3>' + NombreLab + '</h3></strong>',
        animation: 'scale',
        icon: 'fa fa-warning',
        type: 'orange',
        buttons: {
            ok: {
                text: 'Confirmar',
                btnClass: 'btn-blue',
                action: function () {
                    $.ajax({
                        type: "POST",
                        url: "modulos/laboratorios/ProcesosSQL.php",
                        data: Parametros,
                        beforeSend: function (objeto) {
                            $("#MostrarDatosFab").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
                        },
                        success: function (datos) {
                            $("#MostrarDatosFab").html(datos);
                        }
                    });
                },
            },
            cancel: {
                text: 'Cancelar',
                btnClass: 'btn-red',
                action: function () {}
            }
        }
    });
}
