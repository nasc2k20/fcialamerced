var DirectorioKard = 'modulos/kardex/';

$(function () {

    CargarProcesos(1);

    $('#BusquedaTxt').keyup(function () {
        var search = $(this).val();
        if (search != '') {
            CargarProcesos(1);
        } else {
            CargarProcesos(1);
        }
    });

});

function CargarProcesos(NumPag) {
    var NumxPag = 10;
    var BusquedaProd = $('#BusquedaTxt').val();

    var Datos = {
        "BusquedaProd": BusquedaProd,
        "NumxPag": NumxPag,
        "NumPag": NumPag
    };

    $.ajax({
        url: DirectorioKard + "ver_kardex.php",
        type: "POST",
        data: Datos,
        beforeSend: function () {
            $("#MostrarDatosKardex").html("<center><img src='images/ajax-loader.gif' width='50' height='50' class='display:block; margin:auto;'></center>");
        },
        success: function (data) {
            $('#MostrarDatosKardex').html(data).fadeIn('slow');
        }
    });
}

function ConsultarKardex(CodigoProd) {
    $.ajax({
        url: DirectorioKard + "ver_kardex_indiv.php",
        method: "POST",
        data: {
            CodigoProd: CodigoProd
        },
        beforeSend: function () {
            $("#ListadoKardex").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
        },
        success: function (data) {
            $('#ListadoKardex').html(data);
        }
    });
}