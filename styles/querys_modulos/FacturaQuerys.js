$(document).ready(function () {

    CargarDatosFacturas();

    $('#BusquedaTxt').keyup(function () {
        var search = $(this).val();
        if (search != '') {
            CargarDatosFacturas(search);
        } else {
            CargarDatosFacturas();
        }
    });


});


function CargarDatosFacturas(query) {
    $.ajax({
        url: "modulos/facturacion/ver_facturas.php",
        method: "POST",
        data: {
            query: query
        },
        beforeSend: function () {
            $("#MostrarDatosFacturas").html("<center><img src='images/ajax-loader.gif' width='100' height='100' class='display:block; margin:auto;'></center>");
        },
        success: function (data) {
            $('#MostrarDatosFacturas').html(data);
        }
    });
}

function VerFacturaVtaDet(CodFactura) {
    $.ajax({
        url: "modulos/reportes/Rep_FacturaVta.php",
        method: "POST",
        data: {
            CodFactura: CodFactura
        },
        success: function (data) {
            $('#FactDetLista').html(data);
        }
    });
}
