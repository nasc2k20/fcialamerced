<?php
include '../conectar.php';

if(isset($_POST['busquedabtn']))
{
    //echo 'aqui';
    $fecha_busqueda = $_POST['busquedatext'];
    $convert_fecha = new datetime($fecha_busqueda);
    $fecha_buscar = $convert_fecha->format('Y-m-d');
    
    $sel_nfact = "SELECT * FROM ventas 
                  WHERE fecha_venta = '$fecha_buscar'
                  AND venta_anular='NO' 
                  ORDER BY date_venta DESC";
    //$fecha_busqueda = $_POST['busquedatext'];
}
else
{
    $sel_nfact = "SELECT * FROM ventas 
                  WHERE venta_anular='NO' 
                  ORDER BY date_venta DESC";
    //$fecha_busqueda = '';
}

//echo $fecha_buscar;

$eje_nfact = $DBcon->prepare($sel_nfact);
$eje_nfact->execute();

//$err = $eje_nfact->errorInfo();
//print_r($err);

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <link rel="stylesheet" href="../styles/css/bootstrap.min.css">
</head>
<script>
    function formato_fecha_ini() {
        cadena = document.getElementById('busquedatext').value;
        if(cadena.length == 2) { 
            document.getElementById('busquedatext').value += "-";
        } else if (cadena.length == 5) {
            document.getElementById('busquedatext').value += "-";
        } 
    }
</script>

<body>
  <br>
   <div class="container">
           <form action="rep_ventas_fecha_det.php" class="form-inline" method="post">
               <div class="form-group">
                   <label for="" class="label-control">Buscar: </label>
                   <input type="text" class="form-control" name="busquedatext" id="busquedatext" onkeypress="formato_fecha_ini();" maxlength="10">
                   <button class="btn btn-primary btn-sm" name="busquedabtn" id="busquedabtn"><span class="glyphicon glyphicon-search"></span></button>
               </div>
           </form>
       </div>
       <hr>
       
    <div class="container">
        <table class="table table-stripped">
            <thead>
                <tr>
                    <th>CORR</th>
                    <th>FECHA</th>
                    <th>RECIBO</th>
                    <th>NOMBRE DEL CLIENTE</th>
                    <th>VENDEDOR</th>
                    <th>TOTAL</th>
                    <th>ACCION</th>
                </tr>
            </thead>
            <tbody>
               <?php
                $acum_total = 0;
                $contar = 1;
                $contador=0;
                while($ver_nfact = $eje_nfact->fetch(PDO::FETCH_ASSOC))
	               {
                ?>
                <tr>
                    <td><?php echo $contar; ?></td>
                    <td>
                       <?php
                        $fecha_fact = new datetime($ver_nfact["fecha_venta"]);
                        $fecha_nueva = $fecha_fact->format('d-m-Y');
                        echo $fecha_nueva; 
                        ?>
                        </td>
                    <td><?php echo ''; ?></td>
                    <td><?php echo $ver_nfact["nombre_cliente"]; ?></td>
                    <td><?php echo $ver_nfact["vendedor"]; ?></td>
                    <td><?php echo number_format($ver_nfact["total_venta"],4); ?></td>
                    <td>
                    <a href="" class="btn btn-success btn-xs" onClick="window.open('../reportes/rep_fact_venta.php?cod_vta=<?php echo $ver_nfact['cod_venta']; ?>','Factura de Venta','width=800, height=600, top=100, left=300')"><span class="glyphicon glyphicon-search"></span></a>
                    </td>
                </tr>
                <?php
                }
                ?>
            </tbody>
            
        </table>
    </div>
</body>
</html>