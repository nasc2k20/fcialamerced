<?php
if(isset($_POST["consultar"]))
{
	$cod_lab = $_POST["laboratorio"];
	
	require_once '../conectar.php';
	$fecha = date("d-m-Y");
	
	require_once('../tcpdf/tcpdf.php');
	
	$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);
	
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nahum Sanchez');
	$pdf->SetTitle('Toma de Inventario por Laboratorio '.$fecha);
	
	$pdf->setPrintHeader(false); 
	$pdf->setPrintFooter(false);
	$pdf->SetMargins(20, 20, 20, false); 
	$pdf->SetAutoPageBreak(true, 20); 
	$pdf->SetFont('Helvetica', '', 10);
	$pdf->addPage();
	
	$sel_nfact = "SELECT * FROM laboratorios a
			   INNER JOIN productos b ON a.cod_lab=b.cod_lab 
			   WHERE b.cod_lab=$cod_lab 
			   ORDER BY b.nombre_producto ASC";
	$eje_nfact = $DBcon->prepare($sel_nfact);
	$eje_nfact->execute();
	
	$content = "";
	
	$content .= '
	<h2 align="center">Toma de Inventario Por Laboratorio</h2>
	<div class="row">
		<div class="col-lg-12">
		  <div class="table-responsive">
				<div class="table-responsive">
					<table class="table table-bordered table-hover table-striped" id="tabla_ting" border="1">
	<thead>
		<tr bgcolor="#999999">
		  <th width="10%" align="center" height="30">COD.</th>
		  <th width="40%" align="left">NOMBRE DEL PRODUCTO</th>
			<th width="20%">LABORATORIO</th>
			<th width="10%" align="center">COSTO</th>
			<th width="15%" align="center">EXISTENCIA</th>
			<th width="10%" align="center">FISICO</th>
		</tr>
	</thead>
	
	<tbody>
	';
	
	$acum_costo = 0;
	$acum_c_tot = 0;
	$acum_saldo = 0;
	$contar = 0;
	while($ver_nfact = $eje_nfact->fetch(PDO::FETCH_ASSOC))
	{
	$content .= '
	<tr>
	  <td width="10%" align="center" height="20">'.$ver_nfact["cod_producto"].'</td>
	  <td width="40%" align="left">'.strtoupper($ver_nfact["nombre_producto"]).'</td>
	  <td width="20%">'.$ver_nfact["nombre_lab"].'</td>
	  <td width="10%" align="center">$'.number_format($ver_nfact["precio_costo"],4).'</td>
	  <td width="15%" align="center">'.number_format($ver_nfact["existencia_prod"],0).'</td>
	  <td width="10%" align="center"></td>
	</tr>
	';
	$acum_costo = number_format($ver_nfact["precio_costo"]*$ver_nfact["existencia_prod"],4);
	$acum_c_tot +=$acum_costo;
	$acum_saldo += $ver_nfact["existencia_prod"];
	$contar+=1;
	}
	
	$content .= '			</tbody>
					</table>
			  </div>
		</div>
	  </div>
	</div>
	';
	$content.='Total de Productos: '.$contar.'<br><br>
	Total Unidades en Existencia: '.$acum_saldo.'<br><br>
	Total Monetario en Inventario: $'.$acum_c_tot.'
	';
	
	
	
	$pdf->writeHTML($content, true, 0, true, 0);
	
	$pdf->lastPage();
	$pdf->output('Rep_TomaInventarioLaboratorio_VP.pdf', 'I');
}
?>