<?php
require_once '../conectar.php';

$cod_nfactu = $_GET["cod_compra"];

if(!empty($cod_nfactu))
{
	require_once('../tcpdf/tcpdf.php');
	
	$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);
	
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nahum Sanchez');
	$pdf->SetTitle('Factura de Compra #'.$cod_nfactu);
	
	$pdf->setPrintHeader(false); 
	$pdf->setPrintFooter(false);
	$pdf->SetMargins(20, 20, 20, false); 
	$pdf->SetAutoPageBreak(true, 20); 
	$pdf->SetFont('Helvetica', '', 10);
	$pdf->addPage();
	
	$sel_nfact = "SELECT * FROM compras a
				   INNER JOIN compras_detalle b ON a.cod_compra=b.cod_compra 
				   INNER JOIN proveedor c ON c.cod_prov=a.cod_prov 
				   WHERE b.cod_compra=$cod_nfactu 
				   ORDER BY b.cod_det_compra ASC";
	$eje_nfact = $DBcon->prepare($sel_nfact);
	$eje_nfact->execute();
	$ver_nfact = $eje_nfact->fetch(PDO::FETCH_ASSOC);
	
	$subtotal = $ver_nfact["sumas_vta"];
	$iva_subtotal = $ver_nfact["iva_vta"];
	$total_compra = $ver_nfact["total_compra"];
	
	$sel_nfact_temp = "SELECT * FROM compras a
					   INNER JOIN compras_detalle b ON a.cod_compra=b.cod_compra 
					   INNER JOIN proveedor c ON c.cod_prov=a.cod_prov 
					   WHERE b.cod_compra=$cod_nfactu 
					   ORDER BY b.cod_det_compra ASC";
	$eje_nfact_temp = $DBcon->prepare($sel_nfact_temp);
	$eje_nfact_temp->execute();
	
	
	
	
	$content = "";
	
	$content .='
				<table width="100%" border="0">
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td colspan="3" align="center">COMPROBANTE</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td colspan="3" align="center">'.$ver_nfact["cod_compra"].'</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="19%">&nbsp;</td>
						<td width="26%">&nbsp;</td>
						<td width="9%">&nbsp;</td>
						<td width="9%">&nbsp;</td>
						<td width="9%">&nbsp;</td>
						<td width="10%">&nbsp;</td>
						<td width="9%">&nbsp;</td>
						<td width="9%">&nbsp;</td>
					</tr>
					<tr>
						<td>Nombre Cliente: </td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Fecha:</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2">'.$ver_nfact["nombre_prov"].'</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td colspan="3">'.$ver_nfact["fecha_compra"].'</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Tipo Pago:</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Vendedor:</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2">'.$ver_nfact["tipo_pago_compra"].'</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td colspan="3">'.$ver_nfact["vendedor_compra"].'</td>
					</tr>
			</table>
	';
	
	
	
	$content .= '
		<div class="row">
			<div class="col-lg-12">
			  <div class="table-responsive">
					<div class="table-responsive">
						<table class="table table-bordered table-hover table-striped" id="tabla_ting" border="0">
		<thead>
			<tr>
			  <th width="10%" align="center">CANT.</th>
			  <th width="10%" align="center">BON.</th>
				<th width="50%">NOMBRE DEL PRODUCTO</th>
				<th width="10%" align="center">PFACT</th>
				<th width="10%" align="center">DESC</th>
				<th width="10%" align="center">TOTAL</th>
			</tr>
		</thead>
		
		<tbody>
	';
	
	$acum_tot = 0;
	$acum_des = 0;
    while($ver_nfact_temp = $eje_nfact_temp->fetch(PDO::FETCH_ASSOC))
    {
	$content .= '
		<tr>
		  <td width="10%" align="center">'.$ver_nfact_temp["cantidad_comp"].'</td>
		  <td width="10%" align="center">'.$ver_nfact_temp["bonificacion_comp"].'</td>
		  <td width="50%">'.$ver_nfact_temp["nombre_prod_comp"].'</td>
		  <td width="10%" align="center">$'.number_format($ver_nfact_temp["precio_facturacion"],4).'</td>
		  <td width="10%" align="center">'.number_format($ver_nfact_temp["descuento_ind"],2).'%</td>
		  <td width="10%" align="center">$'.number_format($ver_nfact_temp["total_prod_compra"],3).'</td>
		</tr>
	';
	
	$content .= '
				<tr>
				  <td width="10%" align="center"></td>
				  <td width="10%"></td>
				  <td width="50%"></td>
				  <td width="10%"></td>
				  <td width="10%"></td>
				  <td width="10%"></td>
				</tr>
';
	
	 $desc_ind = number_format(($ver_nfact_temp["total_prod_compra"]*$ver_nfact_temp["descuento_ind"])/100,4);
	 
	 $acum_tot+=$ver_nfact_temp["total_prod_compra"];
	 $acum_des+=$desc_ind;
	}
	
	$content .= '			</tbody>
						</table>
				  </div>
			</div>
		  </div>
		</div>
		';
		
	$content .= '
	<div class="row">
		<div class="col-lg-12">
			  <div class="table-responsive">
					<div class="table-responsive">
					
						<table class="table table-bordered table-hover table-striped" id="tabla_ting">
							<thead>
								<tr>
								  <th width="10%">&nbsp;</th>
									<th width="43%">&nbsp;</th>
									<th width="13%">&nbsp;</th>
									<th width="20%">SubTotal:</th>
									<th width="15%">$'.
									 number_format($subtotal,4).'
									</th>
								</tr>
								<tr>
								  <th width="10%">&nbsp;</th>
									<th width="43%">&nbsp;</th>
									<th width="13%">&nbsp;</th>
									<th width="20%">&nbsp;</th>
									<th width="15%">&nbsp;</th>
								</tr>
								<tr>
								  <th width="10%">&nbsp;</th>
									<th width="43%">&nbsp;</th>
									<th width="13%">&nbsp;</th>
									<th width="20%">Desc</th>
									<th width="15%">$'.
									number_format($acum_des,3)
									.'</th>
								</tr>
								<tr>
								  <th width="10%">&nbsp;</th>
									<th width="43%">&nbsp;</th>
									<th width="13%">&nbsp;</th>
									<th width="20%">&nbsp;</th>
									<th width="15%">&nbsp;</th>
								</tr>
								<tr>
								  <th>&nbsp;</th>
								  <th>&nbsp;</th>
								  <th>&nbsp;</th>
								  <th>Iva:</th>
								  <th>$'.$iva_subtotal = number_format((($subtotal-$acum_des)*13)/100,4).'
								  </th>
								</tr>
								<tr>
								  <th width="10%">&nbsp;</th>
									<th width="43%">&nbsp;</th>
									<th width="13%">&nbsp;</th>
									<th width="20%">&nbsp;</th>
									<th width="15%">&nbsp;</th>
								</tr>
								<tr>
								  <th>&nbsp;</th>
								  <th>&nbsp;</th>
								  <th>&nbsp;</th>
								  <th width="20%">Total Compra:</th>
								  <th width="15%">$'.$subtotal = number_format(($subtotal-$acum_des)+$iva_subtotal,4).'
								  </th>
								</tr>
							</thead>
							
							<tbody>
							</tbody>
						</table>
				  </div>
			</div>
		  </div>
		</div>
	';
		
		
		
	
	$pdf->writeHTML($content, true, 0, true, 0);

	$pdf->lastPage();
	$pdf->output('Rep_FacturaCompra_VP.pdf', 'I');
}
?>