<?php
require_once '../conectar.php';
$fecha = date("d-m-Y");

require_once('../tcpdf/tcpdf.php');

$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);

$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nahum Sanchez');
$pdf->SetTitle('Toma de Inventario General '.$fecha);

$pdf->setPrintHeader(false); 
$pdf->setPrintFooter(false);
$pdf->SetMargins(20, 20, 20, false); 
$pdf->SetAutoPageBreak(true, 20); 
$pdf->SetFont('Helvetica', '', 10);
$pdf->addPage();

$sel_nfact = "SELECT * FROM laboratorios a
			   INNER JOIN productos b ON a.cod_lab=b.cod_lab
			   ORDER BY b.nombre_producto ASC";
$eje_nfact = $DBcon->prepare($sel_nfact);
$eje_nfact->execute();

$content = "";

$content .= '
<h2 align="center">Toma de Inventario General</h2>
	<div class="row">
		<div class="col-lg-12">
		  <div class="table-responsive">
				<div class="table-responsive">
					<table class="table table-bordered table-hover table-striped" id="tabla_ting" border="1">
	<thead>
		<tr bgcolor="#999999">
		  <th width="10%" align="center" height="30">CORR</th>
		  <th width="10%" align="center" height="30">COD.</th>
		  <th width="35%" align="left">NOMBRE DEL PRODUCTO</th>
			<th width="20%">LABORATORIO</th>
			<th width="10%" align="center">COSTO</th>
			<th width="10%" align="center">EXISTENCIA</th>
			<th width="10%" align="center">FISICO</th>
		</tr>
	</thead>
	
	<tbody>
';

$acum_costo = 0;
$acum_c_tot = 0;
$acum_saldo = 0;
$acum_p_tot = 0;
$acum_precio= 0;
$total_productos = 1;


$contar = 0;
while($ver_nfact = $eje_nfact->fetch(PDO::FETCH_ASSOC))
{
$content .= '
	<tr>
	  <td width="10%" align="center" height="20"> '.$total_productos.'</td>
	  <td width="10%" align="center" height="20"> '.$ver_nfact["cod_producto"].'</td>
	  <td width="35%" align="left"> '.strtoupper($ver_nfact["nombre_producto"]).'</td>
	  <td width="20%"> '.$ver_nfact["nombre_lab"].'</td>
	  <td width="10%" align="center"> $ '.number_format($ver_nfact["precio_costo"],4).'</td>
	  <td width="10%" align="center"> '.number_format($ver_nfact["existencia_prod"],0).'</td>
	  <td width="10%" align="center"></td>
	</tr>
';
$acum_costo = number_format($ver_nfact["precio_costo"]*$ver_nfact["existencia_prod"],4);
$acum_precio = number_format($ver_nfact["precio_venta"]*$ver_nfact["existencia_prod"],4);
$acum_c_tot +=$acum_costo;
$acum_p_tot +=$acum_precio;
$acum_saldo += $ver_nfact["existencia_prod"];
$contar+=1;
$total_productos +=1;
}

$content .= '			</tbody>
					</table>
			  </div>
		</div>
	  </div>
	</div>
	';
$content .='
<table border="0">
    <thead>
        <tr>
            <th width="35%">CONCEPTO</th>
            <th width="65%">VALOR</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td width="35%">Total de Productos</td>
            <td width="65%">'.$contar.'</td>
        </tr>
        <tr>
            <td width="35%">Total Unidades en Existencia</td>
            <td width="65%">'.number_format($acum_saldo,0).'</td>
        </tr>
        <tr>
            <td width="35%">Total Inventario (Costo)</td>
            <td width="65%">$ '.number_format($acum_c_tot,4).'</td>
        </tr>
        <tr>
            <td width="35%">Total Inventario (Precio Venta)</td>
            <td width="65%">$ '.number_format($acum_p_tot,4).'</td>
        </tr>
        <tr>
            <td width="35%">Diferencia (Precio Venta - Costo)</td>
            <td width="65%">$ '.number_format($acum_p_tot-$acum_c_tot,4).'</td>
        </tr>
    </tbody>
</table>';
	
	

$pdf->writeHTML($content, true, 0, true, 0);

$pdf->lastPage();
$pdf->output('Rep_TomaInventario_VP.pdf', 'I');
?>