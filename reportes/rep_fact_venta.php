<?php
require_once '../conectar.php';

$cod_nfactu = $_GET["cod_vta"];

if(!empty($cod_nfactu))
{
	require_once('../tcpdf/tcpdf.php');
	//require_once '../mpdf/mpdf.php';
	
	$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);
	
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nahum Sanchez');
	$pdf->SetTitle('Factura de Venta #'.$cod_nfactu);
	
	$pdf->setPrintHeader(false); 
	$pdf->setPrintFooter(false);
	$pdf->SetMargins(20, 20, 20, false); 
	$pdf->SetAutoPageBreak(true, 20); 
	$pdf->SetFont('Helvetica', '', 10);
	$pdf->addPage();
	
	$sel_nfact = "SELECT * FROM ventas a
					   INNER JOIN ventas_detalle b ON a.cod_venta=b.cod_venta 
					   WHERE b.cod_venta=$cod_nfactu 
					   ORDER BY b.cod_det_vta ASC";
	$eje_nfact = $DBcon->prepare($sel_nfact);
	$eje_nfact->execute();
	$ver_nfact = $eje_nfact->fetch(PDO::FETCH_ASSOC);
	$fecha_conver = new datetime($ver_nfact["fecha_venta"]);
    $new_fecha = $fecha_conver->format('d-m-Y');
	
	
	$sel_nfact_temp = "SELECT * FROM ventas a
					   INNER JOIN ventas_detalle b ON a.cod_venta=b.cod_venta 
					   WHERE b.cod_venta=$cod_nfactu 
					   ORDER BY b.cod_det_vta ASC";
	$eje_nfact_temp = $DBcon->prepare($sel_nfact_temp);
	$eje_nfact_temp->execute();
	
	
	
	
	$content = "";
	//$formato_fecha=new DateTime($ver_nfact_temp["vence_prod"]).' '. $formato_fecha->format("d-m-Y")
	
	$content .='
				<table width="100%" border="0">
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td colspan="3" align="center">FACTURA</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td colspan="3" align="center">'.$ver_nfact["cod_venta"].'</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td width="19%">&nbsp;</td>
						<td width="26%">&nbsp;</td>
						<td width="9%">&nbsp;</td>
						<td width="9%">&nbsp;</td>
						<td width="9%">&nbsp;</td>
						<td width="10%">&nbsp;</td>
						<td width="9%">&nbsp;</td>
						<td width="9%">&nbsp;</td>
					</tr>
					<tr>
						<td>Nombre Cliente: </td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Fecha:</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2">'.$ver_nfact["nombre_cliente"].'</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td colspan="3">'.$new_fecha.'</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>Tipo Pago:</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>Vendedor:</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td colspan="2">'.$ver_nfact["tipo_pago"].'</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td colspan="3">'.$ver_nfact["vendedor"].'</td>
					</tr>
			</table>
	';
	
	
	
	$content .= '
		<div class="row">
			<div class="col-lg-12">
			  <div class="table-responsive">
					<div class="table-responsive">
						<table class="table table-bordered table-hover table-striped" id="tabla_ting">
							<thead>
								<tr>
								  <th width="10%">CANT.</th>
									<th width="43%">DESCRIPCION</th>
									<th width="13%">PRECIO</th>
									<th width="10%">EXENTA</th>
									<th width="10%">TOTAL</th>
								</tr>
							</thead>
							<tbody>
	';
	$subtotal = 0;
	$acum_tot = 0;
    while($ver_nfact_temp = $eje_nfact_temp->fetch(PDO::FETCH_ASSOC))
    {
	$content .= '
								<tr>
								  <td width="10%" align="center">'.$ver_nfact_temp['cant_vta'].'</td>
								  <td width="43%">'.$ver_nfact_temp["nombre_prod"].'</td>
								  <td width="13%">'.number_format($ver_nfact_temp["precio_vta"],4).'</td>
								  <td width="10%">$'."0.00".'</td>
								  <td width="10%">$'.number_format($ver_nfact_temp["total_vta"],4).'</td>
								</tr>
	';
	
	$content .= '
								<tr>
								  <td width="10%" align="center"></td>
								  <td width="43%"></td>
								  <td width="13%"></td>
								  <td width="10%"></td>
								  <td width="10%"></td>
								</tr>
	';
	
	 $acum_tot+=$ver_nfact_temp["total_vta"];
	}
	
	$content .= '			</tbody>
						</table>
				  </div>
			</div>
		  </div>
		</div>
		';
		
	$content .= '
	<div class="row">
		<div class="col-lg-12">
			  <div class="table-responsive">
					<div class="table-responsive">
					
						<table class="table table-bordered table-hover table-striped" id="tabla_ting">
							<thead>
								<tr>
								  <th width="10%">&nbsp;</th>
									<th width="43%">&nbsp;</th>
									<th width="13%">&nbsp;</th>
									<th width="10%">SubTotal:</th>
									<th width="10%">$'.
									$subtotal = number_format($acum_tot,4).'
									</th>
								</tr>
								<tr>
								  <th width="10%">&nbsp;</th>
									<th width="43%">&nbsp;</th>
									<th width="13%">&nbsp;</th>
									<th width="10%">&nbsp;</th>
									<th width="10%">&nbsp;</th>
								</tr>
								<tr>
								  <th>&nbsp;</th>
								  <th>&nbsp;</th>
								  <th>&nbsp;</th>
								  <th>Exenta:</th>
								  <th>$'.$iva_subtotal = number_format(($subtotal*13)/100,4).'
								  </th>
								</tr>
								<tr>
								  <th width="10%">&nbsp;</th>
									<th width="43%">&nbsp;</th>
									<th width="13%">&nbsp;</th>
									<th width="10%">&nbsp;</th>
									<th width="10%">&nbsp;</th>
								</tr>
								<tr>
								  <th>&nbsp;</th>
								  <th>&nbsp;</th>
								  <th>&nbsp;</th>
								  <th>Total Vta:</th>
								  <th>$'.$subtotal = number_format($acum_tot,4).'
								  </th>
								</tr>
							</thead>
							
							<tbody>
							</tbody>
						</table>
				  </div>
			</div>
		  </div>
		</div>
	';
		
		
		
	
	$pdf->writeHTML($content, true, 0, true, 0);

	$pdf->lastPage();
	$pdf->output('Rep_FacturaVenta_VP.pdf', 'I');

?>


<body>
        <!----><div id="page-wrapper">
        <!--<div id="container">-->
            
            <div class="row">
                <div class="col-lg-12">
                    	
                            <p>
                            <div class="row">
                                <div class="col-lg-12">
                                  <div class="table-responsive">
                                    	<div class="table-responsive">
                                        
                                            <table class="table table-bordered table-hover table-striped" id="tabla_ting">
                                                <thead>
                                                    <tr>
                                                      <th width="10%">Cantidad</th>
                                                        <th width="43%">Nombre del Producto</th>
                                                        <th width="13%">Vence</th>
                                                        <th width="10%">Precio</th>
                                                        <th width="10%">Total</th>
                                                    </tr>
                                                </thead>
                                                
                                                <tbody>
                                                <?php
												$acum_tot = 0;
												while($ver_nfact_temp = $eje_nfact_temp->fetch(PDO::FETCH_ASSOC))
												{
												?>
                                                	<tr>
                                                	  <td>
													  <?php
													  echo $ver_nfact_temp["cant_vta"]; 
													  ?>
                                                      </td>
                                                      <td>
													  <?php
													  echo $ver_nfact_temp["nombre_prod"];
                                                      ?>
                                                      </td>
                                                      <td>
													  <?php
                                                      $formato_fecha=new DateTime($ver_nfact_temp["vence_prod"]);
														echo $formato_fecha->format("d-m-Y");
													  ?>
                                                      </td>
                                                      <td><?php echo "$".number_format($ver_nfact_temp["precio_vta"],4); ?></td>
                                                        <td><?php echo "$".number_format($ver_nfact_temp["total_vta"],4); ?></td>
                                                    </tr>
                                                 <?php
												 $acum_tot+=$ver_nfact_temp["total_vta"];
												}
												?>
                                                </tbody>
                                            </table>
                                   	  </div>
                                    <!-- /.table-responsive -->
                                </div>
                              </div>
                            </div>
                            
                            
                            <div class="row">
                            <div class="col-lg-12">
                                  <div class="table-responsive">
                                    	<div class="table-responsive">
                                        
                                            <table class="table table-bordered table-hover table-striped" id="tabla_ting">
                                                <thead>
                                                    <tr>
                                                      <th width="10%">&nbsp;</th>
                                                        <th width="43%">&nbsp;</th>
                                                        <th width="13%">&nbsp;</th>
                                                        <th width="10%">SubTotal:</th>
                                                        <th width="10%">
                                                        <?php
														$subtotal = number_format($acum_tot/1.13,4);
                                                        echo "$".$subtotal;
														?>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                      <th>&nbsp;</th>
                                                      <th>&nbsp;</th>
                                                      <th>&nbsp;</th>
                                                      <th>Iva:</th>
                                                      <th>
                                                      <?php
                                                      $iva_subtotal = number_format(($subtotal*13)/100,4);
													  echo "$".$iva_subtotal;
													  ?>
                                                      </th>
                                                    </tr>
                                                    <tr>
                                                      <th>&nbsp;</th>
                                                      <th>&nbsp;</th>
                                                      <th>&nbsp;</th>
                                                      <th>Total Vta:</th>
                                                      <th>
                                                      <?php
                                                      echo "$".number_format($subtotal+$iva_subtotal,2);
													  ?>
                                                      </th>
                                                    </tr>
                                                </thead>
                                                
                                                <tbody>
                                                </tbody>
                                            </table>
                                   	  </div>
                                    <!-- /.table-responsive -->
                                </div>
                              </div>
                            </div>
                            
                                
                                
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    
               
                        
                        
                   
            </div>
            <!-- /.row 
        </div>-->
        <!-- /#page-wrapper 
-->
    </div>

 

<script src="../styles/js/jquery.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="../styles/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript 
<script src="styles/js/metisMenu.min.js"></script>
-->

<!--
<script src="styles/js/sb-admin-2.js"></script>
-->
</body>

</html>
<?php
}
?>