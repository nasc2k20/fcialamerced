<?php
if(isset($_POST["consultar"]))
{
	$f_ini_esp = $_POST["fecha_ini"];
	$f_fin_esp = $_POST["fecha_fin"];
	
	$fecha_ini_f=new DateTime($_POST["fecha_ini"]);
	$fecha_ini = $fecha_ini_f->format("Y-m-d");
	
	$fecha_fin_f=new DateTime($_POST["fecha_fin"]);
	$fecha_fin = $fecha_fin_f->format("Y-m-d");
	
	
	require_once '../conectar.php';
	
	require_once('../tcpdf/tcpdf.php');
	
	$pdf = new TCPDF('P', 'mm', 'A4', true, 'UTF-8', false);
	
	$pdf->SetCreator(PDF_CREATOR);
	$pdf->SetAuthor('Nahum Sanchez');
	$pdf->SetTitle('Reporte de Ventas por Fecha '.$fecha_ini .' al '.$fecha_fin);
	
	$pdf->setPrintHeader(false); 
	$pdf->setPrintFooter(false);
	$pdf->SetMargins(20, 20, 20, false); 
	$pdf->SetAutoPageBreak(true, 20); 
	$pdf->SetFont('Helvetica', '', 10);
	$pdf->addPage();
	
	$sel_nfact = "SELECT * FROM ventas 
				  WHERE fecha_venta BETWEEN '$fecha_ini' AND '$fecha_fin'
				  AND venta_anular='NO' 
				  ORDER BY date_venta DESC";
	$eje_nfact = $DBcon->prepare($sel_nfact);
	$eje_nfact->execute();
	
	$content = "";
	
	$content .= '
	<h2 align="center">Facturas de Venta <br><br> '.$f_ini_esp.' y '.$f_fin_esp .'</h2>
	<div class="row">
		<div class="col-lg-12">
		  <div class="table-responsive">
				<div class="table-responsive">
					<table class="table table-bordered table-hover table-striped" id="tabla_ting" border="1">
	<thead>
		<tr bgcolor="#999999">
		  <th width="8%" align="center" height="30">CORR</th>
		  <th width="12%" align="left">FECHA</th>
			<th width="10%">RECIBO</th>
			<th width="30%" align="center">NOMBRE DEL CLIENTE</th>
			<th width="25%" align="center">VENDEDOR</th>
			<th width="15%" align="center">TOTAL</th>
		</tr>
	</thead>
	
	<tbody>
	';
	
	$acum_total = 0;
	$contar = 1;
	$contador=0;
	while($ver_nfact = $eje_nfact->fetch(PDO::FETCH_ASSOC))
	{
	$content .= '
	<tr>
	  <td width="8%" align="center" height="20">'.$contar.'</td>
	  <td width="12%" align="left">'.strtoupper($ver_nfact["fecha_venta"]).'</td>
	  <td width="10%"></td>
	  <td width="30%" align="center">'.$ver_nfact["nombre_cliente"].'</td>
	  <td width="25%" align="center">'.$ver_nfact["vendedor"].'</td>
	  <td width="15%" align="center">$'.number_format($ver_nfact["total_venta"],4).'</td>
	</tr>
	';
	$contar+=1;
	$contador+=1;
	$acum_total+=number_format($ver_nfact["total_venta"],4);
	}
	
	$content .= '			</tbody>
					</table>
			  </div>
		</div>
	  </div>
	</div>
	';
	$content.='Total de Facturas: '.$contador.'<br><br>
	Total De Venta: $'.number_format($acum_total,2).'<br><br>
	';
	
	
	
	$pdf->writeHTML($content, true, 0, true, 0);
	
	$pdf->lastPage();
	ob_end_clean();
	$pdf->output('Rep_TomaInventarioLaboratorio_VP.pdf', 'I');
}
?>